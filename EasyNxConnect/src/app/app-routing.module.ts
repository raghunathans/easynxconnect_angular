import { NgModule } from '@angular/core';
import { MenuComponent } from './menu/menu/menu.component';
import { EnxGeneralSettingsComponent } from './pages/enx-general-settings/enx-general-settings.component';
import { ENXLoginComponent } from './pages/enx-login/enx-login.component';
import { EnxLicenseComponent } from './pages/enx-license/enx-license.component';
import { RouterModule, Routes } from '@angular/router';
import { EnxUserAccountsComponent } from './pages/enx-user-accounts/enx-user-accounts.component';
import { UserConfigureSummaryComponent } from './pages/user-configure-summary/user-configure-summary.component';
import { CreateuserComponent } from './pages/createuser/createuser.component';
import { ConnectorListComponent } from './pages/connector-list/connector-list.component';
import { AmazonS3Component } from './pages/amazon-s3/amazon-s3.component';
import { BoxConnectorComponent } from './pages/box-connector/box-connector.component';
import { AzureBlobComponent } from './pages/azure-blob/azure-blob.component';
import { DropBoxComponent } from './pages/drop-box/drop-box.component';
import { EmailComponent } from './pages/email/email.component';
import { NintexComponent } from './pages/nintex/nintex.component';
import { UIPathComponent } from './pages/uipath/uipath.component';
import { HomeFolderComponent } from './pages/home-folder/home-folder.component';
import { EgnyteComponent } from './pages/egnyte/egnyte.component';
import { FTPComponent } from './pages/ftp/ftp.component';
import { FaxComponent } from './pages/fax/fax.component';
import { OneDriveComponent } from './pages/one-drive/one-drive.component';
import { SharePointOnlineComponent } from './pages/share-point-online/share-point-online.component';
import { JobManagementComponent } from './pages/job-management/job-management.component';
import { JobGroupManagementComponent } from './pages/job-group-management/job-group-management.component';
import { PrinterConnectorComponent } from './pages/printer-connector/printer-connector.component';
import { ExceptionsComponent } from './pages/exceptions/exceptions.component';
import { OCRConnectorComponent } from './pages/ocrconnector/ocrconnector.component';
import { EditJobGroupsComponent } from './pages/edit-job-groups/edit-job-groups.component';
import { WebLoginComponent } from './pages/web-login/web-login.component';
import { DirtycheckGuard } from './dirtycheck.guard';
import { DocuwareComponent } from './pages/docuware/docuware.component';


const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  {path: 'login',component: ENXLoginComponent,},
  {
    path: "",
    component: MenuComponent,
    children: [
      {path:'generalsettings',component:EnxGeneralSettingsComponent,canDeactivate:[DirtycheckGuard]},
      {path:'menu',component:MenuComponent,},
      {path:'license',component:EnxLicenseComponent,canDeactivate:[DirtycheckGuard]},
      {path:'useraccounts',component:EnxUserAccountsComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'userconfiguresummary',component:UserConfigureSummaryComponent},
      {path: 'createuser',component:CreateuserComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'connectorlist',component:ConnectorListComponent,},
      {path: 'amazons3',component:AmazonS3Component,canDeactivate:[DirtycheckGuard]},
      {path: 'box',component:BoxConnectorComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'AzureBlob',component:AzureBlobComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'DropBox',component:DropBoxComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'Email',component:EmailComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'Nintex',component:NintexComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'UIPath',component:UIPathComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'HomeFolder',component:HomeFolderComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'Egnyte',component:EgnyteComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'FTP',component:FTPComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'Fax',component:FaxComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'OneDrive',component:OneDriveComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'SharePointOnline',component:SharePointOnlineComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'JobManagement',component:JobManagementComponent},
      {path: 'CreateJobGroups',component:JobGroupManagementComponent},
      {path: 'printerConnector',component:PrinterConnectorComponent},
      {path: 'Exceptions',component:ExceptionsComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'OCRConnector',component:OCRConnectorComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'EditJobGroups',component:EditJobGroupsComponent,canDeactivate:[DirtycheckGuard]},
      {path: 'WebLogin',component:WebLoginComponent},
      {path:'Docuware',component:DocuwareComponent}

    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
