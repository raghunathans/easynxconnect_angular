import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "src/app/_api/api.service";
import { MatSidenav } from "@angular/material/sidenav";
import { Observable } from "rxjs";
import enUSAdmin from "src/assets/Languages/English/en-USAdmin.json";
import enUSUser from "src/assets/Languages/English/en-USUser.json";
import enUSDocuwareAdmin from "src/assets/Languages/English/en-USDocuwareAdmin.json";
import enUSDocuwareUser from "src/assets/Languages/English/en-USDocuwareUser.json";
import enUSMFilesAdmin from "src/assets/Languages/English/en-USMFilesAdmin.json";
import enUSMFilesUser from "src/assets/Languages/English/en-USMFilesUser.json";
import enUSEgnyteAdmin from "src/assets/Languages/English/en-USEgnyteAdmin.json";
import enUSEgnyteUser from "src/assets/Languages/English/en-USEgnyteUser.json";
import enUSNintexAdmin from "src/assets/Languages/English/en-USNintexAdmin.json";
import enUSNintexUser from "src/assets/Languages/English/en-USNintexUser.json";

import enUSUIPathAdmin from "src/assets/Languages/English/en-USUIPathAdmin.json";
import enUSUIPathUser from "src/assets/Languages/English/en-USUIPathUser.json";

import enUSCustomTargetAdmin from "src/assets/Languages/English/en-USCustomTargetAdmin.json";
import enUSCustomTargetUser from "src/assets/Languages/English/en-USCustomTargetUser.json";
import enUSCustomTargetSLNXAdmin from "src/assets/Languages/English/en-USCustomTargetSLNXAdmin.json";

import enUSCustomTargetKTAAdmin from "src/assets/Languages/English/en-USCustomTargetKTAAdmin.json";
import enUSCustomTargetDIGAdmin from "src/assets/Languages/English/en-USCustomTargetDIGAdmin.json";

//German
import deDEAdmin from "src/assets/Languages/German/de-DEAdmin.json";
import deDEDocuwareAdmin from "src/assets/Languages/German/de-DEDocuwareAdmin.json";
import deDEDocuwareUser from "src/assets/Languages/German/de-DEDocuwareUser.json";
import deDEEasyConnector from "src/assets/Languages/German/de-DEEasyConnector.json";
import deDEEgnyteAdmin from "src/assets/Languages/German/de-DEEgnyteAdmin.json";
import deDEEgnyteUser from "src/assets/Languages/German/de-DEEgnyteUser.json";
import deDEMFilesAdmin from "src/assets/Languages/German/de-DEMFilesAdmin.json";
import deDEMFilesUser from "src/assets/Languages/German/de-DEMFilesUser.json";
import deDENintexAdmin from "src/assets/Languages/German/de-DENintexAdmin.json";
import deDENintexUser from "src/assets/Languages/German/de-DENintexUser.json";
import deDEUIPathAdmin from "src/assets/Languages/German/de-DEUIPathAdmin.json";
import deDEUIPathUser from "src/assets/Languages/German/de-DEUIPathUser.json";
import deDECustomTargetAdmin from "src/assets/Languages/German/de-DECustomTargetAdmin.json";
import deDECustomTargetDIGAdmin from "src/assets/Languages/German/de-DECustomTargetDIGAdmin.json";
import deDECustomTargetKTAAdmin from "src/assets/Languages/German/de-DECustomTargetKTAAdmin.json";
import deDECustomTargetSLNXAdmin from "src/assets/Languages/German/de-DECustomTargetSLNXAdmin.json";

import deDECustomTargetUser from "src/assets/Languages/German/de-DECustomTargetUser.json";

//Spanish

import esESAdmin from "src/assets/Languages/Spanish/es-ESAdmin.json";
import esESCustomTargetAdmin from "src/assets/Languages/Spanish/es-ESCustomTargetAdmin.json";
import esESCustomTargetDIGAdmin from "src/assets/Languages/Spanish/es-ESCustomTargetDIGAdmin.json";
import esESCustomTargetKTAAdmin from "src/assets/Languages/Spanish/es-ESCustomTargetKTAAdmin.json";
import esESCustomTargetSLNXAdmin from "src/assets/Languages/Spanish/es-ESCustomTargetSLNXAdmin.json";
import esESCustomTargetUser from "src/assets/Languages/Spanish/es-ESCustomTargetUser.json";
import esESDocuwareAdmin from "src/assets/Languages/Spanish/es-ESDocuwareAdmin.json";
import esESDocuwareUser from "src/assets/Languages/Spanish/es-ESDocuwareUser.json";
import esESEasyConnector from "src/assets/Languages/Spanish/es-ESEasyConnector.json";
import esESEgnyteAdmin from "src/assets/Languages/Spanish/es-ESEgnyteAdmin.json";
import esESEgnyteUser from "src/assets/Languages/Spanish/es-ESEgnyteUser.json";
import esESMFilesAdmin from "src/assets/Languages/Spanish/es-ESMFilesAdmin.json";
import esESMFilesUser from "src/assets/Languages/Spanish/es-ESMFilesUser.json";
import esESNintexAdmin from "src/assets/Languages/Spanish/es-ESNintexAdmin.json";
import esESNintexUser from "src/assets/Languages/Spanish/es-ESNintexUser.json";
import esESUIPathAdmin from "src/assets/Languages/Spanish/es-ESUIPathAdmin.json";
import esESUIPathUser from "src/assets/Languages/Spanish/es-ESUIPathUser.json";

//French

import frFRAdmin from "src/assets/Languages/French/fr-FRAdmin.json";
import frFRCustomTargetAdmin from "src/assets/Languages/French/fr-FRCustomTargetAdmin.json";
import frFRCustomTargetDIGAdmin from "src/assets/Languages/French/fr-FRCustomTargetDIGAdmin.json";
import frFRCustomTargetKTAAdmin from "src/assets/Languages/French/fr-FRCustomTargetKTAAdmin.json";
import frFRCustomTargetSLNXAdmin from "src/assets/Languages/French/fr-FRCustomTargetSLNXAdmin.json";
import frFRCustomTargetUser from "src/assets/Languages/French/fr-FRCustomTargetUser.json";
import frFRDocuwareAdmin from "src/assets/Languages/French/fr-FRDocuwareAdmin.json";
import frFRDocuwareUser from "src/assets/Languages/French/fr-FRDocuwareUser.json";
import frFREasyConnector from "src/assets/Languages/French/fr-FREasyConnector.json";
import frFREgnyteAdmin from "src/assets/Languages/French/fr-FREgnyteAdmin.json";
import frFREgnyteUser from "src/assets/Languages/French/fr-FREgnyteUser.json";
import frFRMFilesAdmin from "src/assets/Languages/French/fr-FRMFilesAdmin.json";
import frFRMFilesUser from "src/assets/Languages/French/fr-FRMFilesUser.json";
import frFRNintexAdmin from "src/assets/Languages/French/fr-FRNintexAdmin.json";
import frFRNintexUser from "src/assets/Languages/French/fr-FRNintexUser.json";
import frFRUIPathAdmin from "src/assets/Languages/French/fr-FRUIPathAdmin.json";
import frFRUIPathUser from "src/assets/Languages/French/fr-FRUIPathUser.json";

//Italy
import itITAdmin from "src/assets/Languages/Italy/it-ITAdmin.json";
import itITCustomTargetAdmin from "src/assets/Languages/Italy/it-ITCustomTargetAdmin.json";
import itITCustomTargetDIGAdmin from "src/assets/Languages/Italy/it-ITCustomTargetDIGAdmin.json";
import itITCustomTargetKTAAdmin from "src/assets/Languages/Italy/it-ITCustomTargetKTAAdmin.json";
import itITCustomTargetSLNXAdmin from "src/assets/Languages/Italy/it-ITCustomTargetSLNXAdmin.json";
import itITCustomTargetUser from "src/assets/Languages/Italy/it-ITCustomTargetUser.json";
import itITDocuwareAdmin from "src/assets/Languages/Italy/it-ITDocuwareAdmin.json";
import itITDocuwareUser from "src/assets/Languages/Italy/it-ITDocuwareUser.json";
import itITEasyConnector from "src/assets/Languages/Italy/it-ITEasyConnector.json";
import itITEgnyteAdmin from "src/assets/Languages/Italy/it-ITEgnyteAdmin.json";
import itITEgnyteUser from "src/assets/Languages/Italy/it-ITEgnyteUser.json";
import itITMFilesAdmin from "src/assets/Languages/Italy/it-ITMFilesAdmin.json";
import itITMFilesUser from "src/assets/Languages/Italy/it-ITMFilesUser.json";
import itITNintexAdmin from "src/assets/Languages/Italy/it-ITNintexAdmin.json";
import itITNintexUser from "src/assets/Languages/Italy/it-ITNintexUser.json";
import itITUIPathAdmin from "src/assets/Languages/Italy/it-ITUIPathAdmin.json";
import itITUIPathUser from "src/assets/Languages/Italy/it-ITUIPathUser.json";

import { NotificationService } from "src/app/notification.service";
import { TranslateService } from "@ngx-translate/core";
import { ErrorCodesService } from "src/app/ErrorMessageCodes/error-codes.service";

export interface Language {
  Id: number;
  LanguageCode: string;
  LanguageName: string;
  RoleName: string;
}
export interface MenuItem {
  text?: string;
  icon?: string;
  routerLink?: string;
  name?: string;
}

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"],
})
export class MenuComponent implements OnInit {
  hide: boolean = true;
  hideNew: boolean = true;
  hideConfirm: boolean = true;
  oldpassword: any;
  newpassword: any;
  confirmpassword: any;
  username: any;
  MenuItems: MenuItem[] = [];
  isExpanded: boolean = true;
  isMenuOpen = true;
  contentMargin = 240;
  fontsize = 25;
  Languageshow: boolean = false;
  languages: Language[] = [];
  errorCodes: any = {};
  selected: any;
  displayChangeLanguage = "none";
  displayChangepsw = "none";
  constructor(
    private router: Router,
    private ErrorCodes: ErrorCodesService,
    public translate: TranslateService,
    private service: ApiService,
    private notification: NotificationService
  ) {}

  ngOnInit(): void {
    this.username = localStorage.getItem("loginname");
    let lang = localStorage.getItem("PreferredLanguage");
    let roleName = localStorage.getItem("Role");
    let authType = localStorage.getItem("AuthType");
    let secondary = localStorage.getItem("secondaryAuthType");
    var CustomProductType = localStorage.getItem("CustomProductType");
    var model = localStorage.getItem("CustomProductModel");
    this.errorCodes = this.ErrorCodes.errormsgs();

    switch (lang) {
      case "en-US":
        if (authType == "Docuware") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = enUSDocuwareAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = enUSDocuwareUser;
          }
        } else if (authType == "MFiles") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = enUSMFilesAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = enUSMFilesUser;
          }
        } else if (secondary == "Egnyte" || secondary == "EGNYTE") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = enUSEgnyteAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = enUSEgnyteUser;
          }
        } else if (secondary == "Nintex") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = enUSNintexAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = enUSNintexUser;
          }
        } else if (secondary == "UIPath") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = enUSUIPathAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = enUSUIPathUser;
          }
        } else if (
          CustomProductType == "CUSTOM" ||
          CustomProductType == "custom"
        ) {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = enUSCustomTargetAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = enUSCustomTargetUser;
          }
        } else if (model == "SLNX") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == enUSCustomTargetSLNXAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = enUSCustomTargetUser;
          }
        } else if (model == "KTA") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == enUSCustomTargetKTAAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = enUSCustomTargetUser;
          }
        } else if (model == "DIG") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == enUSCustomTargetDIGAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = enUSCustomTargetUser;
          }
        } else {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = enUSAdmin;
          } else {
            this.MenuItems = enUSUser;
          }
        }
        break;
      case "de-DE":
        if (authType == "Docuware") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = deDEDocuwareAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = deDEDocuwareUser;
          }
        } else if (authType == "MFiles") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = deDEMFilesAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = deDEMFilesUser;
          }
        } else if (secondary == "Egnyte" || secondary == "EGNYTE") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = deDEEgnyteAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = deDEEgnyteUser;
          }
        } else if (secondary == "Nintex") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = deDENintexAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = deDENintexUser;
          }
        } else if (secondary == "UIPath") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = deDEUIPathAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = deDEUIPathUser;
          }
        } else if (
          CustomProductType == "CUSTOM" ||
          CustomProductType == "custom"
        ) {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = deDECustomTargetAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = deDECustomTargetUser;
          }
        } else if (model == "SLNX") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == deDECustomTargetSLNXAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = deDECustomTargetUser;
          }
        } else if (model == "KTA") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == deDECustomTargetKTAAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = deDECustomTargetUser;
          }
        } else if (model == "DIG") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == deDECustomTargetDIGAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = deDECustomTargetUser;
          }
        } else {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = deDEAdmin;
          } else {
            this.MenuItems = deDEEasyConnector;
          }
        }
        break;
      case "larry":
        alert("Hey");
        break;
      case "es-ES":
        if (authType == "Docuware") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = esESDocuwareAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = esESDocuwareUser;
          }
        } else if (authType == "MFiles") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = esESMFilesAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = esESMFilesUser;
          }
        } else if (secondary == "Egnyte" || secondary == "EGNYTE") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = esESEgnyteAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = esESEgnyteUser;
          }
        } else if (secondary == "Nintex") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = esESNintexAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = esESNintexUser;
          }
        } else if (secondary == "UIPath") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = esESUIPathAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = esESUIPathUser;
          }
        } else if (
          CustomProductType == "CUSTOM" ||
          CustomProductType == "custom"
        ) {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = esESCustomTargetAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = esESCustomTargetUser;
          }
        } else if (model == "SLNX") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == esESCustomTargetSLNXAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = esESCustomTargetUser;
          }
        } else if (model == "KTA") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == esESCustomTargetKTAAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = esESCustomTargetUser;
          }
        } else if (model == "DIG") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == esESCustomTargetDIGAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = esESCustomTargetUser;
          }
        } else {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = esESAdmin;
          } else {
            this.MenuItems = esESEasyConnector;
          }
        }
        break;
      case "larry":
        alert("Hey");
        break;
      case "fr-FR":
        if (authType == "Docuware") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = frFRDocuwareAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = frFRDocuwareUser;
          }
        } else if (authType == "MFiles") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = frFRMFilesAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = frFRMFilesUser;
          }
        } else if (secondary == "Egnyte" || secondary == "EGNYTE") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = frFREgnyteAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = frFREgnyteUser;
          }
        } else if (secondary == "Nintex") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = frFRNintexAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = frFRNintexUser;
          }
        } else if (secondary == "UIPath") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = frFRUIPathAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = frFRUIPathUser;
          }
        } else if (
          CustomProductType == "CUSTOM" ||
          CustomProductType == "custom"
        ) {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = frFRCustomTargetAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = frFRCustomTargetUser;
          }
        } else if (model == "SLNX") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == frFRCustomTargetSLNXAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = frFRCustomTargetUser;
          }
        } else if (model == "KTA") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == frFRCustomTargetKTAAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = frFRCustomTargetUser;
          }
        } else if (model == "DIG") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == frFRCustomTargetDIGAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = frFRCustomTargetUser;
          }
        } else {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = frFRAdmin;
          } else {
            this.MenuItems = frFREasyConnector;
          }
        }
        break;
      case "it-IT":
        if (authType == "Docuware") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = itITDocuwareAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = itITDocuwareUser;
          }
        } else if (authType == "MFiles") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = itITMFilesAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = itITMFilesUser;
          }
        } else if (secondary == "Egnyte" || secondary == "EGNYTE") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = itITEgnyteAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = itITEgnyteUser;
          }
        } else if (secondary == "Nintex") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = itITNintexAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = itITNintexUser;
          }
        } else if (secondary == "UIPath") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = itITUIPathAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = itITUIPathUser;
          }
        } else if (
          CustomProductType == "CUSTOM" ||
          CustomProductType == "custom"
        ) {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = itITCustomTargetAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = itITCustomTargetUser;
          }
        } else if (model == "SLNX") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == itITCustomTargetSLNXAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = itITCustomTargetUser;
          }
        } else if (model == "KTA") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == itITCustomTargetKTAAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = itITCustomTargetUser;
          }
        } else if (model == "DIG") {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems == itITCustomTargetDIGAdmin;
          } else if (roleName == "Users") {
            this.MenuItems = itITCustomTargetUser;
          }
        } else {
          if (roleName == "Administrator" || roleName == "SuperAdmin") {
            this.MenuItems = frFRAdmin;
          } else {
            this.MenuItems = itITEasyConnector;
          }
        }
        break;
      default:
        this.MenuItems = enUSAdmin;
    }
    if (roleName == "SuperAdmin" || authType == "LaserFiche") {
      this.MenuItems.forEach((element, index) => {
        if (element.name == "Weblogin") this.MenuItems.splice(index, 1);
        return;
      });
    }
    if (authType != "MFiles") {
      this.MenuItems.forEach((element, index) => {
        if (element.text == "M-Files") this.MenuItems.splice(index, 1);
        return;
      });
    }
  }

  openLang() {
    this.getLanguages();
    this.displayChangeLanguage = "block";
  }
  languagechange1(val: any) {
    localStorage.setItem("PreferredLanguage", val.value);
  }
  ChangeLanguage() {
    let url = "api/User/UpdateLanguage";
    let postData = {
      Username: localStorage.getItem("username"),
      PreferredLanguage: localStorage.getItem("PreferredLanguage"),
    };
    this.service.post(url, postData).subscribe(
      (resp) => {
        if (resp == true) {
          let returnUrl: any = localStorage.getItem("PreferredLanguage");
          this.displayChangeLanguage = "none";
          this.translate.use(returnUrl);
          this.notification.openSnackBar(
            this.errorCodes["SUC024"].msg,
            "",
            "success-snackbar"
          );
          window.location.reload();
        }
      },
      (error) => {
        this.displayChangeLanguage = "none";
      }
    );
  }
  getLanguages() {
    let url = "api/User/GetLanguages?id=" + "";
    this.service.getAPI(url).then(
      (resp) => {
        this.Languageshow = true;
        this.languages = resp;
        for (var i = 0; i <= this.languages.length; i++) {
          var lng = this.languages[i].LanguageCode;
          if (lng == localStorage.getItem("PreferredLanguage")) {
            this.selected = this.languages[i].LanguageCode;
            break;
          }
        }
        // this.ngxLoader.stop();
      },
      (error) => {
        this.notification.openSnackBar(error.message, "", "red-snackbar");
        // this.ngxLoader.stop();
      }
    );
  }
  Cancel() {
    this.displayChangeLanguage = "none";
    this.displayChangepsw = "none";
  }

  //Logout
  Logout() {
    window.localStorage.clear();
    this.router.navigate(["/login"]);
  }

  onToolbarMenuToggle() {
    this.isMenuOpen = !this.isMenuOpen;
    if (!this.isMenuOpen) {
      this.contentMargin = 100;
      this.fontsize = 30;
    } else {
      this.contentMargin = 230;
      this.fontsize = 25;
    }
  }

  myFunction() {
    this.hide = !this.hide;
  }
  myFunctionNew() {
    this.hideNew = !this.hideNew;
  }
  myFunctionConfirm() {
    this.hideConfirm = !this.hideConfirm;
  }

  ChangePSW() {
    this.displayChangepsw = "block";
  }
  ChangePassword() {
    if (
      this.newpassword == undefined ||
      this.oldpassword == undefined ||
      this.confirmpassword == undefined
    ) {
      this.notification.openSnackBar(
        this.errorCodes["WAR035"].msg,
        "",
        "warning-snackbar"
      );
      return;
    }
    if (this.newpassword != this.confirmpassword) {
      this.notification.openSnackBar(
        this.errorCodes["WAR030"].msg,
        "",
        "warning-snackbar"
      );
      return;
    }
    let url = "api/User/ChangePassword";
    let postData = {
      UserName: localStorage.getItem("username"),
      oldpassword: this.oldpassword,
      password: this.newpassword,
      ConfirmPassword: this.confirmpassword,
    };
    this.service.post(url, postData).subscribe(
      (resp) => {
        if (resp == true) {
          this.notification.openSnackBar(
            this.errorCodes["SUC007"].msg,
            "",
            "success-snackbar"
          );
        } else {
          this.notification.openSnackBar(
            this.errorCodes["ERR001"].msg,
            "",
            "warning-snackbar"
          );
        }
      },
      (error) => {}
    );
  }
}
