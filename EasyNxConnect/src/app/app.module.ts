import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { MatDividerModule } from '@angular/material/divider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatListModule} from '@angular/material/list';
import {MatNativeDateModule} from '@angular/material/core';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSliderModule} from '@angular/material/slider';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTreeModule} from '@angular/material/tree';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {MatFormFieldModule} from '@angular/material/form-field';
import { RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule,TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from "ngx-spinner";  

import {
  NgxUiLoaderModule,
  NgxUiLoaderConfig,
  NgxUiLoaderHttpModule,
  SPINNER,
  POSITION,
  PB_DIRECTION,
} from "ngx-ui-loader";

import { ENXLoginComponent } from './pages/enx-login/enx-login.component';
import { EnxGeneralSettingsComponent } from './pages/enx-general-settings/enx-general-settings.component';
import { MenuComponent } from './menu/menu/menu.component';
import { EnxLicenseComponent } from './pages/enx-license/enx-license.component';
import { EnxUserAccountsComponent } from './pages/enx-user-accounts/enx-user-accounts.component';
import { ConfirmDialogComponent } from './pages/confirm-dialog/confirm-dialog.component';
import { UserConfigureSummaryComponent } from './pages/user-configure-summary/user-configure-summary.component';
import { CreateuserComponent } from './pages/createuser/createuser.component';
import { SetPasswordComponent } from './pages/set-password/set-password.component';
import { ConnectorListComponent } from './pages/connector-list/connector-list.component';
import { AmazonS3Component } from './pages/amazon-s3/amazon-s3.component';
import { BoxConnectorComponent } from './pages/box-connector/box-connector.component';
import { AzureBlobComponent } from './pages/azure-blob/azure-blob.component';
import { DropBoxComponent } from './pages/drop-box/drop-box.component';
import { EmailComponent } from './pages/email/email.component';
import { NintexComponent } from './pages/nintex/nintex.component';
import { UIPathComponent } from './pages/uipath/uipath.component';
import { HomeFolderComponent } from './pages/home-folder/home-folder.component';
import { EgnyteComponent } from './pages/egnyte/egnyte.component';
import { FTPComponent } from './pages/ftp/ftp.component';
import { FaxComponent } from './pages/fax/fax.component';
import { OneDriveComponent } from './pages/one-drive/one-drive.component';
import { SharePointOnlineComponent } from './pages/share-point-online/share-point-online.component';
import { JobManagementComponent } from './pages/job-management/job-management.component';
import { JobGroupManagementComponent } from './pages/job-group-management/job-group-management.component';
import { TreeModule } from '@circlon/angular-tree-component';
import { PrinterConnectorComponent } from './pages/printer-connector/printer-connector.component';
import { ExceptionsComponent } from './pages/exceptions/exceptions.component';
import { OCRConnectorComponent } from './pages/ocrconnector/ocrconnector.component';
import { EditJobGroupsComponent } from './pages/edit-job-groups/edit-job-groups.component';
import { WebLoginComponent } from './pages/web-login/web-login.component';
import { DocuwareComponent } from './pages/docuware/docuware.component';


const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  fgsColor: "#435a9b",
  overlayColor: "rgba(255,255,255,0.3)",
  // fgsPosition: POSITION.bottomCenter,
  blur: 8,
  fgsSize: 60,
  fgsType: SPINNER.ballSpinClockwise, // background spinner type
  // fgsType: SPINNER.chasingDots, // foreground spinner type
  pbColor: "#435a9b",
  pbDirection: PB_DIRECTION.leftToRight, // progress bar direction
  pbThickness: 3, // progress bar thickness
};
@NgModule({
  declarations: [
    AppComponent,
    ENXLoginComponent,
    EnxGeneralSettingsComponent,
    MenuComponent,
    EnxLicenseComponent,
    EnxUserAccountsComponent,
    ConfirmDialogComponent,
    UserConfigureSummaryComponent,
    CreateuserComponent,
    SetPasswordComponent,
    ConnectorListComponent,
    AmazonS3Component,
    BoxConnectorComponent,
    AzureBlobComponent,
    DropBoxComponent,
    EmailComponent,
    NintexComponent,
    UIPathComponent,
    HomeFolderComponent,
    EgnyteComponent,
    FTPComponent,
    FaxComponent,
    OneDriveComponent,
    SharePointOnlineComponent,
    JobManagementComponent,
    JobGroupManagementComponent,
    PrinterConnectorComponent,
    ExceptionsComponent,
    OCRConnectorComponent,
    EditJobGroupsComponent,
    WebLoginComponent,
    DocuwareComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    MatCardModule,MatToolbarModule,MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatTableModule,
    MatDividerModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatOptionModule,
    MatProgressSpinnerModule,
    FormsModule,  ReactiveFormsModule,
    MatAutocompleteModule,
    MatBadgeModule,
    NgxSpinnerModule,
    MatBottomSheetModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    TreeModule,
    HttpClientModule,
    MatFormFieldModule,
    RouterModule,
    BrowserAnimationsModule,
    NgbModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    }),
    
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    NgxUiLoaderHttpModule.forRoot({ showForeground: true }),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function httpTranslateLoader(http: HttpClient) {
  // return new TranslateHttpLoader(http);
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");


}
