import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder,FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { StoredataService } from 'src/app/storedata.service';
import { ApiService } from 'src/app/_api/api.service';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

@Component({
  selector: 'app-ocrconnector',
  templateUrl: './ocrconnector.component.html',
  styleUrls: ['./ocrconnector.component.scss']
})
export class OCRConnectorComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
  {
    return !this.isDirty
  }
  isDirty=false;

  // OCRForm: FormGroup;

  OCRengine:any;
  OmniPageserver:any;
  Omnipageserverpath:any;
  microsoftocr:any;
  username:string="";
  hide = true;
  domain:string="";
  password:string="";
  cloudRegion:any;
  subscriptionkey:any;
  errorCodes:any = {};
  mapOCRJobs:boolean=false;
  displayAddOCR="none";
  checkval:any;
  loadJob:boolean=true;
  Repository:any;
  Joblist:any;
  IsAllChecked:any;
  constructor(private dataStore:StoredataService,private ngxLoader:NgxUiLoaderService,private service:ApiService,private router:Router,private notification:NotificationService,
    private ErrorCodes:ErrorCodesService,private fb: UntypedFormBuilder,public dialog: MatDialog,public translate: TranslateService) { 
      // this.OCRForm = this.fb.group({
      //   password: ['', Validators.required],
  
      // });
    }

  ngOnInit(): void {

    let returnUrl:any = localStorage.getItem('PreferredLanguage');
    this.translate.use(returnUrl);
    
    this.errorCodes=this.ErrorCodes.errormsgs();
this.LoadOCR();
  }


  LoadOCR()
  {
    let url="api/OCRConnectorSettings/LoadOCRSetting";
    let postData={
      Engine: localStorage.getItem('OCRengine'),
    }
    this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp=>
      {
        this.OCRengine = resp.Engine;
        this.cloudRegion = resp.Region;
        this.subscriptionkey = resp.MSSubscriptionKey;
        this.Omnipageserverpath = resp.Omnipageserverpath;
        this.username = resp.Username;
        this.password = resp.Password;
        this.domain = resp.Domain;
        localStorage.setItem('OCRengine', this.OCRengine);
        if (this.OCRengine == 'Kofax/Nuance OPS') {
            this.OmniPageserver = true;
            this.microsoftocr = false;
        }
        else if (this.OCRengine == 'Microsoft OCR') {
            this.OmniPageserver = false;
            this.microsoftocr = true;
        }
        else {
            this.OmniPageserver = false;
            this.microsoftocr = false;
        }
        this.ngxLoader.stop();
      },
      (error)=>
      {
        this.ngxLoader.stop();

      });
  }
  SaveOCRConnector()
  {
    this.isDirty=false;
    if (localStorage.getItem('OCRengine') == 'Microsoft OCR') {
      if ((this.cloudRegion == "" || this.subscriptionkey == "") ||
          (this.cloudRegion == undefined || this.subscriptionkey == undefined)) {
          
          this.notification.openSnackBar(this.errorCodes["WAR035"].msg,"","warning-snackbar");
          return;
      }
  }
  if (localStorage.getItem('OCRengine') == 'Kofax/Nuance OPS') {
      if (this.Omnipageserverpath == "" || this.Omnipageserverpath == undefined) {
          
          this.notification.openSnackBar(this.errorCodes["WAR035"].msg,"","warning-snackbar");

          return;
      }
  }
  let url="api/OCRConnectorSettings/SaveSetting";
  let postData={
    Engine: localStorage.getItem('OCRengine'),
                    Region: this.cloudRegion,
                    MSSubscriptionKey: this.subscriptionkey,
                    Omnipageserverpath: this.Omnipageserverpath,
                    Username: this.username,
                    Password: this.password,
                    Domain: this.domain
  }
  this.service.post(url,postData).subscribe(resp=>
    {
      if (resp == true) {
        // this.OCRengine = '';
        this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
window.location.reload();
    }
    else {
        
        this.notification.openSnackBar(this.errorCodes["WAR029"].msg,"","warning-snackbar");

    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
    })
  }
  OCRIntegration(val:any)
  {
    this.OCRengine = val;
    localStorage.setItem('OCRengine', this.OCRengine);
    if (this.OCRengine == 'Kofax/Nuance OPS') {
        this.OmniPageserver = true;
        this.microsoftocr = false;
    }
    else if (this.OCRengine == 'Microsoft OCR') {
        this.OmniPageserver = false;
        this.microsoftocr = true;
    }
    else {
        this.OmniPageserver = false;
        this.microsoftocr = false;
    }
    this.mapOCRJobs = false;
  }

  AddOCRConnector()
  {

let url="api/OCRConnectorSettings/GetJobs";
this.service.getAPI(url).then(resp=>
  {
this.loadJob=false;
this.Repository = resp;
console.log(this.Repository);
// this.Joblist = resp.Joblist;
  })
  this.displayAddOCR="block";

  }
  isAllSelected(val:any)
  {
    this.checkval = val;

    this.IsAllChecked = this.Repository.every(function(item:any) {
      return item.Selected == true;
    })
  // this.getCheckedItemList();
  }
  CheckUncheckAll(val:any)
  {
    this.checkval = val;
    for (var i = 0; i < this.Repository.length; i++) {
      this.Repository[i].Selected = this.IsAllChecked;
    }
    this.getCheckedItemList();
  }
   ENXJob:any = [];
   jobList:any = [];
            
  getCheckedItemList(){
    for (var i = 0; i < this.Repository.length; i++) {
      if(this.Repository[i].Selected)
      this.jobList.push(this.Repository[i].jobList);
    }
    // this.checkedList = JSON.stringify(this.checkedList);
  }
  
  OCRJobsUpdate()
  {
    if (this.checkval == undefined) {
      this.displayAddOCR="none";
window.location.reload();
      return;
  }
  for (var i = 0; i < this.Repository.length; i++) 
  {
  this.jobList = this.Repository[i].Joblist;
  var enxJob:any = {};
    if(this.Repository[i].Selected){
      if (this.jobList.ConnectorParameters != null) {
        if (this.jobList.ConnectorParameters.indexOf('-ocr') >= 0) {
          enxJob['ConnectorParameters'] = this.jobList.ConnectorParameters;
      }
      else {
          enxJob['ConnectorParameters'] = this.jobList.ConnectorParameters + " -ocr";
      }
      }
      else{
        this.jobList.ConnectorParameters = "-ocr";
        enxJob['ConnectorParameters'] = this.jobList.ConnectorParameters;
      }
    }   
    else {
      if (this.jobList.ConnectorParameters == null) {
          enxJob['ConnectorParameters'] = "";
      }
      else {
          enxJob['ConnectorParameters'] = this.jobList.ConnectorParameters.replace("-ocr", "");
      }
  }
  enxJob['ButtonColor'] = this.jobList.ButtonColor;
                    enxJob['ConnectorName'] = this.jobList.ConnectorName;
                    enxJob['ConnectorXmlFilename'] = this.jobList.ConnectorXmlFilename;
                    enxJob['Description'] = this.jobList.Description;
                    enxJob['Id'] = this.jobList.Id;
                    enxJob['JobReference'] = this.jobList.JobReference;
                    enxJob['MetadataEntry'] = this.jobList.MetadataEntry;
                    enxJob['MetadataName'] = this.jobList.MetadataName;
                    enxJob['MetadataType'] = this.jobList.MetadataType;
                    enxJob['Name'] = this.jobList.Name;
                    enxJob['ParentId'] = this.jobList.ParentId;
                    enxJob['Profile'] = this.jobList.Profile;
                    this.ENXJob.push(enxJob);
  }
  let url="api/OCRConnectorSettings/OcrjobSaveSettings";
  let postData={
    Jobs: this.ENXJob
  }
  this.service.post(url,postData).subscribe(resp=>
    {
      this.ENXJob = "";
      if (resp == true) {
        
        this.notification.openSnackBar(this.errorCodes["SUC010"].msg,"","success-snackbar");
        this.displayAddOCR="none";
    }
    else {
        this.notification.openSnackBar(this.errorCodes["WAR058"].msg,"","warning-snackbar");
        this.displayAddOCR="none";
    }

    },(error)=>
    {
      this.displayAddOCR="none";
    })
  }
  Regionchange(val:any)
  {

  }
  Close()
  {
    this.displayAddOCR="none";
  }
}
