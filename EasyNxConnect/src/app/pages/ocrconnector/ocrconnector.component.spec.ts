import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OCRConnectorComponent } from './ocrconnector.component';

describe('OCRConnectorComponent', () => {
  let component: OCRConnectorComponent;
  let fixture: ComponentFixture<OCRConnectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OCRConnectorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OCRConnectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
