import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, QueryList, Sanitizer, ViewChild, ViewChildren } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { environment } from 'src/environments/environment';
// import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

export interface UserInfo{
  Email:any;
  Username:any;
  ProviderCode:any;
}
@Component({
  selector: 'app-web-login',
  templateUrl: './web-login.component.html',
  styleUrls: ['./web-login.component.scss']
})
export class WebLoginComponent implements OnInit {
  errorCodes:any = {};
  Loginuserdetail:any=[];
  loginusername:any;
  url:any;
  btnBox:boolean=true;
  btnODB:boolean=true;
  btnSPO:boolean=true;
  btnDBOX:boolean=true;
  btnEgnyte:boolean=true;
  btnUIPath:boolean=true;
  LogUserDetail:boolean=false;
  boxurl:any;
  webdialog="none";
  loginPopup:any;
  loginpopupInterval:any;
    dataSource: MatTableDataSource<UserInfo>;

  //  @ViewChild('iframe') keywordsInput:any;
  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sort = new QueryList<MatSort>();
  displayedColumns: string[] = ['Email','Username','ProviderCode','Action'];
  urlSafe:any;
  constructor(private http: HttpClient,public translate: TranslateService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) { 
    this.dataSource = new MatTableDataSource<UserInfo>();

    // this.sanitizer = sanitizer;    

    if (window.addEventListener) {
      window.addEventListener("message", this.LoadMainFunction.bind(this), false);
    } else {
       (<any>window).attachEvent("onmessage", this.LoadMainFunction.bind(this));
    }
 
  }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');
    this.url = environment.hostUrl;
     this.urlSafe= this.url+"mvc/WebLogin/BoxAuthenticate?username="+this.loginusername;;

    this.translate.use(returnUrl);

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.loginusername=localStorage.getItem('username');
    this.LoadMainFunction();
  }
  TotalArrlength:any;
  
  LoadMainFunction()
  {
    let url=this.url+"api/WebLogin/GetUserLoginDetails";
    let postData={
      username:this.loginusername
    }
    this.http.get(url,{params:postData}).subscribe(resp=>{
      if(resp!=null)
      {
        this.Loginuserdetail=resp;
       this.TotalArrlength = this.Loginuserdetail.length;
       var OffCount = 0;
       var IsUserLogin:any;
       if(this.Loginuserdetail.length==0)
       {
        this.LogUserDetail=false;
       }
        this.dataSource = new MatTableDataSource(this.Loginuserdetail);
        for (var i = 0; i < this.Loginuserdetail.length; i++) {
          IsUserLogin = this.Loginuserdetail[i].LoggedIn;
          if (IsUserLogin == true) {
            if(this.Loginuserdetail[i].ProviderCode=='BOX')
            {
            this.btnBox=false;
            }
            if(this.Loginuserdetail[i].ProviderCode=='ODB')
            {
            this.btnODB=false;
            }
            if(this.Loginuserdetail[i].ProviderCode=='SPO')
            {
            this.btnSPO=false;
            }
            if(this.Loginuserdetail[i].ProviderCode=='DROPBOX')
            {
            this.btnDBOX=false;
            }
            if(this.Loginuserdetail[i].ProviderCode=='EGNYTE')
            {
            this.btnEgnyte=false;
            }
            if(this.Loginuserdetail[i].ProviderCode=='UIPATH')
            {
            this.btnUIPath=false;
            }
          }
else{
  OffCount = OffCount + 1;
  if(this.Loginuserdetail[i].ProviderCode=='BOX')
  {
  this.btnBox=true;
  }
  if(this.Loginuserdetail[i].ProviderCode=='ODB')
  {
  this.btnODB=true;
  }
  if(this.Loginuserdetail[i].ProviderCode=='SPO')
  {
  this.btnSPO=true;
  }
  if(this.Loginuserdetail[i].ProviderCode=='DROPBOX')
  {
  this.btnDBOX=true;
  }
  if(this.Loginuserdetail[i].ProviderCode=='EGNYTE')
  {
  this.btnEgnyte=true;
  }
  if(this.Loginuserdetail[i].ProviderCode=='UIPATH')
  {
  this.btnUIPath=true;
  }
}
if (this.TotalArrlength == OffCount) {
  this.LogUserDetail=false;
}
else {
  this.LogUserDetail=true;
}
        }

      }
    });
  }
  BoxAuthenticate()
  {

     this.boxurl=this.url+"mvc/WebLogin/BoxAuthenticate?username="+this.loginusername;

     this.loginPopup=window.open(this.boxurl,'popup','width=600,height=600,align=center');
     
     this.loginpopupInterval=setInterval(()=> this.Checkwindow(), 5000);

  }
  odburl:any;
  ODBAuthenticate()
  {
    this.odburl=this.url+"mvc/WebLogin/OdbAuthenticate?username="+this.loginusername;

    this.loginPopup=window.open(this.odburl,'popup','width=600,height=600,align=center');
    
    this.loginpopupInterval=setInterval(()=> this.Checkwindow(), 5000);
  }
  spourl:any;
  SPOAuthenticate()
  {
    this.spourl=this.url+"mvc/WebLogin/SpoAuthenticate?username="+this.loginusername;

    this.loginPopup=window.open(this.spourl,'popup','width=600,height=600,align=center');
    
    this.loginpopupInterval=setInterval(()=> this.Checkwindow(), 5000);
  }
  dropboxurl:any;
  DropBoxAuthenticate()
  {
    this.dropboxurl=this.url+"mvc/WebLogin/DropBoxAuthenticate?username="+this.loginusername;

    this.loginPopup=window.open(this.dropboxurl,'popup','width=600,height=600,align=center');
    
    this.loginpopupInterval=setInterval(()=> this.Checkwindow(), 5000);
  }
  uipathurl:any;
  UIPathAuthenticate()
  {
    this.uipathurl=this.url+"mvc/WebLogin/UIPathAuthenticate?username="+this.loginusername;

    this.loginPopup=window.open(this.uipathurl,'popup','width=600,height=600,align=center');
    
    this.loginpopupInterval=setInterval(()=> this.Checkwindow(), 5000);
  }
egnyteurl:any;
  EgnyteAuthenticate()
  {
    this.egnyteurl=this.url+"mvc/WebLogin/EgnyteAuthenticate?username="+this.loginusername;

    this.loginPopup=window.open(this.egnyteurl,'popup','width=600,height=600,align=center');
    
    this.loginpopupInterval=setInterval(()=> this.Checkwindow(), 5000);
  }
  Checkwindow()
  {
    if(this.loginPopup!=null)
    {
      if(this.loginPopup.closed)
      {
        if(this.loginpopupInterval)
        {
        clearInterval(this.loginpopupInterval);
        }
        this.LoadMainFunction();
      }
    }
  }
  //Logout
  Userlogout(userName:any,providercode:any)
  {
    
if(providercode=='BOX')
{
  let url=this.url+"api/WebLogin/LogoutFromBox";
  let postData={
    username:userName
  }
  this.ngxLoader.start();
  this.http.get(url,{params:postData}).subscribe(resp=>{
    if (resp == true) {
      this.notification.openSnackBar(this.errorCodes["SUC020"].msg,"","success-snackbar");
      this.btnBox=true;
      this.ngxLoader.stop();
      this.LoadMainFunction();
    }
    else{
      this.notification.openSnackBar(this.errorCodes["WAR022"].msg,"","warning-snackbar");
      this.ngxLoader.stop();
    }
  });
  this.ngxLoader.stop();
}
else if (providercode == "ODB" || providercode == "SPO")
{
  
  let url=this.url+"api/WebLogin/LogoutFromMSSites";
  let postData={
    username:userName,
    providerCode:providercode
  }
  this.ngxLoader.start();
  this.http.get(url,{params:postData}).subscribe(resp=>{
    if (resp == true) {
      this.notification.openSnackBar(this.errorCodes["SUC020"].msg,"","success-snackbar");
      if (providercode == "ODB") {
        this.btnODB=true;
      }
      else if(providercode=="SPO")
      {
        this.btnSPO=true;
      }
      this.ngxLoader.stop();
      this.LoadMainFunction();
    }
    else{
      this.notification.openSnackBar(this.errorCodes["WAR022"].msg,"","warning-snackbar");
      this.ngxLoader.stop();
    }
  });
  this.ngxLoader.stop();
}
else if(providercode=='DROPBOX')
{
  let url=this.url+"api/WebLogin/LogoutFromDropBox";
  let postData={
    username:userName
  }
  this.ngxLoader.start();
  this.http.get(url,{params:postData}).subscribe(resp=>{
    if (resp == true) {
      this.notification.openSnackBar(this.errorCodes["SUC020"].msg,"","success-snackbar");
      this.btnDBOX=true;
      this.ngxLoader.stop();
      this.LoadMainFunction();
    }
    else{
      this.notification.openSnackBar(this.errorCodes["WAR022"].msg,"","warning-snackbar");
      this.ngxLoader.stop();
    }
  });
  this.ngxLoader.stop();
}
else if(providercode=='EGNYTE')
{
  let url=this.url+"api/WebLogin/LogoutFromEgnyte";
  let postData={
    username:userName
  }
  this.ngxLoader.start();
  this.http.get(url,{params:postData}).subscribe(resp=>{
    if (resp == true) {
      this.notification.openSnackBar(this.errorCodes["SUC020"].msg,"","success-snackbar");
      this.btnEgnyte=true;
      this.ngxLoader.stop();
      this.LoadMainFunction();
    }
    else{
      this.notification.openSnackBar(this.errorCodes["WAR022"].msg,"","warning-snackbar");
      this.ngxLoader.stop();
    }
  });
  this.ngxLoader.stop();
}
  }
}
