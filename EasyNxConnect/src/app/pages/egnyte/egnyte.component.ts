import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { environment } from 'src/environments/environment';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

@Component({
  selector: 'app-egnyte',
  templateUrl: './egnyte.component.html',
  styleUrls: ['./egnyte.component.scss']
})
export class EgnyteComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
   {
     return !this.isDirty
   }
   isDirty=false;

  servicelogin:any;
  egnytedomain:any;
  username:string="";
  password:string="";
  apikey:any;
  applicationcsecret:any;
  token:any;
  note:boolean=false;
  logintypeval:any;
  AppSettings:boolean=true;
  appid:string="";
  appsecret:string="";
  redirecturl:string="";
  Homefolder:boolean=true;
  Specificfolder:any;
  RootFolderId:any;
  folder:any;
  enableforservice:boolean=false;
  AdminMail:any;
  InformAdmin:any;
  InformUser:any;
  AdminEmail:any;
  RemindDays:any;
  Reminder:any;
  Remind:any;
  egnytedomainvalue:any;
  logintypevalue:any;
  usernamevalue:any;
  passwordvalue:any;
  apikeyvalue:any;
  applicationcsecretvalue:any;
  tokenvalue:any;
  appidvalue:any;
  appsecretvalue:any;
  redirecturlvalue:any;
  EmailSubject:string="";
  EmailBody:string="";
  
  errorCodes:any = {};
  testusername:any;
  target:any;
  url:any
ListOfUser:any;
userlist:any;
txtuser:any;
  displayStyle = "none";
  folderType:any;

  constructor(private http: HttpClient,public translate: TranslateService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) { }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');

    this.translate.use(returnUrl);
    this.url = environment.hostUrl;

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.LoadEgnyteConnectorSettings();

  }
  LoadEgnyteConnectorSettings()
  {
    let url="api/EgnyteConnectorSettings/LoadEgnyteControllerSetting";
    let postData={
      UserFolder: "",
    }
     this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if(resp!=null)
      {
        this.Homefolder = resp.UserFolder;
                this.Specificfolder = resp.SpecifcFolder;
                this.RootFolderId = resp.RootFolderId;
                this.Remind = resp.Remind;
                this.RemindDays = resp.RemindDays;
                this.InformUser = resp.InformUser;
                this.InformAdmin = resp.InformAdmin;
                this.AdminMail = resp.AdminMail;
                this.egnytedomain = resp.Domain;
                this.logintypeval = resp.Logintype;
                this.username = resp.EgnyteUsername;
                this.password = resp.EgnytePassword;
                this.apikey = resp.EgnyteClientId;
                this.applicationcsecret = resp.EgnyteClientSecret;

                this.token = resp.AccessToken;
                this.appid = resp.AppID;
                this.appsecret = resp.AppSecret;
                this.redirecturl = resp.RedirectURL;
                this.egnytedomainvalue = resp.Domain;
                this.logintypevalue = resp.Logintype;
                this.usernamevalue = resp.EgnyteUsername;
                this.passwordvalue = resp.EgnytePassword;
                this.apikeyvalue = resp.EgnyteClientId;
                this.applicationcsecretvalue = resp.EgnyteClientSecret;
                this.tokenvalue = resp.AccessToken;
                this.appidvalue = resp.AppID;
                this.appsecretvalue = resp.AppSecret;
                this.redirecturlvalue = resp.RedirectURL;
                if (this.logintypeval == "2") {
                    this.servicelogin = true;
                    this.enableforservice = true;
                }
                else {
                    this.servicelogin = false;
                    this.enableforservice = false;
                }
                if (this.Specificfolder == false) {
                    this.folder = true;
                }
                if (this.InformAdmin == false) {
                    this.AdminEmail = true;
                }
              }
      this.ngxLoader.stop();
    },(error)=>{
      this.ngxLoader.stop();
    
    });
    
  }
  SaveEgnyteConnector()
  {
    this.isDirty=false;
    if (this.servicelogin == true) {
      if ((this.username == null || this.password == null || this.apikey == null || this.applicationcsecret == null || this.token == "") || (this.username == "" || this.password == "" || this.apikey == "" || this.applicationcsecret == "" ||this.token == "")) {
          
          this.notification.openSnackBar(this.errorCodes["WAR059"].msg,"","warning-snackbar");
          return;
      }
  }
  if ((this.appid == null || this.appid == "") || (this.appsecret == null || this.appsecret == "") || (this.redirecturl == null || this.redirecturl == "")) {
      
      this.notification.openSnackBar(this.errorCodes["WAR066"].msg,"","warning-snackbar");
      return;
  }
  if (this.egnytedomain == null || this.egnytedomain == "") {
      
      this.notification.openSnackBar(this.errorCodes["WAR060"].msg,"","warning-snackbar");
      return;
  }
  if (this.AdminMail != null && this.AdminMail != "") {
      var reg = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
      if (reg.test(this.AdminMail) == false) {
          
          this.notification.openSnackBar(this.errorCodes["WAR008"].msg,"","warning-snackbar");
          return;
      }
  }
  if (this.Specificfolder == true) {
      if (this.RootFolderId == null || this.RootFolderId == "") {
          
          this.notification.openSnackBar(this.errorCodes["WAR007"].msg,"","warning-snackbar");
          return;
      }
  }
  if (this.InformAdmin == true) {
      if (this.AdminMail == null || this.AdminMail == "") {
         
          this.notification.openSnackBar(this.errorCodes["WAR008"].msg,"","warning-snackbar");
          return;
      }
  }
  if (this.username != null) {
      this.username = this.username;
  }
  else {
      this.username = "";
  }
  if (this.password != null) {
      this.password = this.password;
  }
  else {
      this.password = "";
  }
  if (this.apikey != null) {
      this.apikey = this.apikey;
  }
  else {
      this.apikey = "";
  }

  if (this.applicationcsecret != null) {
      this.applicationcsecret = this.applicationcsecret;
  }
  else {
      this.applicationcsecret = "";
  }
  if (this.token != null) {
      this.token = this.token;
  }
  else {
      this.token = "";
  }
  
  let url="api/EgnyteConnectorSettings/SaveSetting";
  let postData = {
    UserFolder: this.Homefolder,
                    SpecifcFolder: this.Specificfolder,
                    RootFolderId: this.RootFolderId,
                    Remind: this.Remind,
                    RemindDays: this.RemindDays,
                    InformUser: this.InformUser,
                    InformAdmin: this.InformAdmin,
                    AdminMail: this.AdminMail,
                    Domain: this.egnytedomain,
                    Logintype: this.logintypeval,
                    EgnyteUsername: this.username,
                    EgnytePassword: this.password,
                    EgnyteClientId: this.apikey,
                    AccessToken: this.token,
                    AppID: this.appid,
                    AppSecret: this.appsecret,
                    RedirectURL: this.redirecturl,
                    EgnyteClientSecret: this.applicationcsecret
           
  }
  this.ngxLoader.start();
   this.service.post(url,postData).subscribe(resp => {
    if (resp==true) 
      {
        this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
        this.ngxLoader.stop();
this.LoadEgnyteConnectorSettings();
    }
    else {
      this.notification.openSnackBar(this.errorCodes["WAR003"].msg,"","warning-snackbar");
      this.ngxLoader.stop();
  }
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });


  }
  GenerateToken()
  {

    let url="api/EgnyteConnectorSettings/GetOAuthAccessToken";
    let postData = {
      Domain: this.egnytedomain,
      EgnyteUsername: this.username,
      EgnytePassword: this.password,
      EgnyteClientId: this.apikey,
      EgnyteClientSecret: this.applicationcsecret

    }
    this.ngxLoader.start();
     this.service.post(url,postData).subscribe(resp => {
      if (resp.Status=='SUCCESS') 
        {
          this.ngxLoader.stop();
          this.token = resp.Data["Token"];

      }
      else {
        this.notification.openSnackBar(this.errorCodes["WAR056"].msg,"","warning-snackbar");
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
  EgnyteConnector(val:any)
  {
    if (val == '1') {
      this.Homefolder = true;
      this.Specificfolder = false;
      this.folder = true;
  }
  if (val == '2') {
      this.Homefolder = false;
      this.Specificfolder = true;
      this.folder = false;
  }
  }
  LogReminderAdmin(val:any)
  {
    if (val == true) {
      this.AdminEmail = false;
  } else {
      this.AdminEmail = true;
  }
  }
  LogReminder(val:any)
  {
    if (val == true) {
      this.Reminder = false;
      this.RemindDays = "5";
  }
  else {
      this.Reminder = true;
      this.AdminEmail = true;
      this.RemindDays = false;
      this.InformUser = false;
      this.InformAdmin = false;
  }
  }
  ServiceChange()
  {
    this.note = false;
    if (this.servicelogin == true) {
        this.logintypeval = "2";
        this.enableforservice = true;
    }
    else {
        this.logintypeval = "1";
        this.enableforservice = false;
        this.AppSettings = true;
    }
  }
  Infopopup()
  {

  }
  EditSettings()
  {
    this.AppSettings = false;
    this.note = true;
  }
  ResetSettings()
  {
    let url="api/EgnyteConnectorSettings/ResetSetting";
    let postData = {
      ResetAppID: this.appid,
                    ResetAppSecret: this.appsecret,
                    ResetRedirectURL: this.redirecturl
    }
    this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      this.appid = resp.ResetAppID;
                    this.appsecret = resp.ResetAppSecret;
                    this.redirecturl = resp.ResetRedirectURL;
                         this.ngxLoader.stop();   
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();   
    })
  }
  EmailOption()
  {
    let url="api/EgnyteConnectorSettings/LoadEgnyteControllerSetting";
    let postData = {
      EmailSubject: this.EmailSubject,
      EmailBody: this.EmailBody
    }
    this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if ((resp.EmailSubject != "") || (resp.EmailBody != "")) 
        {
          this.EmailSubject = resp.EmailSubject;
          this.EmailBody = resp.EmailBody;
          this.ngxLoader.stop();
      }
      else {
        this.EmailSubject = this.errorCodes["RES003"].res;
        this.EmailBody = this.errorCodes["RES004"].res;
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });

  }
  SaveEmailOption()
  {
    let url="api/EgnyteConnectorSettings/SaveEmailSetting";
    let postData = {
      EmailSubject: this.EmailSubject,
      EmailBody: this.EmailBody
    }
    this.ngxLoader.start();
     this.service.post(url,postData).subscribe(resp => {
      if (resp==true) 
        {
          this.EmailSubject = resp.EmailSubject;
          this.EmailBody = resp.EmailBody;
          this.ngxLoader.stop();
      }
      else {
        this.notification.openSnackBar(this.errorCodes["WAR003"].msg,"","warning-snackbar");
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  
  }
  //MapJopbs
  EgnyteMapJobs()
  {
    if ((this.egnytedomain != this.egnytedomainvalue) || (this.logintypeval != this.logintypevalue) || (this.username != this.usernamevalue)
                    || (this.password != this.passwordvalue) || (this.apikey != this.apikeyvalue) || (this.token != this.tokenvalue) || (this.appid != this.appidvalue) || (this.appsecret != this.appsecretvalue)
                    || (this.redirecturl != this.redirecturlvalue)) {
                    this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");

                    return;
                }
                localStorage.removeItem('MapDoc');
                localStorage.removeItem('mapEmail');
                localStorage.removeItem('BoxMap');
                localStorage.removeItem('SPOMap');
                localStorage.removeItem('ODBMap');
                localStorage.removeItem('FTPMap');
                localStorage.removeItem('FolderMap');
                localStorage.removeItem("AzureBlobMap");
                localStorage.removeItem("UIPathMap");
                localStorage.removeItem("DropBoxMap");

                localStorage.setItem("EgnyteMap", "EgnyteJobs");
                this.router.navigate(['/JobManagement']);

  }
//TestConnectoe
  TestConnector()
  {
    this.displayStyle = "block";
    if ((this.egnytedomain != this.egnytedomainvalue) || (this.logintypeval != this.logintypevalue) || (this.username != this.usernamevalue)
    || (this.password != this.passwordvalue) || (this.apikey != this.apikeyvalue) || (this.token != this.tokenvalue) || (this.appid != this.appidvalue) || (this.appsecret != this.appsecretvalue)
    || (this.redirecturl != this.redirecturlvalue)) {
      this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");
    return;
}
var idval = -1;
                switch (localStorage.getItem("AuthType")) {
                    case "LocalDirectory":
                        idval = 0;
                        break;
                    case "ActiveDirectory":
                        idval = 1;
                        break;
                    case "Docuware":
                        idval = 2;
                        break;
                    case "Egnyte":
                        idval = 3;
                        break;
                    case "MFiles":
                        idval = 4;
                        break;
                    case "CustomTarget":
                        idval = 99;
                        break;
                }
                let url=this.url+"api/User/GetLocalUsersByAuthId"
let postData={
  AuthId:idval
}
this.http.get(url,{params:postData}).subscribe(resp=>{
if(resp!=null)
{
  this.ListOfUser=resp
  if(this.ListOfUser.length>0)
{
  this.ListOfUser=resp;
  this.userlist = true;
  this.txtuser=false;
}
else{ 
  this.userlist = false;
  this.txtuser=true;
}
}
else {
  this.userlist = false;
  this.txtuser = true;
}
  });

  }
  closePopup() {
    this.displayStyle = "none";
  }
  getUser(val:any)
  {
    let userId = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;
    this.testusername=userId;
  }
  setTargetMandatory(val:any)
  {
    if (this.folderType == 'S') {
      $("#tc_egnyte_target_span").html("*");
      $("#tc_egnyte_target_folder").attr("required", 1);
  }
  else {
      $("#tc_egnyte_target_span").html("");
      $("#tc_egnyte_target_folder").attr("required", 0);
  }
  }
  Test(testusername:any,target:any)
  {
    if (target == undefined || target == null) {
      if (this.folderType == "S") {
          this.notification.openSnackBar(this.errorCodes["WAR088"].msg,"","warning-snackbar");
          return;
      }
      else
          target = "";
  }
  var args = "-t=" + '"' + target + '" ';

                if (this.folderType && this.folderType.trim().length > 0) {
                    args += " -type=" + '"' + this.folderType + '"';
                }
                else
                    args += ' -type="P"';

                    let url="api/User/TestConnector";
let postData={
  ProviderCode: "EGNYTE",
                    UserName: this.testusername,
                    Arguments: args
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
  {
if(resp.Status=='SUCCESS')
{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
else{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });
  }
}
