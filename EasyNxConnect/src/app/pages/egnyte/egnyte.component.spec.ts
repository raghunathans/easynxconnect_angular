import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EgnyteComponent } from './egnyte.component';

describe('EgnyteComponent', () => {
  let component: EgnyteComponent;
  let fixture: ComponentFixture<EgnyteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EgnyteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EgnyteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
