import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnxUserAccountsComponent } from './enx-user-accounts.component';

describe('EnxUserAccountsComponent', () => {
  let component: EnxUserAccountsComponent;
  let fixture: ComponentFixture<EnxUserAccountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnxUserAccountsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnxUserAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
