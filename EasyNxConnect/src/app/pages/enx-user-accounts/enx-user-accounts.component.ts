import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { post } from 'jquery';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ConfirmationDialog, DialogData, NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { StoredataService } from 'src/app/storedata.service';

import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { EnxLicenseComponent } from '../enx-license/enx-license.component';
import {MatDialogModule} from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

 export interface PrimaryAuthProvider
 {
   ProviderName:string;
   PrimaryAuthType:string;
 }
@Component({
  selector: 'app-enx-user-accounts',
  templateUrl: './enx-user-accounts.component.html',
  styleUrls: ['./enx-user-accounts.component.scss']

})

export class EnxUserAccountsComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
   {
     return !this.isDirty
   }
   isDirty=false;

  authenticationList:PrimaryAuthProvider[]=[];
  AuthName:string="";
  ByPassPassword:boolean=false;
  LDir:boolean=false;
  AD:boolean=false;
  Docuwareshow:boolean=false;
  MFileshow:boolean=false;
  ShowCustomTarget:boolean=false;
  LaserFiche:boolean=false;
  btnsaveDisable:boolean=true;
  btnDisable:boolean = true;
  btnreverrt:boolean=true;
   //LD
   LDAdminusername:string="";
   LDpassword:string="";
   //AD
   ADAdminuser:string="";
   ADAdminpassword:string="";
   Adserver:string="";
   Adfilter:string="";
   AdGroupfilter:string="";
   IncludeGroups:boolean=false;
   searchlimit:string="";
   IncludeGroupsAndUsers:boolean=false;
   LDAdminusernamevalue:string="";
   LDpasswordvalue:string="";
   Adminusernamevalue:string="";
   passwordvalue:string="";
   Adserverurl:string="";
   Adfilterval:string="";
   IncludeGroupsval:any;
   AdGroupfilterval:string="";
   IncludeGroupsvalue:any;
   searchlimitvalue:string="";  
   IncludeGroupsAndUsersvalue:any;
   Nfcauth:any;
   Authnamechk:string="";
   Nfcauthvalue:any;
   errorCodes:any = {};
   btnrepository:boolean = false;
   btnrepo = true;
   ShowAuthentication:boolean=false;
  constructor(public translate: TranslateService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private notification:NotificationService,private ErrorCodes:ErrorCodesService,public dialog: MatDialog,public dataStore:StoredataService) { }
  
  ngOnInit(): void {
    if (localStorage.getItem("Role") != "SuperAdmin") {
      this.ShowAuthentication = true;
  }
    this.loadAuthentication();
    this.loadUserAccountssettings();
    this.errorCodes=this.ErrorCodes.errormsgs();
    let returnUrl:any = localStorage.getItem('PreferredLanguage');

    this.translate.use(returnUrl);

  }
  loadAuthentication()
  {
    let url="api/User/GetPrimaryProviders";
    this.service.getAPI(url).then(resp=>{
this.authenticationList=resp;
    });

    var CustomProductType = localStorage.getItem('CustomProductType');
    if (CustomProductType == 'CUSTOM' || CustomProductType == 'custom') {
        for (var i = 0; i < this.authenticationList.length; i++) {
            if (this.authenticationList[i].PrimaryAuthType == "CustomTarget") {
              this.authenticationList.push(this.authenticationList[i]);
                break;
            }
        }
    }
    else {
        for (var i = 0; i < this.authenticationList.length; i++) {
            if (this.authenticationList[i].PrimaryAuthType != "CustomTarget") {
              this.authenticationList.push(this.authenticationList[i]);
            }
        }
    }

     url="api/User/LoadUserAccountSetting";
     let postData={
      DynamicUsers: "",
     }
     this.service.post(url,postData).subscribe(resp=>{
if(resp!=null)
{
  this.AuthName = resp.Authentication;

  if (this.AuthName == "ActiveDirectory") {
     this.AD = true;
    this.ByPassPassword = true;
}
else {
     this.AD = false;
}
if (this.AuthName == "LocalDirectory") {
   this.LDir = true;
    this.ByPassPassword = true;
}
else {
     this.LDir = false;
}
}
     });
  }
  loadUserAccountssettings()
  {
    let url="api/User/LoadUserAccountSetting";
    let postData={
      Authentication: "",
    }
    this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp=>{
console.log();
if(resp!=null)
{
  this.AuthName = resp.Authentication
                this.Authnamechk = resp.Authentication
                this.IncludeGroups = resp.IncludeGroups;
                this.AdGroupfilter = resp.AdGroupfilter;
                this.CheckDynamicUsers = resp.DynamicUsers;
                this.Adserver = resp.Adserver;
                this.Adfilter = resp.Adfilter;
                this.ADAdminuser = resp.ADAdminuser;
                this.ADAdminpassword = resp.ADAdminpassword;
                this.LDAdminusername = resp.LDUsername;
                this.LDpassword = resp.LDPassword;
                this.IncludeGroupsAndUsers = resp.IncludeGroupsAndUsers;
                this.searchlimit = resp.Searchlimit;

                this.LDAdminusernamevalue = resp.LDUsername;
                this.LDpasswordvalue = resp.LDPassword;

                this.Adminusernamevalue = resp.ADAdminuser;
                this.passwordvalue = resp.ADAdminpassword;
                this.Adserverurl = resp.Adserver;
                this.Adfilterval = resp.Adfilter;
                this.IncludeGroupsval = resp.IncludeGroups;
                this.AdGroupfilterval = resp.AdGroupfilter;
                this.IncludeGroupsvalue = resp.IncludeGroups;
                this.searchlimitvalue = resp.Searchlimit;
                this.IncludeGroupsAndUsersvalue = resp.IncludeGroupsAndUsers;
                this.Nfcauth = resp.ByPassPassword;

                localStorage.setItem('Searchlimit', this.searchlimit);
                this.ByPassPassword = false;
                if (this.Nfcauth == "1") {
                  this.Nfcauth = true;
                  this.Nfcauthvalue = true;
              }
              else {
                  this.Nfcauth = false;
                  this.Nfcauthvalue = false;
              }
              if (this.AuthName != null) {
                this.btnDisable = false;
            }
              if (this.AuthName == "ActiveDirectory") {
                this.AD = true;
                this.ByPassPassword = true;
            }
            else {
                this.AD = false;
            }
            if (this.AuthName == "LocalDirectory") {
              this.LDir = true;
              this.ByPassPassword = true;
            }
            else {
              this.LDir = false;
            }
            
            if (this.AuthName == "Docuware") {
              this.Docuwareshow = true;
              this.DocuwareLoad();
          }
          if (this.AuthName == "MFiles") {
              this.MFileshow = true;
              this.MFileLoad();
          }
          if (this.AuthName == "CustomTarget") {
              this.ShowCustomTarget = true;
              this.btnsaveDisable = false;
              this.CustomTargetLoad();
          }
          if (this.AuthName == "LaserFiche") {
              this.LaserFiche = true;
              this.LaserFicheLoad();
          }
}

this.ngxLoader.stop();
    },(error)=>{
      console.log(error);
      this.ngxLoader.stop();
    });
  }
  // Docuware
  docurl:string="";
  docorg:string="";
  userName:string="";
  docuwarepassword:string="";
  url:string="";
  organization="";
  userNamevalue="";
  docuwarepasswordvalue="";
  DocuwareLoad()
  {
if(this.AuthName=='Docuware')
{
let url="api/DocuwareSettings/LoadDocuwareSetting";
let postData={
  URL: ""
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
  {
    if(resp!=null)
    {
      this.docurl = resp.URL;
      this.docorg = resp.Organization;
      this.url = resp.URL;
      this.userName = resp.Username;
      this.docuwarepassword = resp.Password;
      this.organization = resp.Organization;
  
      this.userNamevalue = resp.Username;
      this.docuwarepasswordvalue = resp.Password;
      this.ngxLoader.stop();
    }
    else{
      this.notification.openSnackBar(this.errorCodes["TC16"].msg,"","red-snackbar");
      this.ngxLoader.stop();
    }
    
  },(error)=>
  {
    this.ngxLoader.stop();
  });
}
  }

  //Validation during change the input value.
  Docusernamechnage() {
    if (this.userNamevalue != this.userName) {
        this.btnsaveDisable = false;
    }
}
Docpasswordchnage() {
    if (this.docuwarepasswordvalue != this.docuwarepassword) {
        this.btnsaveDisable = false;
    }
}
Docurlchnage() {
    if (this.docurl != this.url) {
        this.btnsaveDisable = false;
    }
}
Docorganizationchnage() {
    if (this.docorg != this.organization) {
        this.btnsaveDisable = false;
    }
}
NFCUserAuthenticate() {               
    if (this.Nfcauthvalue != this.Nfcauth) {
        this.btnsaveDisable = false;
    }
}
//M-Files
MFDomain:string="";
MFVault:string="";
RootFolderId:string="";
domain:string="";
username:string="";
Mfilepassword:string="";
vault:string="";
class:string="";
usernameval:string="";
Mfilepasswordval:string="";
classval:string="";
  MFileLoad()
  {
if(this.AuthName=='MFiles')
{
let url="api/MFilesConnectorSettings/LoadSettings";
let postData={
  RootFolderId: "",
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>{
if(resp!=null)
{
  this.MFDomain = resp.DomainName;
  this.MFVault = resp.Vault;
  this.RootFolderId = resp.RootFolderId;
  this.domain = resp.DomainName;
  this.username = resp.Username;
  this.Mfilepassword = resp.Password;
  this.vault = resp.Vault;
  this.class = resp.Class;
  this.usernameval = resp.Username;
  this.Mfilepasswordval = resp.Password;
  this.classval = resp.Class;
  this.ngxLoader.stop();
}
else{
  this.notification.openSnackBar(this.errorCodes["TC16"].msg,"","red-snackbar");
  this.ngxLoader.stop();
}
},(error)=>
{
  this.notification.openSnackBar(error.message,"","red-snackbar");
  this.ngxLoader.stop();
});
}
  }
  //Validation during change the input value.
  Mfileusernamechnage() {
    if (this.usernameval != this.username) {
        this.btnsaveDisable = false;
    }
}
Mfilepasswordchnage() {
    if (this.Mfilepasswordval != this.Mfilepassword) {
        this.btnsaveDisable = false;
    }
}
Mfiledomainchnage() {
    if (this.MFDomain != this.domain) {
        this.btnsaveDisable = false;
    }
}
Mfilevaultchnage() {
    if (this.MFVault != this.vault) {
        this.btnsaveDisable = false;
    }
}
Mfileclasschnage() {
    if (this.classval != this.class) {
        this.btnsaveDisable = false;
    }
}
  //Method for loading the CustomTarget settings based on the authentication type.
  customserveraddress:string="";
  adminusername:string="";
  adminpassword:string="";
  parameters:string="";
  dynamicjobs:string="";
  customdynamicjobs:string="";
  AutoJobMapping:string="";
  CustomTargetLoad()
  {
if(this.AuthName=='CustomTarget')
{
let url="api/CustomTarget/LoadcustomtargetSetting";
let postData={
  CustomServerAddress: "",
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>{
if(resp!=null)
{
  this.customserveraddress = resp.CustomServerAddress;
                        this.adminusername = resp.Adminusername;
                        this.adminpassword = resp.Adminpassword;
                        this.parameters = resp.Parameters;
                        this.dynamicjobs = resp.DynamicJobs;
                        this.customdynamicjobs = resp.CustomDynamicJobs;
                        this.AutoJobMapping = resp.AutoJobMapping;

                        if (resp.DynamicJobs == true)
                            localStorage.setItem('DynamicJobs', 'true');
                        else
                            localStorage.setItem('DynamicJobs', 'false');

                        if (resp.CustomDynamicJobs == true)
                            localStorage.setItem('CustomDynamicJobs', 'true');
                        else
                            localStorage.setItem('CustomDynamicJobs', 'false');

                        if (resp.AutoJobMapping == true)
                            localStorage.setItem('AutoJobMapping', 'true');
                        else
                            localStorage.setItem('AutoJobMapping', 'false');

                        localStorage.setItem('CustomTargetName', resp.TargetName);
                        localStorage.setItem('CustomMapControlTitle', resp.MapControlTitle);
                        localStorage.setItem('CustomTargetLabel', resp.TargetLabel);
                        localStorage.setItem('ConnectorName', resp.ConnectorName);
                        localStorage.setItem('ConnectorDescription', resp.Description);
                        localStorage.setItem('ConnectorPrefix', resp.Prefix);
                        localStorage.setItem('ConnectorSuffix', resp.Suffix);
                        localStorage.setItem('ConnectorXmlFileName', resp.XmlFileName);
                        localStorage.setItem('ConnectorFileFormat', resp.FileFormat);

  this.ngxLoader.stop();
}
else{
  this.notification.openSnackBar(this.errorCodes["TC16"].msg,"","red-snackbar");
  this.ngxLoader.stop();
}
},(error)=>
{
  this.notification.openSnackBar(error.message,"","red-snackbar");
  this.ngxLoader.stop();
});

}
  }
  //LF
  LFServer:string="";
  LFServerAddress:string="";
  LFCloudChk:any;
  LFCloudCheck:any;
  LFAdminusername:string="";
  LFPassword:string="";
  LFRepository:any;
  LFAdminusernamevalue:string="";
  LFPasswordvalue:string="";
  LFRepositoryvalue:string="";
  sname:any;
  LaserFicheLoad()
  {
if(this.AuthName=='LaserFiche')
{
  let url="api/LaserFicheSettings/LoadLaserControllerSetting";
  let postData={
    LFServerAddress: "",
  }
  this.ngxLoader.start();
  this.service.post(url,postData).subscribe(resp=>{
  if(resp!=null)
  {
    this.LFServer = resp.LFServerAddress;
                        this.LFServerAddress = resp.LFServerAddress;
                        this.LFCloudChk = resp.LFCloudCheck;
                        this.LFCloudCheck = resp.LFCloudCheck;
                        this.LFAdminusername = resp.LFAdminusername;
                        this.LFPassword = resp.LFPassword;
                        this.LFRepository = resp.LFRepository;

                        this.LFAdminusernamevalue = resp.LFAdminusername;
                        this.LFPasswordvalue = resp.LFPassword;
                        this.LFRepositoryvalue = resp.LFRepository;

                        if (this.LFCloudCheck == true) {
                            let x = "Account Id";
                            this.sname=x;
                            this.btnrepository = true;

                        }
                        else {
                            let x = "Server";
                            this.sname=x;
                            this.btnrepository = false;
                        }
    this.ngxLoader.stop();
  }
  else{
    this.notification.openSnackBar(this.errorCodes["TC16"].msg,"","red-snackbar");
    this.ngxLoader.stop();
  }
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });
}
  }
              //Validation during change the input value.
              LFusernamechange() {
                if (this.LFAdminusernamevalue != this.LFAdminusername) {
                    this.btnsaveDisable = false;
                }
            }
            LFpasswordchange() {
                if (this.LFPasswordvalue != this.LFPassword) {
                    this.btnsaveDisable = false;
                }
            }
            LFserverchange() {
                if (this.LFServer != this.LFServerAddress) {
                    this.btnsaveDisable = false;
                }
            }
            LFrepochange() {
                if (this.LFRepositoryvalue != this.LFRepository) {
                    this.btnsaveDisable = false;
                }
            }
AuthenticationChangevalue(event:any)
{
this.ngxLoader.start();
if ((event.value == undefined) || (event.value == "")) {
  this.btnsaveDisable = true;
  return;
}
this.ByPassPassword = false;
this.btnDisable = true;
this.Docuwareshow = false;
this.btnsaveDisable = false;
if (this.Authnamechk == event.value) {
  this.btnDisable = false;
}
if(event.value=="LocalDirectory")
{
this.LDir=true;
this.Docuwareshow=false;
this.AD=false;
this.MFileshow=false
this.ShowCustomTarget=false;
this.LaserFiche=false;
this.btnreverrt = false;
this.ByPassPassword = true;
}
else if(event.value=="ActiveDirectory")
{
this.AD=true;
this.Docuwareshow=false;
this.LDir=false;
this.MFileshow=false
this.ShowCustomTarget=false;
this.LaserFiche=false;
this.btnreverrt = false;
this.ByPassPassword = true;
}
else if(event.value=="Docuware")
{
  this.Docuwareshow=true;
  this.LDir=false;
  this.AD=false;
  this.MFileshow=false
this.ShowCustomTarget=false;
this.LaserFiche=false;
this.DocuwareLoad();
}
else if(event.value=="MFiles")
{
  this.MFileshow=true;
  this.ShowCustomTarget=false;
  this.Docuwareshow=false;
  this.LDir=false;
  this.AD=false;
  this.LaserFiche=false;
  this.MFileLoad();
}
else if(event.value=="CustomTarget")
{
  this.ShowCustomTarget=true;
  this.Docuwareshow=false;
  this.LDir=false;
  this.AD=false;
  this.MFileshow=false
this.LaserFiche=false;
this.CustomTargetLoad();
}
else if(event.value=="LaserFiche")
{
  this.LaserFiche=true;
  this.Docuwareshow=false;
  this.LDir=false;
  this.AD=false;
  this.MFileshow=false
this.ShowCustomTarget=false;
this.btnDisable = true;
this.LaserFicheLoad();
}
this.ngxLoader.stop();
  }
  ADflag:boolean = false;
  Docflag:any;
  MFilesflag:any;
  LFflag:any;
  Save()
  {
    
    if (this.Nfcauth == true) {
      this.Nfcauth = "1";
  }
  else {
      this.Nfcauth = "0";
  }
  //Docuware validations

  if (this.AuthName == "Docuware") {
    if (this.url == null || this.url == "") {
        this.notification.openSnackBar(this.errorCodes["WAR064"].msg,"","warning-snackbar");
        return false;
    }
    if (this.userName == null || this.userName == "") {
        this.notification.openSnackBar(this.errorCodes["WAR043"].msg,"","warning-snackbar");
        return false;
    }
    if (this.docuwarepassword == null || this.docuwarepassword == "") {
        this.notification.openSnackBar(this.errorCodes["WAR044"].msg,"","warning-snackbar");
        return false;
    }
    if (this.organization == null || this.organization == "") {
        this.notification.openSnackBar(this.errorCodes["WAR065"].msg,"","warning-snackbar");
        return false;
    }
    if ((this.docurl != this.url) || (this.docorg != this.organization)) {
      
      const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
        data: {
          title: this.translate.instant('CTRUserConfigure.WarningText'),
          message: this.translate.instant('CTRUserConfigure.DocuwareWarningText')
        }
      });
      confirmDialog.afterClosed().subscribe(result => {
        if (result === true) {
          this.Docflag = true;
            this.DocuwareSaveFunction();
        }
      });
    }
    else {
        this.DocuwareSaveFunction();
    }
    return;
}
//MFiles
//Mfiles Validations
if (this.AuthName == "MFiles") {
  if (this.username == null || this.username == "") {
      this.notification.openSnackBar(this.errorCodes["WAR043"].msg,"","warning-snackbar");
      return false;
  }
  if (this.Mfilepassword == null || this.Mfilepassword == "") {
      this.notification.openSnackBar(this.errorCodes["WAR044"].msg,"","warning-snackbar");
      return false;
  }
  if (this.domain == null || this.domain == "") {
      this.notification.openSnackBar(this.errorCodes["WAR045"].msg,"","warning-snackbar");
      return false;
  }
  if (this.vault == null || this.vault == "") {
    this.notification.openSnackBar(this.errorCodes["WAR046"].msg,"","warning-snackbar");
    return false;
  }
  if ((this.MFDomain != this.domain) || (this.MFVault != this.vault)) {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: this.translate.instant('CTRUserConfigure.WarningText'),
        message: this.translate.instant('CTRUserConfigure.MFilesWarningText')
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.MFilesflag = true;
          this.MFilesSaveFunction();
      }
    });
  }
  else {
      this.MFilesSaveFunction();
  }     
  return;
}

//CustomTarget

if (this.AuthName == "CustomTarget") {
  if (this.adminusername == null || this.adminusername == "") {
      this.notification.openSnackBar(this.errorCodes["WAR043"].msg,"","warning-snackbar");
      return false;
  }
  if (this.customserveraddress == null || this.customserveraddress == "") {
    this.notification.openSnackBar(this.errorCodes["WAR043"].msg,"","warning-snackbar");
      return false;
  }
  if (this.adminpassword == null || this.adminpassword == "") {
    this.notification.openSnackBar(this.errorCodes["WAR044"].msg,"","warning-snackbar");
      return false;
  }
  this.CustomTargetSaveFunction();
  return;
}
//LF
if (this.AuthName == "LaserFiche") {                   
  if ((this.LFAdminusername == null || this.LFPassword == null || this.LFServerAddress == null) ||
      (this.LFAdminusername == "" || this.LFPassword == "" || this.LFServerAddress == "") ||
      (this.LFAdminusername == undefined || this.LFPassword == undefined || this.LFServerAddress == undefined)) {
        this.notification.openSnackBar(this.errorCodes["WAR075"].msg,"","warning-snackbar");
      return;
  }
  if (this.LFCloudCheck == false || this.LFCloudCheck == undefined || this.LFCloudCheck==null) {
      if (this.LFRepository == null || this.LFRepository=="") {
        this.notification.openSnackBar(this.errorCodes["WAR075"].msg,"","warning-snackbar");
          return;
      }
  }
  if ((this.LFServer != this.LFServerAddress) || (this.LFCloudChk != this.LFCloudCheck)){

    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: this.translate.instant('CTRUserConfigure.WarningText'),
        message: this.translate.instant('CTRUserConfigure.LFWarningText')
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.LFflag = true;
          this.LaserFicheSaveFunction();
      }
    });
  }
  else {
      this.LaserFicheSaveFunction();
  }
  return;
}

  if (this.AuthName == "ActiveDirectory") {
    if (this.searchlimit != null && this.searchlimit != "") {
        if (this.searchlimit != "-1") {
            let regex = /^[0-9()]*$/;           
            if (regex.test(this.searchlimit) == false) {               
            this.notification.openSnackBar(this.errorCodes["WAR074"].msg,"","warning-snackbar");
                this.searchlimit = "";
                return;
            }
        }
      }

    if ((this.ADAdminuser == null || this.ADAdminpassword == null) || (this.ADAdminuser == "" || this.ADAdminpassword == "") || (this.ADAdminuser == undefined || this.ADAdminpassword == undefined)) {
        this.notification.openSnackBar(this.errorCodes["WAR076"].msg,"","warning-snackbar");
        return;
    }
    if ((this.Adserver == null) || (this.Adserver == "") || (this.Adserver == undefined)) {
        this.notification.openSnackBar(this.errorCodes["WAR077"].msg,"","warning-snackbar");
        return;
    }
    if ((this.searchlimit == null) || (this.searchlimit == "") || (this.searchlimit == undefined)) {
        this.notification.openSnackBar(this.errorCodes["WAR078"].msg,"","warning-snackbar");
        return;
    }
}
if ((this.Adserverurl != this.Adserver) || (this.Adfilterval != this.Adfilter) || (this.IncludeGroupsval != this.IncludeGroups) || (this.AdGroupfilterval != this.AdGroupfilter))
 {
  const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
    data: {
      title: this.translate.instant('CTRUserConfigure.WarningText'),
      message: this.translate.instant('CTRUserConfigure.WarningText1')
    }
  });
  confirmDialog.afterClosed().subscribe(result => {
    if (result === true) {
      if (this.Adserverurl != this.Adserver) {
        this.ADflag = true;
    }
    this.SaveFunction();
    }
  });
 }
else {
   this.SaveFunction();
}
return;
  }
  Ldusernamechange()
  {
    if (this.LDAdminusernamevalue != this.LDAdminusername){
      this.btnsaveDisable = false;
  }
  this.btnDisable = false;
  this.btnreverrt = false;
  }
  Ldpasswordchange() {
    if (this.LDpasswordvalue != this.LDpassword) {
        this.btnsaveDisable = false;
    }
    this.btnDisable = false;
    this.btnreverrt = false;
}

Adusernamechange() {
  if (this.Adminusernamevalue != this.ADAdminuser) {
      this.btnsaveDisable = false;
  }
      this.btnDisable = false;
  this.btnreverrt = false;
}
Adpasswordchange() {
  if (this.passwordvalue != this.ADAdminpassword) {
      this.btnsaveDisable = false;
  }
      this.btnDisable = false;
  
  this.btnreverrt = false;
}
Changesearch() {
  if (this.searchlimitvalue != this.searchlimit) {
      this.btnsaveDisable = false;
  }
}
AddGroupsAndUsers() {
  if (this.IncludeGroupsAndUsersvalue != this.IncludeGroupsAndUsers) {
      this.btnsaveDisable = false;
  }               
}
Adserverchange() {
  if (this.Adserverurl != this.Adserver) {
      this.btnsaveDisable = false;
  }       
  this.btnreverrt = false;
  this.btnDisable = true;

}
Adfilterchange() {
  if (this.Adfilterval != this.Adfilter) {
      this.btnsaveDisable = false;
  }
  this.btnreverrt = false;
  this.btnDisable = true;
}
AdGroupfilterchange() {
  if (this.AdGroupfilterval != this.AdGroupfilter) {
      this.btnsaveDisable = false;
  }
  this.btnreverrt = false;
  this.btnDisable = true;
}
AddGroups(value:any) {
  if (this.IncludeGroupsval != value) {
      this.btnsaveDisable = false;
  }
  if (value == true) {
      this.IncludeGroups = true;
  }
  else {
      this.IncludeGroups = false;
  }
}
//Validation during change the input value.
checkLFCloud(value:any) {
  if (this.LFCloudChk != value) {
      this.btnsaveDisable = false;
  }
  if (value == true) {
      var x = "Account Id";
      this.sname = x;
      this.btnrepository = true;
      this.btnrepo = false;
  }
  else {
      var x = "Server";
      this.sname = x;
      this.btnrepository = false;
      this.btnrepo = true;
  }
  this.LFServerAddress = "";
}
  CheckDynamicUsers(){}
  SaveFunction()
  {
    this.isDirty=false;

    let url="api/User/SaveSetting";
    let postData={
      Authentication: this.AuthName,
                    Adserver: this.Adserver,
                    Adfilter: this.Adfilter,
                    IncludeGroups: this.IncludeGroups,
                    AdGroupfilter: this.AdGroupfilter,
                    ADAdminuser: this.ADAdminuser,
                    ADAdminpassword: this.ADAdminpassword,
                    Adflag: this.ADflag,
                    LDUsername: this.LDAdminusername,
                    LDPassword: this.LDpassword,
                    IncludeGroupsAndUsers: this.IncludeGroupsAndUsers,
                    Searchlimit: this.searchlimit,
                    ByPassPassword: this.Nfcauth,
    }
this.service.post(url,postData).subscribe(resp=>{
if(resp==true)
{
  this.IncludeGroupsval = this.IncludeGroups;
  this.IncludeGroupsAndUsersvalue = this.IncludeGroupsAndUsers;
  // this.userconfigureForm.$dirty = false;
  this.btnDisable = false;
  this.btnsaveDisable = true;
  this.btnreverrt = true;
  this.ADflag = false;
  localStorage.setItem('Searchlimit', this.searchlimit);
  if (this.AuthName == "ActiveDirectory") {
      localStorage.setItem('AuthType', 'ActiveDirectory');
  }
  else if (this.AuthName == "LocalDirectory") {
      localStorage.setItem('AuthType', 'LocalDirectory');
  }
  else if (this.AuthName == "Docuware") {
      localStorage.setItem('AuthType', 'Docuware');
  }
  else if (this.AuthName == "LaserFiche") {
      localStorage.setItem('AuthType', 'LaserFiche');
  }
  this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");

}
else {
  this.notification.openSnackBar(this.errorCodes["ERR021"].msg,"","red-snackbar");

}
},(error)=>
{
  this.notification.openSnackBar(error.message,"","red-snackbar");

});
  }
  DocuwareSaveFunction()
  {
let url="api/DocuwareSettings/SaveSetting";
let postData={
  Authentication: this.AuthName,
  URL: this.url,
  Username: this.userName,
  Password: this.docuwarepassword,
  Organization: this.organization,
  Docflag: this.Docflag,
  ByPassPassword: this.Nfcauth,
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>{
  if (resp== true) {
    // this.userconfigureForm.$dirty = false;
    this.btnsaveDisable = true;
    this.btnDisable = false;
    if (this.AuthName == "Docuware") {
        localStorage.setItem('AuthType', 'Docuware');
    }
    this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
    this.ngxLoader.stop();
}
else {
    this.notification.openSnackBar(this.errorCodes["ERR021"].msg,"","red-snackbar");
    this.ngxLoader.stop();
}
},(error)=>
{
  this.notification.openSnackBar(error.message,"","red-snackbar");
this.ngxLoader.stop();
});
  }

  MFilesSaveFunction()
  {
    let url="api/MFilesConnectorSettings/SaveSettings";
    let postData={
      Authentication: this.AuthName,
                    RootFolderId: this.RootFolderId,
                    DomainName: this.domain,
                    Username: this.username,
                    Password: this.Mfilepassword,
                    Vault: this.vault,
                    Class: this.class,
                    MFileFlag: this.MFilesflag,
                    ByPassPassword: this.Nfcauth,
   
    }
    this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp=>{
      if (resp== true) {
        //this.userconfigureForm.$dirty = false;
                        this.btnsaveDisable = true;
                        this.btnDisable = false;
                        if (this.AuthName == "MFiles") {
                            localStorage.setItem('AuthType', 'MFiles');
                        }
        this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
        this.ngxLoader.stop();
    }
    else {
        this.notification.openSnackBar(this.errorCodes["ERR021"].msg,"","red-snackbar");
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
    });
    
  }

  CustomTargetSaveFunction()
  {
    let url="api/CustomTarget/SaveSetting";
    let postData={
      CustomServerAddress: this.customserveraddress,
                    Adminusername: this.adminusername,
                    Adminpassword: this.adminpassword,
                    Parameters: this.parameters,
                    DynamicJobs: this.dynamicjobs,
                    ByPassPassword: this.Nfcauth,
   
    }
    this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp=>{
      if (resp== true) {
        //this.userconfigureForm.$dirty = false;
                        this.btnDisable = false;
                        if (this.AuthName == "CustomTarget") {
                            localStorage.setItem('AuthType', 'CustomTarget');
                        }
        this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
        this.ngxLoader.stop();
    }
    else {
        this.notification.openSnackBar(this.errorCodes["WAR003"].msg,"","warning-snackbar");
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
    });
    
  }
  LaserFicheSaveFunction()
  {
    let url="api/LaserFicheSettings/SaveSetting";
    let postData={
      Authentication: this.AuthName,
                    LFServerAddress: this.LFServerAddress,
                    LFCloudCheck: this.LFCloudCheck,                   
                    LFAdminusername: this.LFAdminusername,                 
                    LFPassword: this.LFPassword,                   
                    LFRepository: this.LFRepository,
                    LFflag: this.LFflag,
                    ByPassPassword: this.Nfcauth,
    }
    this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp=>{
      if (resp== true) {
        //this.userconfigureForm.$dirty = false;
                        this.btnDisable = false;
                        this.btnsaveDisable = true;
                        if (this.AuthName == "LaserFiche") {
                            localStorage.setItem('AuthType', 'LaserFiche');
                        }
        this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
        this.ngxLoader.stop();
    }
    else {
        this.notification.openSnackBar(this.errorCodes["WAR003"].msg,"","warning-snackbar");
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
    });
  }

  Revert()
  {
    this.btnsaveDisable = false;
    let url="api/User/LoadUserAccountSetting";
    let postData={
      Adserver: this.Adserver,
                    Adfilter: this.Adfilter,
                    IncludeGroups: this.IncludeGroups,
                    AdGroupfilter: this.AdGroupfilter
    }
    this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp=>{
      if (resp!=null) {
        this.Adserver = resp.Adserver;
        this.Adfilter = resp.Adfilter;
        this.IncludeGroups = resp.IncludeGroups;
        this.AdGroupfilter = resp.AdGroupfilter;
        this.ADAdminuser = resp.ADAdminuser;
        this.ADAdminpassword = resp.ADAdminpassword;
        this.ngxLoader.stop();
    }
    else {
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
    });
  }

  UserConfigure()
  {
    if (this.AuthName == "ActiveDirectory") {
      if ((this.ADAdminuser == null || this.ADAdminpassword == null || this.Adserver == null) || (this.ADAdminuser == "" || this.ADAdminpassword == "" || this.Adserver == "") || (this.ADAdminuser == undefined || this.ADAdminpassword == undefined || this.Adserver == undefined)) 
      {
          this.notification.openSnackBar("Admin credentials required.","","red-snackbar");
          return;
      }
  }
  
  this.dataStore.userConfigure={Authentication:this.AuthName,Adserver:this.Adserver,Adfilter:this.Adfilter,IncludeGroups:this.IncludeGroups,AdGroupfilter:this.AdGroupfilter}
  this.router.navigate(['/userconfiguresummary']);
  }

}

// this.notification.confirmation(
    //   this.translate.instant('CTRUserConfigure.WarningText1'), //message body
    //   () => { //okCallback
    //    this.notification.success("confirm oked");
    //   },
    //    this.translate.instant('CTRUserConfigure.WarningText'), //title
    //   () => { //cancelCallback
    //    this.notification.error("confirm canceled");
    //   });
         
    