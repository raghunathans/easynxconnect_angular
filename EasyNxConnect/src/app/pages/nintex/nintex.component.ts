import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { environment } from 'src/environments/environment';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

@Component({
  selector: 'app-nintex',
  templateUrl: './nintex.component.html',
  styleUrls: ['./nintex.component.scss']
})
export class NintexComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
  {
    return !this.isDirty
  }
  isDirty=false;

  servicelogin:any;
  folderpath:string="";
  personalaccesstoken:string="";
  folderpathvalue:string="";
  personalaccesstokenvalue:string="";
  errorCodes:any = {};
  testusername:any;
  target:any;
  url:any
ListOfUser:any;
userlist:any;
txtuser:any;
  displayStyle = "none";
  hidepath:boolean=false;
  paccesstoken:any;
  Appdetail:any;
  appsecret:any;
  appid:any;
  AppSettings:any;
  logintypeval:any;
  tokenurl:any;
  tokenexpireson:any;
  constructor(private http: HttpClient,public translate: TranslateService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) { }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');

    this.translate.use(returnUrl);
    this.url = environment.hostUrl;

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.LoadNintexConnectorSettings();

  }
  servicechange(val:any)
  {
    if (val.checked == true) {
      this.Appdetail = true;
      this.paccesstoken = false;
      this.logintypeval = "2";

  }
  else {
      this.logintypeval = "1";
      this.paccesstoken = true;
      this.Appdetail = false;
  }
  }
  GenerateToken()
  {
    if ((this.appid == "" || this.appsecret == "" || this.tokenurl=="") || (this.folderpath == null || this.appsecret == null || this.tokenurl==null)) {    
      this.notification.openSnackBar(this.errorCodes["WAR035"].msg,"","warning-snackbar");
      return ;
  }

  let url="api/NintexSettings/GetAccessToken";
  let postData={
    DomainUrl: this.tokenurl,
    NintexClientId: this.appid,
    NintexClientSecret: this.appsecret
  }
   this.ngxLoader.start();
  this.service.post(url,postData).subscribe(resp => {
    if (resp.Status == "SUCCESS") {
      this.tokenexpireson = resp.Data.ExpireAt;
  }
  else {
      
      this.notification.openSnackBar(this.errorCodes["WAR056"].msg,"","warning-snackbar");
  }
  });


  }
  LoadNintexConnectorSettings()
  {
    let url="api/NintexSettings/LoadSettings";
    let postData={
      Configurationfolderpath: "",
    }
     this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if(resp!=null)
      {
        this.folderpath = resp.Configurationfolderpath;
                    this.personalaccesstoken = resp.Personalaccesstoken;
                    this.folderpathvalue = resp.Configurationfolderpath;
                    this.personalaccesstokenvalue = resp.Personalaccesstoken;

                    this.tokenurl = resp.DomainUrl;
                    this.appid = resp.ClientId;
                    this.appsecret = resp.ClientSecret;
                    this.tokenexpireson = resp.ExpireAt;

                    this.logintypeval = resp.Logintype;

                    if (this.logintypeval == "2") {
                        this.servicelogin = true;
                        this.Appdetail = true;
                        this.paccesstoken = false;
                    }
                    else {
                        this.servicelogin = false;
                        this.Appdetail = false;
                        this.paccesstoken = true;
                    }

                }
      this.ngxLoader.stop();
    },(error)=>{
      this.ngxLoader.stop();
    
    });
    
  }
  SaveNintex()
  {
this.isDirty=false;
  //   if (this.folderpath != null && this.folderpath != "") {
  //     var reg = /^(([a-z]:\\)|(\\\\)){1}([a-z]+|(\d{1,3}.)*\\?)+$/ig;
  //     if (reg.test(this.folderpath) == false) {
  //         this.notification.openSnackBar(this.errorCodes["WAR007"].msg,"","warning-snackbar");
  //         return;
  //     }
  // }
//   if (this.folderpath == "" || this.folderpath == null) {
//     this.notification.openSnackBar(this.errorCodes["WAR004"].msg,"","warning-snackbar");
//     return;
// }
if (this.servicelogin == true) {
  if ((this.appid == "" || this.appsecret == "") || (this.folderpath == null || this.appsecret == null)) {    
       this.notification.openSnackBar(this.errorCodes["WAR035"].msg,"","warning-snackbar");
      return ;
  }
}
if (this.servicelogin == false) {
if (this.personalaccesstoken == "" || this.personalaccesstoken == null) {
    this.notification.openSnackBar(this.errorCodes["WAR052"].msg,"","warning-snackbar");
    return;
}
}
// if the use service login check box is not checked then the login type value is ""
                // this will cause a issue in the connector and it will fail to upload the document, hence this fix
                var loginType = this.logintypeval;
                if (!loginType || loginType.trim() == "")
                    loginType = "1"; //by default use personal access token.

    let url="api/NintexSettings/SaveSettings";
    let postData = {
      Configurationfolderpath: this.folderpath,
      Personalaccesstoken: this.personalaccesstoken,
      DomainUrl: this.tokenurl,
      ClientId: this.appid,
      ClientSecret: this.appsecret,
      LoginType: loginType,
      ExpireAt: this.tokenexpireson    
    }
    this.ngxLoader.start();
     this.service.post(url,postData).subscribe(resp => {
      if (resp==true) 
        {
          this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
          this.ngxLoader.stop();
this.LoadNintexConnectorSettings();
      }
      else {
        this.notification.openSnackBar(this.errorCodes["WAR003"].msg,"","warning-snackbar");
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }

  TestConnector()
  {
    this.displayStyle = "block";

    if ((this.folderpath != this.folderpathvalue) || (this.personalaccesstoken != this.personalaccesstokenvalue)) {
      this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");
 
      return;
  }
  var idval = -1;
switch (localStorage.getItem("AuthType")) {
    case "LocalDirectory":
        idval = 0;
        break;
    case "ActiveDirectory":
        idval = 1;
        break;
    case "Docuware":
        idval = 2;
        break;
    case "Egnyte":
        idval = 3;
        break;
    case "MFiles":
        idval = 4;
        break;
    case "CustomTarget":
        idval = 99;
        break;
}
let url=this.url+"api/User/GetLocalUsersByAuthId"
let postData={
  AuthId:idval
}
this.http.get(url,{params:postData}).subscribe(resp=>{
if(resp!=null)
{
  this.ListOfUser=resp
  if(this.ListOfUser.length>0)
{
  this.ListOfUser=resp;
  this.userlist = true;
  this.txtuser=false;
}
else{ 
  this.userlist = false;
  this.txtuser=true;
}
}
else {
  this.userlist = false;
  this.txtuser = true;
}
  });

  }
  closePopup() {
    this.displayStyle = "none";
  }
  getUser(val:any)
  {
    let userId = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;
    this.testusername=userId;
  }

  Test(username:any,target:any)
  {
    if (target == undefined || target == "") {
      
      this.notification.openSnackBar(this.errorCodes["WAR066"].msg,"","warning-snackbar");

      return;
  }
  let url="api/User/TestConnector";
  let postData={
    ProviderCode: "NINTEX",
    UserName:username,
    Arguments: "-t=" + '"' + target + '"'
  }
  this.ngxLoader.start();
  this.service.post(url,postData).subscribe(resp=>
    {
  if(resp.Status=='SUCCESS')
  {
    this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");
    this.ngxLoader.stop();
    this.displayStyle = "none";
  
  }
  else{
    this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
    this.ngxLoader.stop();
    this.displayStyle = "none";
  
  }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
}
