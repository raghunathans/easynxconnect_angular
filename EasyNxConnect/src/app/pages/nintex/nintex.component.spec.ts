import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NintexComponent } from './nintex.component';

describe('NintexComponent', () => {
  let component: NintexComponent;
  let fixture: ComponentFixture<NintexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NintexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NintexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
