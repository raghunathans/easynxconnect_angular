import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxConnectorComponent } from './box-connector.component';

describe('BoxConnectorComponent', () => {
  let component: BoxConnectorComponent;
  let fixture: ComponentFixture<BoxConnectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxConnectorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxConnectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
