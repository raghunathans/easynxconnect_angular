import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ENXLoginComponent } from './enx-login.component';

describe('ENXLoginComponent', () => {
  let component: ENXLoginComponent;
  let fixture: ComponentFixture<ENXLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ENXLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ENXLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
