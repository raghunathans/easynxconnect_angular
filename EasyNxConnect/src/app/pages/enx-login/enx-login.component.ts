import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/_api/api.service';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from "ngx-ui-loader";
import { NotificationService } from 'src/app/notification.service';
import { post, trim } from 'jquery';
import { MatDialog } from '@angular/material/dialog';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { HttpClient } from "@angular/common/http";

export interface Language{
Id:number;
LanguageCode:string;
LanguageName:string;
RoleName:string;
}
interface Provider {
  AuthUSer: string
}
let countryProviders:any;

@Component({
  selector: 'app-enx-login',
  templateUrl: './enx-login.component.html',
  styleUrls: ['./enx-login.component.scss']
})
export class ENXLoginComponent implements OnInit {
  loginForm: UntypedFormGroup;
  validateLoginForm = false;
  Languageshow:boolean=false;
  hide = true;
  lstLang:string="";
  Waitinginfo:boolean = false;

  usrname:any;
  langcode:any;
  AuthUser:string="";
  languages: Language[] = [];
  errorCodes:any = {};
  lng:any;
  constructor(public translate: TranslateService,private httpClient: HttpClient,private ErrorCodes:ErrorCodesService,private fb: UntypedFormBuilder,private notification:NotificationService, private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,public dialog: MatDialog) { 
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      language:['',Validators.required],

    });
    translate.addLangs(['en-US', 'de-DE']);
    translate.setDefaultLang('en-US');

  }

  ngOnInit(): void {
    this.getLanguages();
    this.errorCodes=this.ErrorCodes.errormsgs();

  }
  switchLang(event:any) {
    this.translate.use(event.value);
    this.lstLang = event.value;
                localStorage.setItem('PreferredLanguage', this.lstLang);
                this.AuthInfoLoad();
  }
 
  getLanguages()
  {
    let url = "api/User/GetLanguages?id=" + "";
    this.service.getAPI(url).then(resp => {
      this.Languageshow=true;
      this.languages = resp;
      // this.ngxLoader.stop();
    },(error)=>{
       this.notification.openSnackBar(error.message,"","red-snackbar");
      // this.ngxLoader.stop();
    });

  }
  get loginFormControls() {
    return this.loginForm.controls;
  }
  onSubmit() {
  }

  GetLang()
  {
if(this.Languageshow==true && this.loginForm.controls['username'].value!="")
{
if(this.loginForm.controls['username'].value==undefined)
{
return;
}
if (this.usrname == this.loginForm.controls['username'].value) {
  this.lstLang = this.langcode;
  return;
}
this.usrname = this.loginForm.controls['username'].value;
let url="api/User/GetLanguageCode";
let postData={
  Username: this.loginForm.controls['username'].value
}
this.service.post(url,postData).subscribe(resp=>
  {
    console.log(resp);
    if (resp.length > 0) {
      if (resp[0].RoleName == "RestrictedUser") {
        localStorage.setItem('PreferredLanguage', resp[0].LanguageCode);
    this.notification.openSnackBar(this.errorCodes["WAR068"].msg,"","warning-snackbar")
return;
      }

      var langCode = resp[0].LanguageCode;
      this.langcode = resp[0].LanguageCode;
      if (langCode && (langCode) != '' && langCode != "undefined") {
        this.lstLang = resp[0].LanguageCode;
        this.lng = resp[0].LanguageCode;
        localStorage.setItem('PreferredLanguage', resp[0].LanguageCode);
   
      }
    }
    this.AuthInfoLoad();
  },(error)=>{

  });
}
  }
  CommonItems:any={};
   AuthInfoLoad() {
    if (localStorage.getItem('PreferredLanguage') == "en-US") {
      this.httpClient.get("./assets/i18n/en-US.json").subscribe(data =>{
           this.CommonItems=data;
           this.AuthUser=this.CommonItems.CommonItems.AuthUser;

      })
  
    }
    else if (localStorage.getItem('PreferredLanguage') == "de-DE") {
        this.httpClient.get("./assets/i18n/de-DE.json").subscribe(data =>{
          this.CommonItems=data;
          this.AuthUser=this.CommonItems.CommonItems.AuthUser;
       });
    }
    else if (localStorage.getItem('PreferredLanguage') == "es-ES") {
      this.httpClient.get("./assets/i18n/es-ES.json").subscribe(data =>{
        this.CommonItems=data;
        this.AuthUser=this.CommonItems.CommonItems.AuthUser;
     });
    }
    else if (localStorage.getItem('PreferredLanguage') == "fr-FR") {
      this.httpClient.get("./assets/i18n/fr-FR.json").subscribe(data =>{
        this.CommonItems=data;
        this.AuthUser=this.CommonItems.CommonItems.AuthUser;
     });
    }
    else if (localStorage.getItem('PreferredLanguage') == "it-IT") {
      this.httpClient.get("./assets/i18n/it-IT.json").subscribe(data =>{
        this.CommonItems=data;
        this.AuthUser=this.CommonItems.CommonItems.AuthUser;
     });
    }
    else {
         this.AuthUser = "Authenticating the User. Please wait...";
    }
}


  Login()
{
  this.Waitinginfo = true;

  if (this.lstLang == "" || this.lstLang == null || this.lstLang == undefined) {
    this.Waitinginfo = false;
    this.notification.openSnackBar(this.errorCodes["ERR020"].msg,"","red-snackbar")
    return;
}

  let loginData = {
    UserName: this.loginForm.controls['username'].value,
    password: this.loginForm.controls['password'].value,
    PreferredLanguage:this.loginForm.controls['language'].value,
  };
  let data = {
    loginData: loginData,
  };
  let url = 'api/User/AuthenticateUser';
  this.ngxLoader.start();
  this.service.post(url,loginData).subscribe(resp=>{
    if(resp!=false)
    {
      if (resp.UserList!=null) 
      {
        var preLanquage = resp.UserList.PreferredLanguage;
        if (!preLanquage || preLanquage == 'null' || preLanquage == "") {
            preLanquage = "en-US";
        }
        if (resp.UserList.LastName == null) {
            resp.UserList.LastName = "";
        }  
        localStorage.setItem('Prevstate', 'General');
        localStorage.setItem('Notify','Show');
        localStorage.setItem('PreferredLanguage', preLanquage);
        localStorage.setItem('Role', resp.UserList.RoleName);
        localStorage.setItem('Id', resp.UserList.Id);
        localStorage.setItem('username', this.loginForm.controls['username'].value);
        localStorage.setItem('loginname', resp.UserList.FirstName + " " + resp.UserList.LastName);
        localStorage.setItem('AuthType', resp.AuthType);
        localStorage.setItem('AppVersion', resp.Appversion);
        localStorage.setItem('secondaryAuthType', resp.Secondary);
        localStorage.setItem('CustomProductType', resp.CustomProductType);
        localStorage.setItem('CustomProductModel', resp.CustomProductModel);
        localStorage.setItem('Idealtime', resp.Appidealtime);  
        this.translate.use(resp.UserList.PreferredLanguage);
        // var webLoginUrl = $location.absUrl();
        var webLoginUrl = window.location.href;

                         var queryparams = webLoginUrl.split('?')[1];
                        if (queryparams != undefined) {
                            var sessionUrl = decodeURIComponent(queryparams);
                            var index = sessionUrl.lastIndexOf("/") + 1;
                            var filenameWithExtension = sessionUrl.substr(index);
                            var filename = filenameWithExtension.split(".")[0];
                            // $state.go('app.' + filename + '');
                            // return false;
                        }
        if (resp.AuthType == "Docuware") {
          if (resp.UserList.RoleName == "Administrator") {
            this.router.navigate(['/generalsettings']);
          }
          else if (!resp.UserList.RoleName ||
              0 === resp.UserList.RoleName.length ||
              resp.UserList.RoleName == "SuperAdmin") {
                this.router.navigate(['/generalsettings']);
              }
          else if (!resp.UserList.RoleName ||
              0 === resp.UserList.RoleName.length ||
              resp.UserList.RoleName == "Users") {
              this.router.navigate(['/userconfiguresummary']);

          }
          else {
              this.notification.openSnackBar("User Role Required","","red-snackbar")

          }
      }
      else if (resp.Secondary == "Egnyte") {
          if (resp.UserList.RoleName == "Administrator") {
            this.router.navigate(['/generalsettings']);
          }
          else if (!resp.UserList.RoleName ||
              0 === resp.UserList.RoleName.length ||
              resp.UserList.RoleName == "SuperAdmin") {
                this.router.navigate(['/generalsettings']);
              }
          else if (!resp.UserList.RoleName ||
              0 === resp.UserList.RoleName.length ||
              resp.UserList.RoleName == "Users") {
                this.router.navigate(['/userconfiguresummary']);
              }
          else {
                this.notification.openSnackBar(this.errorCodes["ERR019"].msg,"","red-snackbar")
      }
      }
      else if (resp.AuthType == "MFiles") {
          if (resp.UserList.RoleName == "Administrator") {
            this.router.navigate(['/generalsettings']);
          }
          else if (!resp.UserList.RoleName ||
              0 === resp.UserList.RoleName.length ||
              resp.UserList.RoleName == "SuperAdmin") {
                this.router.navigate(['/generalsettings']);
              }
          else if (!resp.UserList.RoleName ||
              0 === resp.UserList.RoleName.length ||
              resp.UserList.RoleName == "Users") {
                this.router.navigate(['/userconfiguresummary']);
          }
          else {
              this.notification.openSnackBar(this.errorCodes["ERR019"].msg,"","red-snackbar")
          }
      }
      else if (resp.Secondary == "Nintex") {
          if (resp.UserList.RoleName == "Administrator") {
            this.router.navigate(['/generalsettings']);
          }
          else if (!resp.UserList.RoleName ||
              0 === resp.UserList.RoleName.length ||
              resp.UserList.RoleName == "SuperAdmin") {
                this.router.navigate(['/generalsettings']);
              }
          else if (!resp.UserList.RoleName ||
              0 === resp.UserList.RoleName.length ||
              resp.UserList.RoleName == "Users") {
                this.router.navigate(['/userconfiguresummary']);
              }
          else {
              this.notification.openSnackBar(this.errorCodes["ERR019"].msg,"","red-snackbar")
          }
      }
      else {
          if (resp.UserList.RoleName == "Administrator") {
            this.router.navigate(['/generalsettings']);
            this.ngxLoader.stop();
          }
          else if (!resp.UserList.RoleName ||
              0 === resp.UserList.RoleName.length ||
              resp.UserList.RoleName == "SuperAdmin") {
                this.router.navigate(['/generalsettings']);
                this.ngxLoader.stop();
              }
          else if (!resp.UserList.RoleName ||
              0 === resp.UserList.RoleName.length ||
              resp.UserList.RoleName == "Users") {
                this.router.navigate(['/userconfiguresummary']);
              this.ngxLoader.stop();
          }
          else {
              this.notification.openSnackBar(this.errorCodes["ERR019"].msg,"","red-snackbar")
          }
      }
    }
    else{
      this.Waitinginfo = false;
this.notification.openSnackBar(this.errorCodes["ERR002"].msg,"","red-snackbar")
    }
    }
    else if(resp.status=='FAILED')
    {
    //this.notification.openSnackBar("Invalid Credentials","","red-snackbar")
     this.ngxLoader.stop();
    }
    else{
      this.ngxLoader.stop();
    this.notification.openSnackBar("Invalid Credentials","","red-snackbar")

    }
    },(error)=>{
      this.notification.openSnackBar(error.message,"","red-snackbar")
    });
}



}
