import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnxGeneralSettingsComponent } from './enx-general-settings.component';

describe('EnxGeneralSettingsComponent', () => {
  let component: EnxGeneralSettingsComponent;
  let fixture: ComponentFixture<EnxGeneralSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnxGeneralSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnxGeneralSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
