import { Component, OnInit } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ApiService } from 'src/app/_api/api.service';
import {Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { TranslateService } from '@ngx-translate/core';
import { Form, FormArray, FormGroup } from '@angular/forms';
 import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from '../../component-can-deactivate';
// import { DeactivateComponent } from '../../can-deactivate-guard.service';

export interface Language{
  Id:number;
  LanguageCode:string;
  LanguageName:string;
  RoleName:string;
}
@Component({
  selector: 'app-enx-general-settings',
  templateUrl: './enx-general-settings.component.html',
  styleUrls: ['./enx-general-settings.component.scss']
})
export class EnxGeneralSettingsComponent implements OnInit,ComponentCanDeactivate {

   canDeactivate():boolean
   {
     return !this.isDirty
   }

  languages:Language[]=[];
  moveexception:any;
  leaveexception:any;
  movefolder:any;
  movefolderpath:any;
  EnableLogCheck:any;
  LogRetentionDays:any;
  DeleteReleasedocs:any;
  DaysToKeepDocs:any;
  serverportal:any;
  serveraddress:any;
  NXportval:any;
  serveradminname:any;
  serveradminpassword:any;
  Language:any;
  EmailServerAddress:any;
  PortNumbervalue:any;
  SSlRequire:any;
  Username:any;
  Password:any;
  SenderName:any;
  AdminCredentials:any;
  CurrentUser:any;
  UILength:any;
  UIPattern:any;
  FollowCredentials:any;
  SmtpEmailDomain:any;
  generalForm: any;
  Languageshow:boolean=false;
  EmailInfo:boolean=true;
  errorCodes:any = {};
	personForm = {} as FormArray;
isDirty=false;
  // canExit() : boolean {
 
  //   if (confirm("Do you wish to Please confirm")) {
  //       return true
  //     } else {
  //       return false
  //     }
  //   }

    
  constructor(public translate: TranslateService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) { }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');

    this.translate.use(returnUrl);

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.getLanguages();
this.loadGeneralSettings();
  }


  // canDeactivate(): Observable<boolean> | boolean {
	// 	if (!this.isUpdating && this.personForm.dirty) {
	// 		this.isUpdating = false;
	// 		return this.dialogService.confirm('Discard changes for Person?');
	// 	}
	// 	return true;
	// }

  
  getLanguages()
  {
    let url = "api/User/GetLanguages?id=" + "";
    this.service.getAPI(url).then(resp => {
      this.Languageshow = ! this.Languageshow;
      this.languages = resp;
      // this.ngxLoader.stop();
    },(error)=>{
      // this.notification.openSnackBar(error.message,"","red-snackbar");
      // this.ngxLoader.stop();
    });

  }
  //Load General settings

  loadGeneralSettings()
  {
    let url="api/GeneralExceptionSettings/LoadGeneralSetting";
let postData={
  SubScriptionId:""
}
 this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp => {
  if(resp!=null)
  {
    
    this.Language = resp.DefaultLanguage;
    this.moveexception = resp.chkMoveToExceptionSite;
    this.leaveexception = resp.chkLeaveAtSource;
    this.movefolder = resp.chkMoveToExceptionFolder;
    this.movefolderpath = resp.ExceptionFolder;
    this.EnableLogCheck = resp.chkLogging;
    this.LogRetentionDays = resp.numLogRetentionDays;
    this.DeleteReleasedocs = resp.chkDeleteReleasedDocs;
    this.DaysToKeepDocs = resp.numDaysToKeepDocs;
    this.serverportal = resp.NXMServerProtocol;
    this.serveraddress = resp.NXMServerAddress;
    this.NXportval = resp.PortNXMServer;
    this.serveradminname = resp.NXMServerAdmin;
    this.serveradminpassword = resp.NXMServerAdminPwd;
    this.EmailServerAddress = resp.EmailServer;
    this.PortNumbervalue = resp.PortNumber;
    this.SSlRequire = resp.chkSSL;
    this.Username = resp.SpecificUsername;
    this.Password = resp.SpecificPassword;
    this.SenderName = resp.SenderName;
    this.AdminCredentials = resp.UseAdminCredentials;
    this.CurrentUser = resp.UseCurrentUserCredentials;
    this.UILength = resp.UniqueIDLength;
    this.UIPattern = resp.UniqueIDPattern;
    this.FollowCredentials = resp.UseSpecificCredentials;
    this.SmtpEmailDomain = resp.SmtpEmailDomain;
    this.ngxLoader.stop();
  }
},(error)=>{
  this.ngxLoader.stop();

});
  }
  Authentication(value:string)
  {
    if (value == '1') {
      this.EmailInfo=true;
      this.AdminCredentials = true;
      this.CurrentUser = false;
      this.FollowCredentials = false;
    }

    if (value == '2') {
      this.EmailInfo=true;
      this.AdminCredentials = false;
      this.CurrentUser = true;
      this.FollowCredentials = false;
      // this.username = true;
      // this.password = true;
  }
  if (value == '3') {
    this.EmailInfo = !this.EmailInfo;
    this.AdminCredentials = false;
      this.CurrentUser = false;
      this.FollowCredentials = true;
      // this.username = false;
      // this.password = false;
  }
  }

  ExceptionMove(value:string) {
    if (value == '1') {
      this.moveexception = true;
      this.leaveexception = false;
      this.movefolder = false;
      //this.folder = true;
      this.movefolderpath = '';
  }
  if (value == '2') {
      this.moveexception = false;
      this.leaveexception = true;
      this.movefolder = false;
      //this.folder = true;
      this.movefolderpath = '';
  }
  if (value == '3') {
      this.moveexception = false;
      this.leaveexception = false;
      this.movefolder = true;
      //this.folder = false;
  }
  }
  SaveGeneralSettings()
  {
    this.isDirty=false;
    if (this.movefolderpath != null && this.movefolderpath != "") {
      var reg = /^(([a-z]:\\)|(\\\\)){1}([a-zA-Z0-9_]+|(\d{1,3}.)*\\?)+$/ig;
      if (reg.test(this.movefolderpath) == false) {
        this.notification.openSnackBar(this.errorCodes["WAR007"].msg,"","warning-snackbar");
          return false;
      }
  }
  if ((this.UILength == "3") || (this.UILength == "4") || (this.UILength == "5") || (this.UILength == "6") || (this.UILength == "7")) {

  }
  else {
    this.notification.openSnackBar(this.errorCodes["WAR079"].msg,"","warning-snackbar");
      return false;
  }
  
  if (this.movefolder == true) {
    if (this.movefolderpath == "" || this.movefolderpath == null) {
      this.notification.openSnackBar(this.errorCodes["WAR004"].msg,"","warning-snackbar");
        return false;
    }
}
if ((this.serverportal == "" || this.serverportal == null) || (this.serveraddress == "" || this.serveraddress == null) || (this.NXportval == "" || this.NXportval == null) || (this.serveradminname == "" || this.serveradminname == null) || (this.serveradminpassword == "" || this.serveradminpassword == null)) {
  this.notification.openSnackBar(this.errorCodes["WAR053"].msg,"","warning-snackbar");
    return false;
}
if (this.FollowCredentials == true) {
  if ((this.Username == null || this.Username == '') && (this.Password == null || this.Password == '')) {
    this.notification.openSnackBar(this.errorCodes["WAR040"].msg,"","warning-snackbar");
      return false;
  }
  if (this.Username == null || this.Username == '') {
    this.notification.openSnackBar(this.errorCodes["WAR062"].msg,"","warning-snackbar");
      return false;
  }
  if (this.Password == null || this.Password == '') {
    this.notification.openSnackBar(this.errorCodes["WAR063"].msg,"","warning-snackbar");
      return false;
  }
  return this.Mainfunction();
}
else {
  return this.Mainfunction();
}
  }

        Mainfunction()
        {
          let url="api/GeneralExceptionSettings/SaveSetting";
    let postData={
      chkMoveToExceptionSite:this.moveexception,
      chkLeaveAtSource:this.leaveexception,
      chkMoveToExceptionFolder:this.movefolder,
      ExceptionFolder:this.movefolderpath,
      chkLogging:this.EnableLogCheck,
      numLogRetentionDays:this.LogRetentionDays,
      chkDeleteReleasedDocs:this.DeleteReleasedocs,
      numDaysToKeepDocs:this.DaysToKeepDocs,
      NXMServerProtocol:this.serverportal,
      NXMServerAddress:this.serveraddress,
      PortNXMServer:this.NXportval,
      NXMServerAdmin:this.serveradminname,
      NXMServerAdminPwd:this.serveradminpassword,
      DefaultLanguage:this.Language,
      EmailServer:this.EmailServerAddress,
      PortNumber:this.PortNumbervalue,
      chkSSL:this.SSlRequire,
      SpecificUsername:this.Username,
      SpecificPassword:this.Password,
      SenderName:this.SenderName,
      UseAdminCredentials:this.AdminCredentials,
      UseCurrentUserCredentials:this.CurrentUser,
      UseSpecificCredentials:this.FollowCredentials,
      UniqueIDPattern:this.UIPattern,
      UniqueIDLength:this.UILength,
      SmtpEmailDomain:this.SmtpEmailDomain
    }
      this.ngxLoader.start();
      this.service.post(url,postData).subscribe(resp => {
      if(resp==true)
      {
   this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar")
     this.ngxLoader.stop();
      }
      else if(resp==false)
      {
        this.notification.openSnackBar(this.errorCodes["WAR003"].msg,"","warning-snackbar")
       this.ngxLoader.stop();
      }
    });

        }
}
