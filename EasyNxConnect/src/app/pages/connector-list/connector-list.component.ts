import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/_api/api.service';

@Component({
  selector: 'app-connector-list',
  templateUrl: './connector-list.component.html',
  styleUrls: ['./connector-list.component.scss']
})
export class ConnectorListComponent implements OnInit {
  ListOfConnector:any = [];

  Connectorlist:any = [];
  Selectedconnector:any;
  constructor(private service: ApiService,private router:Router) { }

  ngOnInit(): void {

    this.getConnectorList();
    
  }

  getConnectorList()
  {
    let url="api/User/GetAllProviders";
    this.service.getAPI(url).then(resp=>
      {
        if(resp!=null)
        {
           this.ListOfConnector=resp;

          if (localStorage.getItem('AuthType') == "MFiles") 
          {
            for (var i = 0; i < this.ListOfConnector.length;i++) 
            {
              if (this.ListOfConnector[i].ProviderCode == "MFILES") 
              {
              this.Connectorlist.push(this.ListOfConnector[i]);
              break;
              }
            }
          }
          else if (localStorage.getItem('AuthType') == "Docuware") 
          {
            for (var i = 0; i < this.ListOfConnector.length; i++) 
            {
                if (this.ListOfConnector[i].ProviderCode == "DOCUWARE") 
                {
                    this.Connectorlist.push(this.ListOfConnector[i]);
                    break;
                }
            }
          }
          else if (localStorage.getItem('AuthType') == "LaserFiche") 
          {
            for (var i = 0; i < this.ListOfConnector.length; i++) 
            {
                if (this.ListOfConnector[i].ProviderCode == "LF") 
                {
                    this.Connectorlist.push(this.ListOfConnector[i]);
                    break;
                }
            }

          }
          else if (localStorage.getItem('AuthType') == 'CustomTarget') 
          {
            for (var i = 0; i < this.ListOfConnector.length; i++) 
            {
                if (this.ListOfConnector[i].ProviderCode == "CUSTOMTARGET") 
                {
                    this.Connectorlist.push(this.ListOfConnector[i]);
                    break;
                }
            }
          }
        else 
        {
            for (var i = 0; i < this.ListOfConnector.length; i++) 
            {
                if (this.ListOfConnector[i].ProviderCode != "DOCUWARE" && this.ListOfConnector[i].ProviderCode != "MFILES" && this.ListOfConnector[i].ProviderCode != "AD" && this.ListOfConnector[i].ProviderCode != "LOCAL"
                    && this.ListOfConnector[i].ProviderCode != "LF" && this.ListOfConnector[i].ProviderCode != "CUSTOMTARGET")
                {
                    this.Connectorlist.push(this.ListOfConnector[i]);
                }
            }
        }
        }
      });

  }
  
  ConnectorsChange(event:any)
  {
    localStorage.setItem('SelConnector', event.value);
    switch (this.Selectedconnector) {
        case 'PRINTERCONNECTOR':
            this.router.navigate(['/printerConnector']);           
            break;
        case 'EMAILCONNECTOR':
            this.router.navigate(['/Email']);           
            break;
        case 'FOLDERCONNECTOR':
            this.router.navigate(['/HomeFolder']);           
            break;
        case 'FTPCONNECTOR':
            this.router.navigate(['/FTP']);            
            break;
        case 'SPO':
            this.router.navigate(['/SharePointOnline']);           
            break;
        case 'AWSS3':
            this.router.navigate(['/amazons3']);
            
            break;
        case 'BOX':
            this.router.navigate(['/box']);           
            break;
        case 'DROPBOX':
            this.router.navigate(['/DropBox']);            
            break;
        case 'ODB':
            this.router.navigate(['/OneDrive']);                        
            break;
        case 'LF':
            // $state.go('app.LaserFicheConnector');
            
            break;
        case 'NINTEX':
            this.router.navigate(['/Nintex']);           
            break;
        case 'AZUREBLOB':
            this.router.navigate(['/AzureBlob']);
            break;
        case 'DOCUWARE':
            // $state.go('app.Docuware');
            this.router.navigate(['/Docuware']);
            break;
        case 'EGNYTE':
            this.router.navigate(['/Egnyte']);
            break;
        case 'MFILES':
            // $state.go('app.MFileConnector');
            
            break;
        case 'FAX':
            this.router.navigate(['/Fax']);                        
            break;
        case 'UIPATH':
            this.router.navigate(['/UIPath']);            
            break;
        case 'CUSTOMTARGET':
            // $state.go('app.CustomTarget');
            
            break;

        default:
    }
    }
}
