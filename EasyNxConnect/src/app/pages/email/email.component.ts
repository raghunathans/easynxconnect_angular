import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { environment } from 'src/environments/environment';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
   {
     return !this.isDirty
   }
   isDirty=false;

  DetermineAuto:any;
  Enterfolderchk:any;
  Enterpath:any;
  EnterHerepath:string="";
  Email:any;
  EmailDomain:string="";
  errorCodes:any = {};
  SenderEmailAddres:boolean = true;
  EmailSubject:string="";
  EmailBody:string="";
  testusername:any;
  target:any;
  url:any
ListOfUser:any;
userlist:any;
txtuser:any;
  displayStyle = "none";

  constructor(private http: HttpClient,public translate: TranslateService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) { }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');

    this.translate.use(returnUrl);
    this.url = environment.hostUrl;

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.LoadEmailConnectorSettings();

  }
  LoadEmailConnectorSettings()
  {
    let url="api/EmailConnectorSettings/LoadEmailControllerSetting";
    let postData={
      EmailDomain: "",
    }
     this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if(resp!=null)
      {
        this.EmailDomain = resp.EmailDomain;
                this.Enterfolderchk = resp.UseSpecificEmailTarget;
                this.EnterHerepath = resp.EmailTarget;
                this.DetermineAuto = resp.AutomaticEmail;
                this.ngxLoader.stop();
                if (this.Enterfolderchk == false) {
                    this.Enterpath = true;
                }
      }
      this.ngxLoader.stop();
    },(error)=>{
      this.ngxLoader.stop();
    
    });
    
  }
  SendEmail(val:any)
  {
    if (val == '1') {
      this.DetermineAuto = true;
      this.Enterpath = true;
      this.Enterfolderchk = false;
  }
  if (val == '2') {
      this.DetermineAuto = false;
      this.Enterpath = false;
      this.Enterfolderchk = true;
  }
  }
  EBody()
  {
    let url="api/EmailConnectorSettings/LoadEmailControllerSetting";
       let postData={
         EmailSubject: this.EmailSubject,
         EmailBody: this.EmailBody  }
        this.ngxLoader.start();
        this.service.post(url,postData).subscribe(resp => {
         if ((resp.EmailSubject != "") || (resp.EmailBody != "")) 
           {
             this.EmailSubject = resp.EmailSubject;
             this.EmailBody = resp.EmailBody;
             this.ngxLoader.stop();
         }
         else {
           this.EmailSubject = this.errorCodes["RES007"].res;
           this.EmailBody = this.errorCodes["RES008"].res;
           this.ngxLoader.stop();
       }
       },(error)=>
       {
         this.notification.openSnackBar(error.message,"","red-snackbar");
         this.ngxLoader.stop();
       });
  }
  SaveEmailOption()
  {
    let url="api/EmailConnectorSettings/SaveEmailSetting";
    let postData = {
      EmailSubject: this.EmailSubject,
      EmailBody: this.EmailBody
    }
    this.ngxLoader.start();
     this.service.post(url,postData).subscribe(resp => {
      if (resp==true) 
        {
          this.EmailSubject = resp.EmailSubject;
          this.EmailBody = resp.EmailBody;
          this.ngxLoader.stop();
      }
      else {
        this.notification.openSnackBar(this.errorCodes["WAR003"].msg,"","warning-snackbar");
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  
  }
  SaveEmailConnector()
  {
    this.isDirty=false;
    if (this.Enterfolderchk == true) {
      if (this.EnterHerepath == null || this.EnterHerepath == "") {
          this.notification.openSnackBar(this.errorCodes["WAR008"].msg,"","warning-snackbar");
          return;
      }
    }
    let url="api/EmailConnectorSettings/SaveSetting";
    let postData = {
  EmailDomain: this.EmailDomain,
  UseSpecificEmailTarget: this.Enterfolderchk,
  EmailTarget: this.EnterHerepath,
  AutomaticEmail: this.DetermineAuto,
}

this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp => {
if(resp==true)
{
  this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
  this.LoadEmailConnectorSettings();
  this.ngxLoader.stop();
}
else
{
  this.notification.openSnackBar(this.errorCodes["WAR016"].msg,"","warning-snackbar");
    this.ngxLoader.stop();
}
},(error)=>
{
  this.notification.openSnackBar(error.message,"","red-snackbar");
  this.ngxLoader.stop();
});
  }
  CreateEmailJobs()
  {
    localStorage.removeItem('Map');
    localStorage.removeItem('BoxMap');
    localStorage.removeItem('ODBMap');
    localStorage.removeItem('EgnyteMap');
    localStorage.removeItem('FTPMap');
    localStorage.removeItem('FolderMap');
    localStorage.removeItem("AzureBlobMap");
    localStorage.removeItem("SPOMap");
    localStorage.removeItem("MapDoc");
    localStorage.removeItem("mapMFiles");
    localStorage.removeItem("mapfax");
    localStorage.removeItem("UIPathMap");
    localStorage.removeItem("DropBoxMap");

    localStorage.setItem('mapEmail', 'EmailMap');
    this.router.navigate(['/JobManagement']);
  }

  TestConnector()
  {
    this.displayStyle = "block";

    var idval = -1;
                switch (localStorage.getItem("AuthType")) {
                    case "LocalDirectory":
                        idval = 0;
                        break;
                    case "ActiveDirectory":
                        idval = 1;
                        break;
                    case "Docuware":
                        idval = 2;
                        break;
                    case "Egnyte":
                        idval = 3;
                        break;
                    case "MFiles":
                        idval = 4;
                        break;
                    case "CustomTarget":
                        idval = 99;
                        break;
                }
                
                let url=this.url+"api/User/GetLocalUsersByAuthId"
let postData={
  AuthId:idval
}
                this.http.get(url,{params:postData}).subscribe(resp=>{
if(resp!=null)
{
  this.ListOfUser=resp
  if(this.ListOfUser.length>0)
{
  this.ListOfUser=resp;
  this.userlist = true;
  this.txtuser=false;
}
else{ 
  this.userlist = false;
  this.txtuser=true;
}
}
else {
  this.userlist = false;
  this.txtuser = true;
}
                });
  }

  getUser(val:any)
  {
    let userId = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;
    this.testusername=userId;
  }

  Test(testusername:any,target:any)
  {
    if (this.target == undefined) {
      this.target = "";
  }
  let url="api/User/TestConnector";
let postData={
  ProviderCode: "EMAILCONNECTOR",
                    UserName: this.testusername,
                    Arguments: "-t=" + '"' + target + '"'
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
  {
if(resp.Status=='SUCCESS')
{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
else{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });

  }
  closePopup() {
    this.displayStyle = "none";
  }
}
