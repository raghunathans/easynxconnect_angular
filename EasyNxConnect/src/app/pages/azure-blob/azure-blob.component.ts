import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { environment } from 'src/environments/environment';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

@Component({
  selector: 'app-azure-blob',
  templateUrl: './azure-blob.component.html',
  styleUrls: ['./azure-blob.component.scss']
})
export class AzureBlobComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
   {
     return !this.isDirty
   }
   isDirty=false;

  storageaccountname:string="";
  accesstype:any="ConnectionString";
  accessinfo:string="";
  containername:string="";
  defaultfolder:string="";
  storageaccountnamevalue:string="";
  connectionstringvalue:string="";
  containernamevalue:string="";
  btnmapjob:any;
  btntest:any;
  errorCodes:any = {};
  url:any
  ListOfUser:any;
  userlist:any;
  txtuser:any;
  username:any;
  target:any;
  container:string="";
    displayStyle = "none";
  
  constructor(private http: HttpClient,public translate: TranslateService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) { }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');
    this.url = environment.hostUrl;

    this.translate.use(returnUrl);

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.LoadAzureBlobConnectorSettings();
  }

  
  LoadAzureBlobConnectorSettings()
  {
    let url="api/AzureBlob/LoadAzureControllerSetting";
    let postData={
      Storageaccountname:""
    }
     this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if(resp!=null)
      {
                this.storageaccountname = resp.Storageaccountname;
                this.accessinfo = resp.AccessInfo;
                this.accesstype = resp.AccessType;
                this.containername = resp.Containername;
                this.defaultfolder = resp.Defaultfolder;
                this.storageaccountnamevalue = resp.Storageaccountname;
                this.connectionstringvalue = resp.AccessInfo;
                this.containernamevalue = resp.Containername;

                if (!this.accesstype || this.accesstype == '')
                    this.accesstype = 'ConnectionString'; //Default Access Type - if not set
                
      }
      this.ngxLoader.stop();
    },(error)=>{
      this.ngxLoader.stop();
    
    });
    }
    SaveAzureBlob()
    {
      this.isDirty=false;
      if ((this.storageaccountname == "" || this.storageaccountname == undefined) || (this.accessinfo == "" || (this.accessinfo == undefined)) || (this.containername == "" || this.containername == undefined)) {     
        this.notification.openSnackBar(this.errorCodes["WAR035"].msg,"","warning-snackbar");
        return;
    }
    let url="api/AzureBlob/SaveSetting";
    let postData={
      Storageaccountname: this.storageaccountname,
      AccessInfo: this.accessinfo,
      Containername: this.containername,
      Defaultfolder: this.defaultfolder,
      AccessType: this.accesstype    }
     this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if(resp==true)
      {
        this.btnmapjob = false;
        this.btntest = false;
        this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
        this.ngxLoader.stop();

this.LoadAzureBlobConnectorSettings();
      }
else{
  this.notification.openSnackBar(this.errorCodes["WAR003"].msg,"","warning-snackbar");
  this.ngxLoader.stop();
}
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
    }
  ChangeStorage()
  {

  }
  ChangeConnection()
  {

  }
  ChangeContainer()
  {

  }
  clearfield(val:any)
  {

  }
  AzureBlobMapJobs()
  {
    if ((this.storageaccountnamevalue != this.storageaccountname) || (this.connectionstringvalue != this.accessinfo) || (this.containernamevalue != this.containername)) {
      
      this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");

      return;
  }
  localStorage.removeItem('Map');

                localStorage.removeItem('MapDoc');
                localStorage.removeItem('mapEmail');
                localStorage.removeItem('BoxMap');
                localStorage.removeItem('SPOMap');
                localStorage.removeItem('ODBMap');
                localStorage.removeItem('FolderMap');
                localStorage.removeItem('FTPMap');
                localStorage.removeItem("mapMFiles");
                localStorage.removeItem('EgnyteMap');
                localStorage.removeItem("UIPathMap");
                localStorage.removeItem("DropBoxMap");

                localStorage.setItem("AzureBlobMap", "AzureBlobJobs");
                this.router.navigate(['/JobManagement']);

  }
  //TestConnector

  TestConnector()
  {
    this.displayStyle = "block";
    if ((this.storageaccountnamevalue != this.storageaccountname) || (this.connectionstringvalue != this.accessinfo) || (this.containernamevalue != this.containername)) {      
      this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");
      return;
  }
  var idval = -1;
  switch (localStorage.getItem("AuthType")) {
      case "LocalDirectory":
          idval = 0;
          break;
      case "ActiveDirectory":
          idval = 1;
          break;
      case "Docuware":
          idval = 2;
          break;
      case "Egnyte":
          idval = 3;
          break;
      case "MFiles":
          idval = 4;
          break;
      case "CustomTarget":
          idval = 99;
          break;
  }
  
  let url=this.url+"api/User/GetLocalUsersByAuthId"
  let postData={
    AuthId:idval
  }
  this.http.get(url,{params:postData}).subscribe(resp=>{
  if(resp!=null)
  {
    this.ListOfUser=resp
    if(this.ListOfUser.length>0)
  {
    this.ListOfUser=resp;
    this.userlist = true;
    this.txtuser=false;
  }
  else{ 
    this.userlist = false;
    this.txtuser=true;
  }
  }
  else {
    this.userlist = false;
    this.txtuser = true;
  }                 
});
  }
  getUser(val:any)
{
  let userId = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;
  this.username=userId;
console.log(this.username);
}
closePopup() {
  this.displayStyle = "none";
}

Test(username:any,target:any,container:any)
{
  if (username == undefined || username==null) {
    username = "";
}
if (target == undefined || target == null) {
    target = "";
}
if (container == undefined || container == null) {
    container = "";
}
let url="api/User/TestConnector";
let postData={
  ProviderCode: "AZUREBLOB",
                    UserName: username,
                    Arguments: "-t=" + '"' + target + '"' +" "+ "-ct=" + '"' + container + '"'                
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
  {
if(resp.Status=='SUCCESS')
{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
else{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });
}

}
