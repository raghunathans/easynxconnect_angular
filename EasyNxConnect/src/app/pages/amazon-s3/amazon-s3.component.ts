import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { StoredataService } from 'src/app/storedata.service';
import { ApiService } from 'src/app/_api/api.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

@Component({
  selector: 'app-amazon-s3',
  templateUrl: './amazon-s3.component.html',
  styleUrls: ['./amazon-s3.component.scss']
})
export class AmazonS3Component implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
   {
     return !this.isDirty
   }
   isDirty=false;

  GetRegions:any[]=[];
  GetUsers:any[]=[];
  GetBuckets:any[]=[];
  Bucket:any;
  Username:any;
  username:any;
  target:any;
  Region:any;
  Homefolder:any;
  Specificfolder:any;
  RootFolderId:string="";
  folder:boolean=false;
  Awsaccesskey:any;
  Awssecrekey:any;

  Bucket1:any;
  Awsaccesskeyvalue:any;
  Awssecrekeyvalue:any;
  Regionvalue:any;
  Usernamevalue:any;
  Bucketvalue:any;
  buc:any;
  errorCodes:any = {};
url:any
ListOfUser:any;
userlist:any;
txtuser:any;
  displayStyle = "none";

  constructor(private http: HttpClient,private service:ApiService,private dataStore:StoredataService,private router:Router,private ngxLoader:NgxUiLoaderService,private notification:NotificationService,private ErrorCodes:ErrorCodesService) { }

  ngOnInit(): void {
    this.errorCodes=this.ErrorCodes.errormsgs();
    this.getRegions();
    this.getUsers();
    this.getBuckets();
    this.loadAmazonSettings();
    this.url = environment.hostUrl;

  }
getRegions()
{
  let url="api/Amazon/GetRegions"
  this.service.getAPI(url).then(resp=>
    {
if(resp!=null)
{
this.GetRegions=resp;
}
    });
}
getUsers()
{
  let url="api/Amazon/GetUsers"
  this.service.getAPI(url).then(resp=>
    {
if(resp!=null)
{
this.GetUsers=resp;
}
    });
}
getBuckets()
{

  let url="api/Amazon/GetBuckets"
let postData={
  Username: this.Username,
  SelectedRegion:this.Region,
  AWSS3AccessID:this.Awsaccesskey,
  AWSS3SecretKey:this.Awssecrekey
}
  this.service.post(url,postData).subscribe(resp=>
    {
if(resp!=null)
{
this.GetBuckets=resp;
}
    });
}

loadAmazonSettings()
{
  let url="api/Amazon/LoadAmazonControllerSetting";
  let postData={
    UserFolder: "",
  }
   this.ngxLoader.start();
   this.service.post(url,postData).subscribe(resp => {
    if(resp!=null)
    {
      this.Homefolder = resp.UserFolder;
      this.Specificfolder = resp.SpecificFolder;
      this.RootFolderId = resp.RootFolder;
      this.Region = resp.Regions;
      this.Username = resp.Users;
      this.Bucket1 = resp.Buckets;
      this.Bucket = resp.Buckets;
      this.Awsaccesskey = resp.AWSS3AccessID;
      this.Awssecrekey = resp.AWSS3SecretKey;
      this.Awsaccesskeyvalue = resp.AWSS3AccessID;
      this.Awssecrekeyvalue = resp.AWSS3SecretKey;
      this.Regionvalue = resp.Regions;
      this.Usernamevalue = resp.Users;
      this.Bucketvalue = resp.Buckets;
      localStorage.setItem('bucket', resp.Buckets);
      this.buc = this.Bucket;
      this.ngxLoader.stop();
    }});
}

AmazonRoot(value:string)
{
  if (value == '1') {
    this.Homefolder = true;
    this.Specificfolder = false;
    this.folder = true;
}
if (value == '2') {
    this.Homefolder = false;
    this.Specificfolder = true;
    this.folder = false;
}
}

RegionChange(region:any) 
{
  this.Region = region.value;
}
BucketChange(bucket:any) {
  this.Bucket = bucket.value
}

UserChange(user:any)
{
  let url="api/Amazon/GetBuckets"
let postData={
  Username: user.value,
  SelectedRegion:this.Region,
  AWSS3AccessID:this.Awsaccesskey,
  AWSS3SecretKey:this.Awssecrekey
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>{

  this.ngxLoader.stop();
});
}

SaveAmazonSettings()
{
  if (this.Specificfolder == true) {
    if (this.RootFolderId == null || this.RootFolderId == "") {
        this.notification.openSnackBar(this.errorCodes["WAR057"].msg,"","warning-snackbar");
        return;
    }
  }
    let url="api/Amazon/SaveSetting"
    let postData={
      UserFolder: this.Homefolder,
      SpecificFolder:this.Region,
      RootFolder:this.RootFolderId,
      Regions:this.Region,
      Users:this.Username,
      Buckets:this.Bucket,
      AWSS3AccessID:this.Awsaccesskey,
      AWSS3SecretKey:this.Awssecrekey
    }
    this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp=>{
    if(resp==true)
    {
      this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
      window.location.reload();
    }
    else{
      this.notification.openSnackBar(this.errorCodes["WAR017"].msg,"","warning-snackbar");
    }
      this.ngxLoader.stop();
    });
}

MapBuckets()
{
  if ((this.Awsaccesskey != this.Awsaccesskeyvalue) || (this.Awssecrekey != this.Awssecrekeyvalue) || (this.Region != this.Regionvalue)
                    || (this.Username != this.Usernamevalue) || (this.Bucket != this.Bucketvalue)) {
                    
                    this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");

                    return;
                }
                this.dataStore.AmazonS3={Username:this.Username,Region:this.Region}

                // this.amazon = 'Buckets'
                // ScopeValueService.store('CTRAmazons3Controller', this);
                localStorage.removeItem('MapDoc');
                localStorage.removeItem('mapEmail');
                localStorage.removeItem('ODBMap');
                localStorage.removeItem('SPOMap');
                localStorage.removeItem("AzureBlobMap");
                localStorage.removeItem('FTPMap');
                localStorage.removeItem('FolderMap');
                localStorage.removeItem('EgnyteMap');
                localStorage.removeItem("mapMFiles");
                localStorage.removeItem("BoxMap");
                localStorage.removeItem("UIPathMap");
                localStorage.removeItem("DropBoxMap");

                localStorage.setItem('Map', 'Buckets');
                this.router.navigate(['/JobManagement']);

               
}

TestConnector() {
  this.displayStyle = "block";
  if ((this.Awsaccesskey != this.Awsaccesskeyvalue) || (this.Awssecrekey != this.Awssecrekeyvalue) || (this.Region != this.Regionvalue)
                    || (this.Username != this.Usernamevalue) || (this.Bucket != this.Bucketvalue)) {
                    this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");
                    return;
                }
                var idval = -1;
                switch (localStorage.getItem("AuthType")) {
                    case "LocalDirectory":
                        idval = 0;
                        break;
                    case "ActiveDirectory":
                        idval = 1;
                        break;
                    case "Docuware":
                        idval = 2;
                        break;
                    case "Egnyte":
                        idval = 3;
                        break;
                    case "MFiles":
                        idval = 4;
                        break;
                    case "CustomTarget":
                        idval = 99;
                        break;
                }
                
                let url=this.url+"api/User/GetLocalUsersByAuthId"
let postData={
  AuthId:idval
}
                this.http.get(url,{params:postData}).subscribe(resp=>{
if(resp!=null)
{
  this.ListOfUser=resp
  if(this.ListOfUser.length>0)
{
  this.ListOfUser=resp;
  this.userlist = true;
  this.txtuser=false;
}
else{ 
  this.userlist = false;
  this.txtuser=true;
}
}
else {
  this.userlist = false;
  this.txtuser = true;
}
                });
              }
closePopup() {
  this.displayStyle = "none";
}
getUser(val:any)
{
  let userId = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;
  this.username=userId;
console.log(this.username);
}
Test(username:any,target:any)
{
if (target == undefined || target == "") {
  this.notification.openSnackBar(this.errorCodes["WAR066"].msg,"","warning-snackbar");
  return;
}
let url="api/User/TestConnector";
let postData={
  ProviderCode: "AWSS3",
                    UserName: username,
                    Arguments: "-t=" + '"' + target + '"'
   
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
  {
if(resp.Status=='SUCCESS')
{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
else{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });
}

}
