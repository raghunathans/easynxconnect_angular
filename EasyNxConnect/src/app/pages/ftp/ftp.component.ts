import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { environment } from 'src/environments/environment';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

@Component({
  selector: 'app-ftp',
  templateUrl: './ftp.component.html',
  styleUrls: ['./ftp.component.scss']
})
export class FTPComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
   {
     return !this.isDirty
   }
   isDirty=false;

  usekeyfile:any;
  ftpserver:string="";
  folderpath:any;
  usekey:any;
  ftppassphrase:any;
  ftpusername:any;
  mandatory:any;
  uploadimage:any;
  ftpprivatekey:any;
  Onlysftp:any;
  ftpprotocolFTPSE:any;
  ftpprotocolFTPSI:any;
  ftpprotocolSFTP:any;
  ftpprotocolFTP:any;
  EnterHerepath:any;
  Enterpath:any;
  Enterfolderchk:any;
  DetermineAuto:any;
  password:any;
  Password:any;
  username:any;
  Username:any;
  FollowCredentials:any;
  CurrentUser:any;
  AdminCredentials:any;
  PortNo:any;
  ftppublickey:any;
  ftpservervalue:any;
  PortNovalue:any;
  ftpprotocolFTPvalue:any;
  ftpprotocolSFTPvalue:any;
  ftpprotocolFTPSIvalue:any;
  ftpprotocolFTPSEvalue:any;
  AdminCredentialsvalue:any;
  CurrentUservalue:any;
  FollowCredentialsvalue:any;
  Usernamevalue:any;
  Passwordvalue:any;
  usekeyfilevalue:any;
  ftpprivatekeyvalue:any;
  ftppassphrasevalue:any;
  ftpusernamevalue:any;
  hide = true;

  testusername:any;
  testpassword:any;
  target:any;
  url:any
ListOfUser:any;
userlist:any;
txtuser:any;
  displayStyle = "none";
        
  errorCodes:any = {};

  constructor(private http: HttpClient,public translate: TranslateService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) { }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');

    this.translate.use(returnUrl);
    this.url = environment.hostUrl;

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.LoadFTPConnectorSettings();

  }
  LoadFTPConnectorSettings()
  {
    let url="api/FTPConnectorSettings/LoadFTPControllerSetting";
    let postData={
      FTPServer: "",
    }
     this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if(resp!=null)
      {
        this.ftpserver = resp.FTPServer;
                this.PortNo = resp.PortNumber;
                this.ftpprotocolFTP = resp.ftpprotocolFTP;
                this.ftpprotocolSFTP = resp.ftpprotocolSFTP;
                this.ftpprotocolFTPSI = resp.ftpprotocolFTPSI;
                this.ftpprotocolFTPSE = resp.ftpprotocolFTPSE;
                this.Username = resp.SpecificUsername;
                this.Password = resp.SpecificPassword;
                this.Enterfolderchk = resp.UseSpecificEmailTarget;
                this.EnterHerepath = resp.EmailTarget;
                this.DetermineAuto = resp.AutomaticEmail;
                this.AdminCredentials = resp.UseAdminCredentials;
                this.CurrentUser = resp.UseCurrentUserCredentials;
                this.FollowCredentials = resp.UseSpecificCredentials;
                this.ftppublickey = resp.PublicKey;
                this.usekeyfile = resp.UseKeyFile;
                this.ftpprivatekey = resp.KeyFile;
                this.ftppassphrase = resp.PassPhrase;
                this.ftpusername = resp.FTPUsername;


                if (this.usekeyfile == true) {
                    this.mandatory = true;
                    this.usekey = false;
                }
                else {
                    this.mandatory = false;
                    this.usekey = true;

                }

                this.ftpservervalue = resp.FTPServer;
                this.PortNovalue = resp.PortNumber;
                this.ftpprotocolFTPvalue = resp.ftpprotocolFTP;
                this.ftpprotocolSFTPvalue = resp.ftpprotocolSFTP;
                this.ftpprotocolFTPSIvalue = resp.ftpprotocolFTPSI;
                this.ftpprotocolFTPSEvalue = resp.ftpprotocolFTPSE;
                this.AdminCredentialsvalue = resp.UseAdminCredentials;
                this.CurrentUservalue = resp.UseCurrentUserCredentials;
                this.FollowCredentialsvalue = resp.UseSpecificCredentials;
                this.Usernamevalue = resp.SpecificUsername;
                this.Passwordvalue = resp.SpecificPassword;
                this.usekeyfilevalue = resp.UseKeyFile;
                this.ftpprivatekeyvalue = resp.KeyFile;
                this.ftppassphrasevalue = resp.PassPhrase;
                this.ftpusernamevalue = resp.FTPUsername;
                if (this.ftpprotocolSFTP == true) {
                    this.Onlysftp = true;
                }
                else {
                    this.Onlysftp = false;
                }
                if (this.Enterfolderchk == false) {
                    this.Enterpath = true;
                }
                if (this.FollowCredentials == false) {
                    this.username = true;
                    this.password = true;
                }
      this.ngxLoader.stop();
              }
    },(error)=>{
      this.ngxLoader.stop();
    
    });
  }
SFTPUseKeyFile(val:any)
{
  if (val == true) {
    this.usekeyfile = true;
    this.mandatory = true;
    this.usekey = false;

}
else {
    this.usekeyfile = false;
    this.mandatory = false;
    this.usekey = true;
}
}
SendEmail(val:any)
{
  if (val == '1') {
    this.DetermineAuto = true;
    this.Enterpath = true;
    this.Enterfolderchk = false;
}
if (val == '2') {
    this.DetermineAuto = false;
    this.Enterpath = false;
    this.Enterfolderchk = true;
}
}
Authentication(val:any)
{
  if (val == '1') {
    this.AdminCredentials = true;
    this.CurrentUser = false;
    this.FollowCredentials = false;
    this.username = true;
    this.password = true; 
  let url="api/FTPConnectorSettings/VerifyAdminCredentialsAreValid";
  this.ngxLoader.start();
   this.service.post(url,"").subscribe(resp => {
    if (resp=='-2') 
      {
        this.notification.openSnackBar(this.errorCodes["WAR014"].msg,"","warning-snackbar");
        this.ngxLoader.stop();
    }
   
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });
  }
  if (val == '2') {
    this.AdminCredentials = false;
    this.CurrentUser = true;
    this.FollowCredentials = false;
    this.username = true;
    this.password = true;
}
if (val == '3') {
    this.AdminCredentials = false;
    this.CurrentUser = false;
    this.FollowCredentials = true;
    this.username = false;
    this.password = false;
}
}
protocolchange(val:any)
{
  if (val == '1') {
    this.ftpprotocolFTP = true;
    this.ftpprotocolSFTP = false;
    this.ftpprotocolFTPSI = false;
    this.ftpprotocolFTPSE = false;
}
if (val == '2') {
    this.ftpprotocolFTP = false;
    this.ftpprotocolSFTP = true;
    this.ftpprotocolFTPSI = false;
    this.ftpprotocolFTPSE = false;
}
if (val == '3') {
    this.ftpprotocolFTP = false;
    this.ftpprotocolSFTP = false;
    this.ftpprotocolFTPSI = true;
    this.ftpprotocolFTPSE = false;
}
if (val == '4') {
    this.ftpprotocolFTP = false;
    this.ftpprotocolSFTP = false;
    this.ftpprotocolFTPSI = false;
    this.ftpprotocolFTPSE = true;
}
if (this.ftpprotocolSFTP==true) {
    this.Onlysftp = true;
}
else {
    this.Onlysftp = false;
}
}
onChange(event:any)
{
var formData = new FormData();
                var fileCtrl = document.getElementById("keyFile");
                var fileData = event.target.files[0];
                formData.append("file", fileData, fileData.name);


                let url="/api/FTPConnectorSettings/UploadKeyFile";
  
  this.ngxLoader.start();
   this.service.post(url,formData).subscribe(resp => {
    if (resp.Status=='SUCCESS') 
      {
        this.ftpprivatekey = resp.Data;

        this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");
        this.ngxLoader.stop();
    }
    else if(resp.Status='FAILED'){
      this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
      this.ngxLoader.stop();
  }
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });
}
FTPConnectorSave()
{
  this.isDirty=false;

  if (this.Enterfolderchk == true) {
    if (this.EnterHerepath == "" || this.EnterHerepath == null) {
        
        this.notification.openSnackBar(this.errorCodes["WAR004"].msg,"","warning-snackbar");
        return;
    }
}
if (this.usekeyfile == true) {
    if (this.ftpprivatekey == "" || this.ftpprivatekey == null) {
        this.notification.openSnackBar(this.errorCodes["WAR066"].msg,"","warning-snackbar");
        return;
    }
    if (this.ftpusername == "" || this.ftpusername == null) {
        this.notification.openSnackBar(this.errorCodes["WAR043"].msg,"","warning-snackbar");
        return;
    }
}
if (this.FollowCredentials == true) {
    if (this.Username == "" || this.Username == null || this.Password == null || this.Password == "") {
        this.notification.openSnackBar(this.errorCodes["WAR040"].msg,"","warning-snackbar");
        return;
    }
}
let url="api/FTPConnectorSettings/SaveSetting";
  let postData = {
    FTPServer: this.ftpserver,
                    PortNumber: this.PortNo,
                    ftpprotocolFTP: this.ftpprotocolFTP,
                    ftpprotocolSFTP: this.ftpprotocolSFTP,
                    ftpprotocolFTPSI: this.ftpprotocolFTPSI,
                    ftpprotocolFTPSE: this.ftpprotocolFTPSE,
                    SpecificUsername: this.Username,
                    SpecificPassword: this.Password,
                    UseSpecificEmailTarget: this.Enterfolderchk,
                    EmailTarget: this.EnterHerepath,
                    AutomaticEmail: this.DetermineAuto,
                    UseAdminCredentials: this.AdminCredentials,
                    UseCurrentUserCredentials: this.CurrentUser,
                    UseSpecificCredentials: this.FollowCredentials,
                    UseKeyFile: this.usekeyfile,
                    KeyFile: this.ftpprivatekey,
                    PassPhrase: this.ftppassphrase,
                    FTPUsername: this.ftpusername       
  }
  this.ngxLoader.start();
   this.service.post(url,postData).subscribe(resp => {
    if (resp==true) 
      {
        this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
        this.ngxLoader.stop();
this.LoadFTPConnectorSettings();
    }
    else {
      this.notification.openSnackBar(this.errorCodes["WAR015"].msg,"","warning-snackbar");
      this.ngxLoader.stop();
  }
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });
}
FtpFolderMapJobs()
{
  if ((this.ftpserver != this.ftpservervalue) || (this.PortNo != this.PortNovalue) || (this.ftpprotocolFTP != this.ftpprotocolFTPvalue)
                    || (this.ftpprotocolSFTP != this.ftpprotocolSFTPvalue) || (this.ftpprotocolFTPSI != this.ftpprotocolFTPSIvalue) || (this.ftpprotocolFTPSE != this.ftpprotocolFTPSEvalue)
                    || (this.AdminCredentials != this.AdminCredentialsvalue) || (this.CurrentUser != this.CurrentUservalue) || (this.FollowCredentials != this.FollowCredentialsvalue)
                    || (this.Username != this.Usernamevalue) || (this.Password != this.Passwordvalue) || (this.usekeyfile != this.usekeyfilevalue) || (this.ftpprivatekey != this.ftpprivatekeyvalue)
                    || (this.ftppassphrase != this.ftppassphrasevalue) || (this.ftpusername != this.ftpusernamevalue)) {
                    
                    this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");

                    return;
                }
                localStorage.removeItem('MapDoc');
                localStorage.removeItem('mapEmail');
                localStorage.removeItem('BoxMap');
                localStorage.removeItem('SPOMap');
                localStorage.removeItem('ODBMap');
                localStorage.removeItem('AzureBlobMap');
                localStorage.removeItem('FolderMap');
                localStorage.removeItem('EgnyteMap');
                localStorage.removeItem("UIPathMap");
                localStorage.removeItem("DropBoxMap");

                localStorage.setItem("FTPMap", "FTPJobs");
                this.router.navigate(['/JobManagement']);


}
TestConnector()
{
  this.displayStyle = "block";

  if ((this.ftpserver != this.ftpservervalue) || (this.PortNo != this.PortNovalue) || (this.ftpprotocolFTP != this.ftpprotocolFTPvalue)
                    || (this.ftpprotocolSFTP != this.ftpprotocolSFTPvalue) || (this.ftpprotocolFTPSI != this.ftpprotocolFTPSIvalue) || (this.ftpprotocolFTPSE != this.ftpprotocolFTPSEvalue)
                    || (this.AdminCredentials != this.AdminCredentialsvalue) || (this.CurrentUser != this.CurrentUservalue) || (this.FollowCredentials != this.FollowCredentialsvalue)
                    || (this.Username != this.Usernamevalue) || (this.Password != this.Passwordvalue) || (this.usekeyfile != this.usekeyfilevalue) || (this.ftpprivatekey != this.ftpprivatekeyvalue)
                    || (this.ftppassphrase != this.ftppassphrasevalue) || (this.ftpusername != this.ftpusernamevalue)) {
                    
                    this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");

                    return;
                }

  var idval = -1;
              switch (localStorage.getItem("AuthType")) {
                  case "LocalDirectory":
                      idval = 0;
                      break;
                  case "ActiveDirectory":
                      idval = 1;
                      break;
                  case "Docuware":
                      idval = 2;
                      break;
                  case "Egnyte":
                      idval = 3;
                      break;
                  case "MFiles":
                      idval = 4;
                      break;
                  case "CustomTarget":
                      idval = 99;
                      break;
              }
              let url=this.url+"api/User/GetLocalUsersByAuthId"
let postData={
AuthId:idval
}
              this.http.get(url,{params:postData}).subscribe(resp=>{
if(resp!=null)
{
this.ListOfUser=resp
if(this.ListOfUser.length>0)
{
this.ListOfUser=resp;
this.userlist = true;
this.txtuser=false;
}
else{ 
this.userlist = false;
this.txtuser=true;
}
}
else {
this.userlist = false;
this.txtuser = true;
}
              });

}
getUser(val:any)
  {
    let userId = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;
    this.testusername=userId;
  }
  closePopup() {
    this.displayStyle = "none";
  }
  Test(username:any,password:any,target:any)
  {
    if (target == undefined || target == "") {
      this.notification.openSnackBar(this.errorCodes["WAR066"].msg,"","warning-snackbar");
      return;
  }
  if (username == undefined || username == null) {
    username = "";
  }
  if (password == undefined || password == null) {
    password = "";
  }

  let url="api/User/TestConnector";
let postData={
  ProviderCode: "FTPCONNECTOR",
  UserName:username,
  Arguments: "-t=" + '"' + target + '"' + " " + "-p=" + '"' + password + '"'

}


this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
  {
if(resp.Status=='SUCCESS')
{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
else{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });

  }
}
