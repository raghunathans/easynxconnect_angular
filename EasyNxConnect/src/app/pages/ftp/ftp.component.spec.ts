import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FTPComponent } from './ftp.component';

describe('FTPComponent', () => {
  let component: FTPComponent;
  let fixture: ComponentFixture<FTPComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FTPComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FTPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
