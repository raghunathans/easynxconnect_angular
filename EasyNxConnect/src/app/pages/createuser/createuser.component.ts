import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/_api/api.service';
import { ConfirmationDialog, DialogData, NotificationService } from 'src/app/notification.service';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

export interface Language{
  Id:number;
  LanguageCode:string;
  LanguageName:string;
  RoleName:string;
}
export interface Role{
  Id:number;
  RoleName:string;
  RoleCode:string;
}
@Component({
  selector: 'app-createuser',
  templateUrl: './createuser.component.html',
  styleUrls: ['./createuser.component.scss']
})
export class CreateuserComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
   {
     return !this.isDirty
   }
   isDirty=false;

  languages:Language[]=[];
  roles:Role[]=[];

  Language:any;
  Role:any;
  user_name:string="";
  emailid:string="";
  home_folder:string="";
  nfccard_id:string="";
  first_name:string="";
  last_name:string="";
  awsaccesskey:string="";
  awssecretkey:string="";


  CRE_emailid:boolean = true;
  CRE_home_folder:boolean = true;
  CRE_nfccard_id:boolean = true;
  CRE_first_name:boolean = true;
  CRE_last_name:boolean = true;
  egn:any;
  LF:any;
  btnLF:any;
  btnLD:any;
  Languageshow:any
  errorCodes:any = {};

  constructor(private router:Router, private service:ApiService,private notification:NotificationService,private ErrorCodes:ErrorCodesService,private ngxLoader:NgxUiLoaderService) { }

  ngOnInit(): void {
    this.Languageshow = false;

    this.getLanguages();
this.getRoles();
this.errorCodes=this.ErrorCodes.errormsgs();


if (localStorage.getItem('AuthType') == "LaserFiche") {
  this.egn = false;
  this.LF = false;
  this.btnLF = true;
  this.btnLD = false;
}
else {
  this.egn = true;
  this.Languageshow = false;
  this.btnLF = false;                    ;
  this.btnLD = true;
  this.LF = true;
}
  }
  getRoles()
  {
    let url = "api/User/GetAllRoles?PreferredLanguage=" + localStorage.getItem('PreferredLanguage');
    this.service.getAPI(url).then(resp => {
      // this.Languageshow = ! this.Languageshow;
      this.roles = resp;
      // this.ngxLoader.stop();
    },(error)=>{
      // this.notification.openSnackBar(error.message,"","red-snackbar");
      // this.ngxLoader.stop();
    });
  }
  getLanguages()
  {
    let url = "api/User/GetLanguages?id=" + "";
    this.service.getAPI(url).then(resp => {
       this.Languageshow = ! this.Languageshow;
      this.languages = resp;
      // this.ngxLoader.stop();
    },(error)=>{
      // this.notification.openSnackBar(error.message,"","red-snackbar");
      // this.ngxLoader.stop();
    });

  }

  
  switchLang(event:any)
{

}
userchange(val:string)
{
  if (val != undefined) {
    this.CRE_emailid = false;
    this.CRE_home_folder = false;
    this.CRE_nfccard_id = false;
    this.CRE_first_name = false;
    this.CRE_last_name = false;
}
else {

    this.CRE_emailid = true;
    this.CRE_home_folder = true;
    this.CRE_nfccard_id = true;
    this.CRE_first_name = true;
    this.CRE_last_name = true;
}
}

CreateNewUser()
{
  this.isDirty=false;
  if (localStorage.getItem('AuthType') == "LaserFiche") {
    if ((this.user_name == '' || this.user_name == null)) {
        this.notification.openSnackBar(this.errorCodes["WAR035"].msg,"","warning-snackbar");
        return false;
    }
}
else {
    if ((this.user_name == '' || this.user_name == null) || (this.emailid == '' || this.emailid == null) || (this.first_name == '' || this.first_name == null)) {
        this.notification.openSnackBar(this.errorCodes["WAR035"].msg,"","warning-snackbar");
        return false;
    }
}
let url="api/User/CreateUser";
let postData={
  Authentication: "",
                    Username: this.user_name,
                    Email: this.emailid,
                    HomeFolder: this.home_folder,
                    NFCCardId: this.nfccard_id,
                    FirstName: this.first_name,
                    LastName: this.last_name,
                    AWSS3AccessID: this.awsaccesskey,
                    AWSS3SecretKey: this.awssecretkey,
                    Role: this.Role,
                    PreferredLanguage: this.Language
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>{
  if (resp == true) {
    this.notification.openSnackBar(this.errorCodes["SUC003"].msg,"","success-snackbar");
this.router.navigate(['/userconfiguresummary']);
this.ngxLoader.stop();
}
else if (resp.data == "UserExist") {
    this.notification.openSnackBar(this.errorCodes["WAR029"].msg,"","warning-snackbar");
    this.ngxLoader.stop();
}
else {
    this.notification.openSnackBar(this.errorCodes["ERR017"].msg,"","red-snackbar");
    this.ngxLoader.stop();
}
},(error)=>
{
  this.notification.openSnackBar(error.message,"","red-snackbar");
  this.ngxLoader.stop();

});
return;
}

}
