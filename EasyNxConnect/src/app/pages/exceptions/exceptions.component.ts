import { HttpClient } from '@angular/common/http';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { post } from 'jquery';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

export interface AllUserExceptions{
  BatchCount:number;
  UserId:number;
  Username:string;
}

export interface ViewBatchFiles{
  BatchName:any;
  ExceptionMessage:string;
  FileCreatedOn:any;
  FileName:any;
  FolderName:any;
  Id:any;
}
export interface ViewBatchs{
  BatchName:string;
  BatchParameter:string;
  CreatedBy:any;
  CreatedOn:any;
  ExceptionMessage:any;
  FileCount:number;
  Id:number;
  JobName:any;
  ModifiedBy:any;
  ModifiedOn:any;
  Operation:string;
  ProviderCode:string;
  ScannerName:string;
  UserId:number;
}
@Component({
  selector: 'app-exceptions',
  templateUrl: './exceptions.component.html',
  styleUrls: ['./exceptions.component.scss']
})
export class ExceptionsComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
  {
    return !this.isDirty
  }
  isDirty=false;

  AllUserException:[]=[];
  BatchName:any;
  AllException:boolean=true;
  BatchException:boolean=false;
  BatchExceptionname:boolean=false;
  GetAllException:boolean=true;
  ViewBatchshow:boolean = false;
  ViewBatchfile:boolean=false;
  providercodeval:any;
  userName:any;
  uid:any;
  errorCodes:any = {};
            ID:any;
            providerCode:string = "";
            parameters:any;
            paramUsername:any;
            paramPassword:any;
            password:any;
            usernameval:any;
            displayStyle = "none";
            displaydeletefile = "none";
            removebatch="none";
            retrybatch="none";

 dataSource: MatTableDataSource<AllUserExceptions>;
 ViewBatchdataSource: MatTableDataSource<ViewBatchs>;
 ViewBatchFiledataSource: MatTableDataSource<ViewBatchFiles>;

  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
@ViewChildren(MatSort) sort = new QueryList<MatSort>();

displayedColumns: string[] = ['Username','Batchcount'];
viewbatchdisplayedColumns: string[] = ['BatchName','CreatedOn','FileCount','Operation','BatchParameter','Action'];
viewbatchfiledisplayedColumns: string[] = ['Document','Exception','Action'];

  constructor(private http: HttpClient,public translate: TranslateService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) {
    this.dataSource = new MatTableDataSource<AllUserExceptions>();
    this.ViewBatchdataSource = new MatTableDataSource<ViewBatchs>();
    this.ViewBatchFiledataSource = new MatTableDataSource<ViewBatchFiles>();

   }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');

    this.translate.use(returnUrl);

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.LoadUserExceptionGrid();
  }

  LoadUserExceptionGrid()
  {

    let url="api/Exceptions/Index";
    let postData=
    {
      userId: localStorage.getItem('Id')
    }
    this.service.post(url,postData).subscribe(resp=>
      {
        if(resp!="No Data found")
        {
          this.dataSource = new MatTableDataSource(resp);
  this.dataSource.paginator = this.paginator.toArray()[0];
  this.dataSource.sort = this.sort.toArray()[0];
        }
      })
  }
  ViewBatch(id:number,username:any)
  {
    this.userName = username;
                this.uid = id;
    this.GetAllException = false;
                 this.ViewBatchfile = false;
                this.ViewBatchshow = true;
this.LoadViewBatch(id,username)

  }
LoadViewBatch(id:any,username:any)
{
  let url="api/Exceptions/ViewBatches";
  let postData={
    userId:id,
    username:username
  }
  this.service.post(url,postData).subscribe(resp=>
    {
      if (resp!= "No Data found") {
        if (resp.length > 0) {
          for (var i = 0; i < resp.length; i++) {
              var createdate = resp[i].CreatedOn.substring(0, resp[i].CreatedOn.indexOf('.'));
              createdate = createdate.replace(/[T]/g, ' ');
              resp[i].CreatedOn = createdate;
          }
      }
        this.ViewBatchdataSource = new MatTableDataSource(resp);
        this.ViewBatchdataSource.paginator = this.paginator.toArray()[0];
        this.ViewBatchdataSource.sort = this.sort.toArray()[0];
      }
    },(error)=>
      {
        this.notification.openSnackBar(error.message,"","red-snackbar");
      });
}
nameval:any;
idvalue:any;
  ViewBatchFiledetail(id:number,name:string,batchname:string,providercode:any,batchparameter:any)
  {
this.providercodeval=providercode;
var batchParam = decodeURIComponent(batchparameter);
var batchParams = this.getBatchParameterValues(batchParam, '-u', true);
var paramUsername = batchParams.value;
batchParams = this.getBatchParameterValues(batchParams.parameter, '-p', true);
var paramPassword = batchParams.value;
batchparameter = batchParams.parameter;
this.BatchName = batchname;
                this.ID = id;
                this.providerCode = providercode;
                this.parameters = batchparameter;
                this.paramUsername = paramUsername;
                this.paramPassword = paramPassword;
                
                this.ViewBatchfile = true;
                this.GetAllException = false;
                this.ViewBatchshow = false;
                this.AllException = false;
                this.BatchException = true;
                this.BatchExceptionname = true;
                if (name == null || name == undefined) {
                    name = this.paramUsername
                }
                if (this.paramPassword != null) {
                    this.password = this.paramPassword;
                }
                if (this.userName == "" || this.userName == null || this.userName == "AllUsers") {
                    if (this.paramUsername != null) {
                        this.usernameval = this.paramUsername;
                    }
                    if (this.paramPassword != null) {
                        this.password = this.paramPassword;
                    }
                }
                else {
                    this.usernameval = this.userName;
                }
                this.nameval=name;
                this.idvalue=id;
                this.LoadViewBatchFile(id,name)
                
  }

  LoadViewBatchFile(id:any,name:any)
  {
    let url="api/Exceptions/ViewFiles";
    let postData=
    {
      id: id,
      username: name
    }
    this.service.post(url,postData).subscribe(resp=>
      {
        if (resp != "No Data found") {
          this.ViewBatchFiledataSource = new MatTableDataSource(resp);
          this.ViewBatchFiledataSource.paginator = this.paginator.toArray()[0];
          this.ViewBatchFiledataSource.sort = this.sort.toArray()[0];
        }
      },(error)=>
      {
        this.notification.openSnackBar(error.message,"","red-snackbar");
      })
  }

   getBatchParameterValues(batchParameter:any, paramOption:any, remove:any) {
    var batchParams:any = {};
    batchParams.parameter = batchParameter;
    try {
        if (batchParameter && batchParameter.trim() != '' && paramOption.trim() != '') {
            var splitBatchParams = batchParameter.split(' ');
            if (splitBatchParams && splitBatchParams.length > 0) {
                splitBatchParams.forEach(function (value:any, index:any, array:any) {
                    if (value && value.trim().indexOf(paramOption) >= 0) {
                        var splitValue = value.trim().split('=');
                        if (splitValue && splitValue.length > 1) {
                            if (remove && splitValue[0] != '-proc') {
                                batchParams.parameter = batchParameter.replace(value, "");
                            }
                            var paramValue = splitValue[1].trim().replace(/"|'|\\/ig, "");
                            if (splitValue[0] != '-proc')
                                batchParams.value = paramValue.trim(); //set the password
                            else
                                batchParams.value = "";
                            return; //break the loop
                        }
                    }
                });
            }
        }
        else {
            console.log('getBatchParameterValues: Invalid batch parameter or param option');
        }
    }
    catch (ex) {
        console.log('getBatchParameterValues: ' + ex);
    }
    return batchParams;
}

ViewBatchFileview(id:any)
{
  let url="api/Exceptions/OpenFile";
  let postData={
    id: id
  }
  this.service.post(url,postData).subscribe(resp=>
    {
if(resp!=false)
{
  window.open(resp);
}
    },(error)=>
    {

    });

}
Back()
{
  this.GetAllException = true;
  this.ViewBatchshow = false;
  this.ViewBatchfile = false;
  this.AllException = true;
  this.BatchException = false;
  this.BatchExceptionname = false;
  this.LoadUserExceptionGrid();
  this.userName="";
  
}
BackFile()
{
  this.usernameval = '';
  this.GetAllException = false;
  this.ViewBatchfile = false;
  this.ViewBatchshow = true;
  this.BatchExceptionname = false;
  this.LoadViewBatch(this.uid,this.userName)
}
Idval:any;
Batchval:any;

DeleteBatch(id:any,batchname:any)
{
  this.displayStyle = "block";
this.Idval=id;
this.Batchval=batchname;
}
Confirm()
{
let url="api/Exceptions/DeleteBatch";
let postData={
  id: this.Idval,
  batchName: this.Batchval
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
  {
if(resp==true)
{
  this.notification.openSnackBar(this.errorCodes["SUC018"].msg,"","success-snackbar");
  this.BatchExceptionname = false;
  this.displayStyle = "none";
  this.LoadViewBatch(this.uid,this.userName)
  this.ngxLoader.stop();
}
  },(error)=>
  {
    this.ngxLoader.stop();
  });
  this.ngxLoader.stop();
}
closePopup()
{
  this.displayStyle = "none";
}
closePopupDFile()
{
  this.displaydeletefile = "none";
}
Iddfileval:any;
Filenameval:any;

DeleteBatchFile(id:any,filename:any)
{
  this.displaydeletefile = "block";
  this.Iddfileval=id;
  this.Filenameval=filename;
}
ConfirmDeleteFile()
{
let url="api/Exceptions/DeleteBatchFile";
let postData={
  id: this.Iddfileval,
  batchName: this.BatchName,
  fileName: this.Filenameval
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
  {
if(resp==true)
{
  this.displaydeletefile = "none";
  this.notification.openSnackBar(this.errorCodes["SUC017"].msg,"","success-snackbar");

  let url="api/Exceptions/ViewFiles"
  let postData={
    id: this.ID
  }
  this.service.post(url,postData).subscribe(resp=>
    {
      if (resp.length <= 0) {
        this.GetAllException = false;
        this.ViewBatchfile = false;
        this.ViewBatchshow = true;

        let url="api/Exceptions/ViewBatches";
        let postData={
          userId: this.uid,
          username: this.userName
        }
        this.service.post(url,postData).subscribe(resp=>
          {
            if (resp.length <= 0) {
              window.location.reload();
          }
          else {
              if (resp != "No Data found") {
                  this.LoadViewBatch(this.uid,this.userName);
                  this.ngxLoader.stop();
              }
              this.ngxLoader.stop();
            }
          })
      }
      else{
        this.LoadViewBatchFile(this.idvalue,this.nameval);
        this.ngxLoader.stop();
      }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    })
    this.ngxLoader.stop();
}
else{
  this.notification.openSnackBar(this.errorCodes["WAR019"].msg,"","warning-snackbar");
  this.ngxLoader.stop();
}
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });
  this.ngxLoader.stop();
}

RemoveBatch()
{
this.removebatch="block";
}
ConfirmDeleteBatch()
{
let url="api/Exceptions/DeleteBatch";
let postData={
  id: this.ID,
  batchName: this.BatchName
}
this.ngxLoader.start();

this.service.post(url,postData).subscribe(resp=>
  {
if(resp==true)
{
  this.removebatch="none";
  this.notification.openSnackBar(this.errorCodes["SUC017"].msg,"","success-snackbar");
  this.GetAllException = false;
  this.ViewBatchfile = false;
  this.ViewBatchshow = true;
  this.BatchExceptionname = false;

  let url="api/Exceptions/ViewBatches";
  let postData={
    userId: this.uid,
    username: this.userName
  }
  this.service.post(url,postData).subscribe(resp=>
    {
      if (resp.length <= 0) {
        window.location.reload();
    }
    else {
        if (resp != "No Data found") {
            this.LoadViewBatch(this.uid,this.userName);
            this.ngxLoader.stop();
        }
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
}
else{
  this.notification.openSnackBar(this.errorCodes["WAR020"].msg,"","warning-snackbar");
  this.ngxLoader.stop();
}
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });
}
closePopupDbatch()
{
  this.removebatch="none";
}
RetryBatch(parameter:any,username:any,password:any)
{
  this.isDirty=false;

  this.usernameval = username;
                this.parameters = parameter;
                this.password = password;
  if (this.providercodeval != 'AZUREBLOB') {
    if (this.userName == null || this.userName == "" || this.userName == "AllUsers") {
        if (this.usernameval == null || this.usernameval == "") {           
            this.notification.openSnackBar(this.errorCodes["WAR041"].msg,"","warning-snackbar");
            return;
        }
    }
}
  this.retrybatch="block";
}
ConfirmRetryBatch()
{
  let url="api/Exceptions/RetryUpload";
  let postData={
    id: this.ID,
    batchName: this.BatchName,
    username: this.usernameval,
    batchParameter: this.parameters,
    providercode: this.providerCode,
    password: this.password

  }
  this.ngxLoader.start();
  this.service.post(url,postData).subscribe(resp=>
    {
      if(resp==true)
      {
        this.notification.openSnackBar(this.errorCodes["SUC019"].msg,"","success-snackbar");
        this.ngxLoader.stop();
        this.retrybatch="block";
        window.location.reload();
      }
      else{
        this.ngxLoader.start();
        window.location.reload();
      }
    });
}
closePopupRetrybatch()
{
  this.retrybatch="none";

}
}
