import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeFolderComponent } from './home-folder.component';

describe('HomeFolderComponent', () => {
  let component: HomeFolderComponent;
  let fixture: ComponentFixture<HomeFolderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeFolderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeFolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
