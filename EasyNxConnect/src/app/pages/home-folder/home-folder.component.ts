import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { environment } from 'src/environments/environment';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

@Component({
  selector: 'app-home-folder',
  templateUrl: './home-folder.component.html',
  styleUrls: ['./home-folder.component.scss']
})
export class HomeFolderComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
   {
     return !this.isDirty
   }
   isDirty=false;

  AutoFolder:any;
  password:boolean=false;
  username:boolean=false;
  Password:string="";
  Username:string="";
  CurrentUser:any;
  AdminCredentials:any;
  FollowCredentials:any;
  Enterfolder:any;
  Enterfolderpath:string="";
  EnterFolder:any;
  Underfolderpath:any;
  folderpath:any;
  Underfolder:any;
  errorCodes:any = {};
  hide = true;

  testusername:any;
  testpassword:any;
  target:any;
  url:any
ListOfUser:any;
userlist:any;
txtuser:any;
  displayStyle = "none";


  constructor(private http: HttpClient,public translate: TranslateService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) { }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');

    this.translate.use(returnUrl);
    this.url = environment.hostUrl;

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.HomeFolderConnectorSettings();

  }
  HomeFolderConnectorSettings()
  {
    let url="api/HomeFolder/LoadHomeFolderSetting";
    let postData={
      optAutomaticFolder: "",
    }
     this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if(resp!=null)
      {
        this.AutoFolder = resp.AutomaticFolder;
                this.Underfolder = resp.UseParentFolder;
                this.EnterFolder = resp.UseSpecificFolder;
                this.Underfolderpath = resp.ParentFolder;
                this.Enterfolderpath = resp.SpecificFolder;
                this.AdminCredentials = resp.UseAdminCredentials;
                this.CurrentUser = resp.UseCurrentUserCredentials;
                this.FollowCredentials = resp.UseSpecificCredentials;
                this.Username = resp.SpecificUsername;
                this.Password = resp.SpecificPassword;
                if (this.Underfolder == false) {
                  this.folderpath = true;
              }
              if (this.EnterFolder == false) {
                  this.Enterfolder = true;
              }
              if (this.FollowCredentials == false) {
                  this.username = true;
                  this.password = true;
              }
              }
      this.ngxLoader.stop();
    },(error)=>{
      this.ngxLoader.stop();
    
    });
    
  }
  HomeFolderSave()
  {
    this.isDirty=false;
    if (this.Underfolderpath != null && this.Underfolderpath != "") {
      var reg = /^(([a-z]:\\)|(\\\\)){1}([a-zA-Z0-9_]+|(\d{1,3}.)*\\?)+$/ig;
      if (reg.test(this.Underfolderpath) == false) {
          this.notification.openSnackBar(this.errorCodes["WAR007"].msg,"","warning-snackbar");
          return;
      }
  }
  if (this.Enterfolderpath != null && this.Enterfolderpath != "") {
      var reg = /^(([a-z]:\\)|(\\\\)){1}([a-zA-Z0-9_]+|(\d{1,3}.)*\\?)+$/ig;
      if (reg.test(this.Enterfolderpath) == false) {
          this.notification.openSnackBar(this.errorCodes["WAR007"].msg,"","warning-snackbar");
          return;
      }
  }
  if (this.Underfolder == true) {
      if (this.Underfolderpath == "" || this.Underfolderpath == null) {
          this.notification.openSnackBar(this.errorCodes["WAR004"].msg,"","warning-snackbar");
          return;
      }
  }
  if (this.EnterFolder == true) {
      if (this.Enterfolderpath == "" || this.Enterfolderpath == null) {
        this.notification.openSnackBar(this.errorCodes["WAR004"].msg,"","warning-snackbar");
          return;
      }
  }
  if (this.FollowCredentials == true) {
      if (this.Username == "" || this.Username == null || this.Password == null || this.Password == "") {
          this.notification.openSnackBar(this.errorCodes["WAR040"].msg,"","warning-snackbar");
          return;
      }
  }
  let url="api/HomeFolder/SaveSetting";
  let postData = {
    AutomaticFolder: this.AutoFolder,
    UseParentFolder: this.Underfolder,
    UseSpecificFolder: this.EnterFolder,
    ParentFolder: this.Underfolderpath,
    SpecificFolder: this.Enterfolderpath,
    UseAdminCredentials: this.AdminCredentials,
    UseCurrentUserCredentials: this.CurrentUser,
    UseSpecificCredentials: this.FollowCredentials,
    SpecificUsername: this.Username,
    SpecificPassword: this.Password           
  }
  this.ngxLoader.start();
   this.service.post(url,postData).subscribe(resp => {
    if (resp==true) 
      {
        this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
        this.ngxLoader.stop();
this.HomeFolderConnectorSettings();
    }
    else {
      this.notification.openSnackBar(this.errorCodes["WAR003"].msg,"","warning-snackbar");
      this.ngxLoader.stop();
  }
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });
  }
  CopytoFolder(val:any)
  {
    if (val == '1') {
      this.AutoFolder = true;
      this.Underfolder = false;
      this.EnterFolder = false;
      this.folderpath = true;
      this.Enterfolder = true;
  }
  if (val == '2') {
      this.AutoFolder = false;
      this.Underfolder = true;
      this.EnterFolder = false;
      this.folderpath = false;
      this.Enterfolder = true;

  }
  if (val == '3') {
      this.AutoFolder = false;
      this.Underfolder = false;
      this.EnterFolder = true;
      this.folderpath = true;
      this.Enterfolder = false;
  }
  }
  Authentication(val:any)
  {
    if (val == '1') {
      this.AdminCredentials = true;
      this.CurrentUser = false;
      this.FollowCredentials = false;
      this.username = true;
      this.password = true;
      
      this.ngxLoader.start();
      let url='api/EmailConnectorSettings/VerifyAdminCredentialsAreValid';
      this.service.post(url,"").subscribe(resp => {
       if (resp=='-2') 
         {
           this.notification.openSnackBar(this.errorCodes["WAR024"].msg,"","warning-snackbar");
           this.ngxLoader.stop();
       }
     },(error)=>
     {
       this.notification.openSnackBar(error.message,"","red-snackbar");
       this.ngxLoader.stop();
     });
    }
  if (val == '2') {
      this.AdminCredentials = false;
      this.CurrentUser = true;
      this.FollowCredentials = false;
      this.username = true;
      this.password = true;
  }
  if (val == '3') {
      this.AdminCredentials = false;
      this.CurrentUser = false;
      this.FollowCredentials = true;
      this.username = false;
      this.password = false;
  }
  }
  HomeFolderMapJobs()
  {
    localStorage.removeItem('MapDoc');
    localStorage.removeItem('mapEmail');
    localStorage.removeItem('BoxMap');
    localStorage.removeItem('SPOMap');
    localStorage.removeItem('ODBMap');
    localStorage.removeItem("UIPathMap");
    localStorage.removeItem('AzureBlobMap');
    localStorage.removeItem('FTPMap');
    localStorage.removeItem("mapMFiles");
    localStorage.removeItem('EgnyteMap');
    localStorage.removeItem('Map');
    localStorage.removeItem("DropBoxMap");

    localStorage.setItem("FolderMap", "FolderJobs");
this.router.navigate(['/JobManagement']);
  }
  TestConnector()
  {
    this.displayStyle = "block";

    var idval = -1;
                switch (localStorage.getItem("AuthType")) {
                    case "LocalDirectory":
                        idval = 0;
                        break;
                    case "ActiveDirectory":
                        idval = 1;
                        break;
                    case "Docuware":
                        idval = 2;
                        break;
                    case "Egnyte":
                        idval = 3;
                        break;
                    case "MFiles":
                        idval = 4;
                        break;
                    case "CustomTarget":
                        idval = 99;
                        break;
                }
                let url=this.url+"api/User/GetLocalUsersByAuthId"
let postData={
  AuthId:idval
}
                this.http.get(url,{params:postData}).subscribe(resp=>{
if(resp!=null)
{
  this.ListOfUser=resp
  if(this.ListOfUser.length>0)
{
  this.ListOfUser=resp;
  this.userlist = true;
  this.txtuser=false;
}
else{ 
  this.userlist = false;
  this.txtuser=true;
}
}
else {
  this.userlist = false;
  this.txtuser = true;
}
                });
  }
  getUser(val:any)
  {
    let userId = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;
    this.testusername=userId;
  }
  closePopup() {
    this.displayStyle = "none";
  }
  Test(testusername:any,testpassword:any,target:any)
  {
    if (this.target == undefined || this.target == "") {
      this.notification.openSnackBar(this.errorCodes["WAR066"].msg,"","warning-snackbar");
      return;
  }
  if (this.testusername == undefined || this.testusername == null) {
    this.testusername = "";
  }
  if (this.testpassword == undefined || this.testpassword == null) {
    this.testpassword = "";
  }

  let url="api/User/TestConnector";
  let postData:any;
if (this.testpassword == "") {
   postData = {
      ProviderCode: "FOLDERCONNECTOR",
      UserName: this.testusername,
      Arguments: "-t=" + '"' + target + '"'

  }
}
else {
   postData = {
      ProviderCode: "FOLDERCONNECTOR",
      UserName: this.testusername,
      Arguments: "-t=" + '"' + target + '"' + " " + "-p=" + '"' + this.testpassword + '"'

  }
}

this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
  {
if(resp.Status=='SUCCESS')
{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
else{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });

  }
}
