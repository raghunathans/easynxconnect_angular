import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UIPathComponent } from './uipath.component';

describe('UIPathComponent', () => {
  let component: UIPathComponent;
  let fixture: ComponentFixture<UIPathComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UIPathComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UIPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
