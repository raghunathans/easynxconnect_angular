import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { environment } from 'src/environments/environment';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

@Component({
  selector: 'app-uipath',
  templateUrl: './uipath.component.html',
  styleUrls: ['./uipath.component.scss']
})
export class UIPathComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
  {
    return !this.isDirty
  }
  isDirty=false;

  domainurl:string="";
  useCloud:any;
  tenantname:any;
  username:string="";
  password:string="";
  filesize:any;
  spositeurl:any;
  sharepointusername:any;
  sharepointpassword:any;
  spodocumentlibrary:any;
  hidefileuploadfield:boolean=false;
  errorCodes:any = {};
  folderpath = "";
  passwordvalue:any;
  usernamevalue:any;
  tenantnamevalue:any;
  domainurlvalue:any;
  btnweblogin:boolean = true;
  winWidth:any;
  winHeight:any;
  uipathcloud:boolean = false;
  usecloudtrue:any;
  usecloudfalse:any;
  testusername:any;
  foldername:any;
  processname:any;
  sitepath:any;
  doclibrary:any;
  url:any
ListOfUser:any;
userlist:any;
txtuser:any;
  displayStyle = "none";


  constructor(private http: HttpClient,public translate: TranslateService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) { }

  ngOnInit(): void {
    
    let returnUrl:any = localStorage.getItem('PreferredLanguage');
    this.url = environment.hostUrl;

    this.translate.use(returnUrl);

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.LoadUIPathConnectorSettings();
  }
  LoadUIPathConnectorSettings()
  {
    let url="api/UIPathSettings/LoadUIPathSetting";
    let postData={
      Configurationfolderpath: "",
    }
     this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if(resp!=null)
      {
        this.domainurl = resp.DomainURL;
        this.tenantname = resp.Tenantname;
        this.username = resp.Username;
        this.password = resp.Password;
        this.filesize = resp.FileUploadsize;
        this.spositeurl = resp.SharepointSiteURL;
        this.spodocumentlibrary = resp.Spodocumentlibrary;
        this.sharepointusername = resp.SharePointUsername;
        this.sharepointpassword = resp.SharePointPassword;
       this.folderpath = "";
        this.useCloud = resp.UseCloud;
        this.domainurlvalue = resp.DomainURL;
        this.tenantnamevalue = resp.Tenantname;
        this.usernamevalue = resp.Username;
        this.passwordvalue = resp.Password;

                }
      this.ngxLoader.stop();
    },(error)=>{
      this.ngxLoader.stop();
    
    });
    
  }
  clearValues(val:any)
  {
    this.username = "";
    this.password = "";
  }
  SaveUIpath()
  {
    this.isDirty=false;
    if ((this.spositeurl == "" || this.spositeurl == null) || (this.domainurl == "" || this.domainurl == null) || (this.tenantname == "" || this.tenantname == null)
    || (this.username == "" || this.username == null) || (this.password == "" || this.password == null)) {
    this.notification.openSnackBar(this.errorCodes["WAR035"].msg,"","warning-snackbar");
    return;
}
    let url="api/UIPathSettings/SaveSetting";
    let postData = {
      DomainURL: this.domainurl,
      Tenantname: this.tenantname,
      Username: this.username,
      Password: this.password,
      FileUploadsize: this.filesize,
      SharepointSiteURL: this.spositeurl,
      Spodocumentlibrary: this.spodocumentlibrary,
      SharePointUsername: this.sharepointusername,
      SharePointPassword: this.sharepointpassword,
      FolderPath: "",
      UseCloud: this.useCloud,
                
    }
    this.ngxLoader.start();
     this.service.post(url,postData).subscribe(resp => {
      if (resp==0) 
        {
          this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
          this.ngxLoader.stop();
this.LoadUIPathConnectorSettings();
      }
      else if (resp == -2) {
        
        this.notification.openSnackBar(this.errorCodes["WAR070"].msg,"","warning-snackbar");
        this.ngxLoader.stop();
      }
      else {
        this.notification.openSnackBar(this.errorCodes["WAR003"].msg,"","warning-snackbar");
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
  WebLogin()
  {
    this.winWidth = "900";
    this.winHeight = "400";
    var left = (screen.width - this.winWidth) / 2;
    var top = (screen.height - this.winHeight) / 4;
    var webLoginUrl = location.origin + "/EasyNxConnectWebApi/mvc/WebLogin/Index?username=" + localStorage.getItem("username");
    window.open(webLoginUrl, "_blank", "toolbar=no,addressbar=no, location=no, directories=no, status=no, menubar=yes, scrollbars=yes, resizable=no, copyhistory=yes,width=" + this.winWidth + ", height=" + this.winHeight + ", top=" + top + ", left=" + left + "");

  }
  TestConnector()
  {
if(this.useCloud==true)
{
  this.usecloudtrue=true;
  this.usecloudfalse=false;
}
else{
  this.usecloudtrue=false;
  this.usecloudfalse=true;
}

this.displayStyle = "block";
var idval = -1;
switch (localStorage.getItem("AuthType")) {
    case "LocalDirectory":
        idval = 0;
        break;
    case "ActiveDirectory":
        idval = 1;
        break;
    case "Docuware":
        idval = 2;
        break;
    case "Egnyte":
        idval = 3;
        break;
    case "MFiles":
        idval = 4;
        break;
    case "CustomTarget":
        idval = 99;
        break;
}
let url=this.url+"api/User/GetLocalUsersByAuthId"
let postData={
  AuthId:idval
}
this.http.get(url,{params:postData}).subscribe(resp=>{
if(resp!=null)
{
  this.ListOfUser=resp
  if(this.ListOfUser.length>0)
{
  this.ListOfUser=resp;
  this.userlist = true;
  this.txtuser=false;
}
else{ 
  this.userlist = false;
  this.txtuser=true;
}
}
else {
  this.userlist = false;
  this.txtuser = true;
}
  });
  }
  closePopup() {
    this.displayStyle = "none";
  }
  getUser(val:any)
  {
    let userId = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;
    this.testusername=userId;
  }
  Test(username:any, foldername:any, process:any)
  {
    if (username == undefined || username == null) {
      username = "";
  }
    let url="api/User/TestConnector";
  let postData={
    ProviderCode: "UIPATH",
                    UserName: username,
                    Arguments: "-proc=" + '"' + process + '"' + " " + "-mf=" + '"' + foldername + '"'
               
  }
  this.ngxLoader.start();
  this.service.post(url,postData).subscribe(resp=>
    {
  if(resp.Status=='SUCCESS')
  {
    this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");
    this.ngxLoader.stop();
    this.displayStyle = "none";
  
  }
  else{
    this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
    this.ngxLoader.stop();
    this.displayStyle = "none";
  
  }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
}
