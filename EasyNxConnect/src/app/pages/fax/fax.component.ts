import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { environment } from 'src/environments/environment';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

@Component({
  selector: 'app-fax',
  templateUrl: './fax.component.html',
  styleUrls: ['./fax.component.scss']
})
export class FaxComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
   {
     return !this.isDirty
   }
   isDirty=false;

  FaxDomain:string="";
  AuthSMTP:any;
  AuthUser:any;
  AuthSpecific:any;
  username:any;
  FaxUsername:string="";
  password:any;
  FaxPassword:string="";
  SenderSMTP:any;
  SenderUser:any;
  SenderSpecific:any;
  faxsendername:any;
  FaxSenderName:string="";
  CoverNone:any;
  CoverJobBased:any;
  CoverSpecific:any;
  FaxBody:any;
  FaxDomainvalue:any;
  ShowFaxAuthentication:boolean=false;
  errorCodes:any = {};
  testusername:any;
  target:any;
  url:any
ListOfUser:any;
userlist:any;
txtuser:any;
  displayStyle = "none";

  constructor(private http: HttpClient,public translate: TranslateService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) { }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');

    this.translate.use(returnUrl);
    this.url = environment.hostUrl;

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.LoadFaxConnectorSettings();

  }
  LoadFaxConnectorSettings()
  {
    let url="api/FaxConnectorSettings/LoadFaxControllerSetting";
    let postData={
      FaxEmailDomain: this.FaxDomain,
    }
     this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if(resp!=null)
      {
        this.FaxDomain = resp.FaxEmailDomain;
        this.AuthSMTP = resp.AuthSMTP;
        this.AuthUser = resp.AuthCurrentUser;
        this.AuthSpecific = resp.AuthSpecificUser;
        this.FaxUsername = resp.FaxUsername;
        this.FaxPassword = resp.FaxPassword;
        this.SenderSMTP = resp.SenderSMTP;
        this.SenderUser = resp.SenderCurrentUser;
        this.SenderSpecific = resp.SenderSpecificUser;
        this.FaxSenderName = resp.FaxSenderName;
        this.CoverNone = resp.CoverNone;
        this.CoverJobBased = resp.CoverJobBased;
        this.CoverSpecific = resp.CoverBody;
        this.FaxBody = resp.FaxBody;

        this.FaxDomainvalue = resp.FaxEmailDomain;

      this.ngxLoader.stop();
              }
    },(error)=>{
      this.ngxLoader.stop();
    
    });
  }
  AuthFax(val:any)
  {
    if (val == '1') {
      this.AuthSMTP = true;
      this.AuthUser = false;
      this.AuthSpecific = false;
  }
  if (val == '2') {
      this.AuthSMTP = false;
      this.AuthUser = true;
      this.AuthSpecific = false;
  }
  if (val == '3') {
      this.AuthSMTP = false;
      this.AuthUser = false;
      this.AuthSpecific = true;
  }
  }
  FaxSender(val:any)
  {
    if (val == '1') {
      this.SenderSMTP = true;
      this.SenderUser = false;
      this.SenderSpecific = false;
  }
  if (val == '2') {
      this.SenderSMTP = false;
      this.SenderUser = true;
      this.SenderSpecific = false;
  }
  if (val == '3') {
      this.SenderSMTP = false;
      this.SenderUser = false;
      this.SenderSpecific = true;
  }
  }
  FaxCover(val:any)
  {
    if (val == '1') {
      this.CoverNone = true;
      this.CoverJobBased = false;
      this.CoverSpecific = false;
  }
  if (val == '2') {
      this.CoverNone = false;
      this.CoverJobBased = true;
      this.CoverSpecific = false;
  }
  if (val == '3') {
      this.CoverNone = false;
      this.CoverJobBased = false;
      this.CoverSpecific = true;
  }
  }
  FaxConnectorSave()
  {
this.isDirty=false;
    if (this.AuthSpecific == true) {
      if (this.FaxUsername == null || this.FaxUsername == "") {
        this.notification.openSnackBar(this.errorCodes["WAR008"].msg,"","warning-snackbar");
          return;
      }
  }
  if (this.SenderSpecific == true) {
      if (this.FaxSenderName == null || this.FaxSenderName == "") {
        this.notification.openSnackBar(this.errorCodes["WAR008"].msg,"","warning-snackbar");
          return ;
      }

  }


    let url="api/FaxConnectorSettings/SaveSetting";
  let postData = {
    FaxEmailDomain: this.FaxDomain,
                    AuthSMTP: this.AuthSMTP,
                    AuthCurrentUser: this.AuthUser,
                    AuthSpecificUser: this.AuthSpecific,
                    FaxUsername: this.FaxUsername,
                    FaxPassword: this.FaxPassword,
                    SenderSMTP: this.SenderSMTP,
                    SenderCurrentUser: this.SenderUser,
                    SenderSpecificUser: this.SenderSpecific,
                    FaxSenderName: this.FaxSenderName,
                    CoverNone: this.CoverNone,
                    CoverJobBased: this.CoverJobBased,
                    CoverBody: this.CoverSpecific,
                    FaxBody: this.FaxBody       
  }
  this.ngxLoader.start();
   this.service.post(url,postData).subscribe(resp => {
    if (resp==true) 
      {
        this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
        this.ngxLoader.stop();
this.LoadFaxConnectorSettings();
    }
    else {
      this.notification.openSnackBar(this.errorCodes["WAR016"].msg,"","warning-snackbar");
      this.ngxLoader.stop();
  }
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });
  }

  FaxCoverBodyEdit()
  {
    let url="api/FaxConnectorSettings/LoadFaxControllerSetting";
    let postData={
      FaxBody: ""

    }
     this.ngxLoader.start();
     this.service.post(url,postData).subscribe(resp => {
      if (resp.FaxBody != "") 
        {
          this.FaxBody = resp.FaxBody;

          this.ngxLoader.stop();
      }
      else {
        this.FaxBody = this.errorCodes["RES012"].res;
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
  FaxCoverSave()
  {
    let url="api/FaxConnectorSettings/SaveFaxBodySetting";
    let postData = {
      FaxBody: this.FaxBody,
    }
    this.ngxLoader.start();
     this.service.post(url,postData).subscribe(resp => {
      if (resp==true) 
        {
          this.ngxLoader.stop();
      }
      else {
        this.notification.openSnackBar(this.errorCodes["WAR003"].msg,"","warning-snackbar");
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
  CreateFaxJobs()
  {
    if (this.FaxDomain != this.FaxDomainvalue) {
      this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");
      return;
  }
  localStorage.removeItem('Map');
  localStorage.removeItem('BoxMap');
  localStorage.removeItem('ODBMap');
  localStorage.removeItem('EgnyteMap');
  localStorage.removeItem('FTPMap');
  localStorage.removeItem('FolderMap');
  localStorage.removeItem("AzureBlobMap");
  localStorage.removeItem("SPOMap");
  localStorage.removeItem("MapDoc");
  localStorage.removeItem("mapMFiles");
  localStorage.removeItem("mapEmail");
  localStorage.setItem('mapFax', 'FaxMap');
  this.router.navigate(['/JobManagement']);

  }
  TestConnector()
  {
    this.displayStyle = "block";

    if (this.FaxDomain != this.FaxDomainvalue) {
      this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");
      return;
  }
  var idval = -1;
  switch (localStorage.getItem("AuthType")) {
      case "LocalDirectory":
          idval = 0;
          break;
      case "ActiveDirectory":
          idval = 1;
          break;
      case "Docuware":
          idval = 2;
          break;
      case "Egnyte":
          idval = 3;
          break;
      case "MFiles":
          idval = 4;
          break;
      case "CustomTarget":
          idval = 99;
          break;
  }
  let url=this.url+"api/User/GetLocalUsersByAuthId"
let postData={
  AuthId:idval
}
                this.http.get(url,{params:postData}).subscribe(resp=>{
if(resp!=null)
{
  this.ListOfUser=resp
  if(this.ListOfUser.length>0)
{
  this.ListOfUser=resp;
  this.userlist = true;
  this.txtuser=false;
}
else{ 
  this.userlist = false;
  this.txtuser=true;
}
}
else {
  this.userlist = false;
  this.txtuser = true;
}
                });
  }
  getUser(val:any)
  {
    let userId = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;
    this.testusername=userId;
  }
  Test(testusername:any,target:any)
  {
    if (target == undefined || target == "") {
      
      this.notification.openSnackBar(this.errorCodes["WAR066"].msg,"","warning-snackbar");

      return;
  }
  let url="api/User/TestConnector";
let postData={
  ProviderCode: "EMAILCONNECTOR",
                    UserName: this.testusername,
                    Arguments: "-t=" + '"' + target + '" -fax'
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
  {
if(resp.Status=='SUCCESS')
{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
else{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });

  }
  closePopup() {
    this.displayStyle = "none";
  }
}
