import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrinterConnectorComponent } from './printer-connector.component';

describe('PrinterConnectorComponent', () => {
  let component: PrinterConnectorComponent;
  let fixture: ComponentFixture<PrinterConnectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrinterConnectorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrinterConnectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
