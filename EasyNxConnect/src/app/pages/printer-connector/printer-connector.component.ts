import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { StoredataService } from 'src/app/storedata.service';
import { ApiService } from 'src/app/_api/api.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-printer-connector',
  templateUrl: './printer-connector.component.html',
  styleUrls: ['./printer-connector.component.scss']
})
export class PrinterConnectorComponent implements OnInit {

  templateok = true;
  templateclear = true;
  folderTargets:any = [];
  Joblist:any = [];
  DeleteJoblist = [];
  DeleteJobGrouplist = [];
  errorCodes:any = {};
  Jobs:any=[];
  Jobgroups:any;
  Printers:any;
  JobName:string="";
  templateJobName:string="";
  editJobName:any;
  editJobNameId:any;
  Jobname:string="";
  Description:string="";
  IPprofiles:any;
  ProfileName:any;
  buttoncolor:any;
  MetaDataName:string="";
  MetaDataType:any;
  multipleedit:any;
  Include:boolean=false;
  testusername:any;
  target:any;
  url:any
ListOfUser:any;
userlist:any;
txtuser:any;
  displayStyle = "none";

  constructor(private http: HttpClient,public translate: TranslateService,private dataStore:StoredataService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) { }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');

    this.translate.use(returnUrl);    
    this.url = environment.hostUrl;

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.LoadJobs();
    this.LoadJobGroups();
this.LoadPrinters();
this.Includeprinters();
  }
  LoadJobs()
  {
    let url="api/Jobs/GetJobsList";
    this.service.getAPI(url).then((resp:any[])=>{
this.Jobs=resp;
    });

  }

  LoadJobGroups()
  {
    let url="api/Jobs/GetJobGroups";
    this.service.getAPI(url).then(resp=>{
this.Jobgroups=resp;
  });
}

LoadPrinters()
{
  let url="api/Printer/GetAllPrinters";
    this.service.getAPI(url).then(resp=>{
this.Printers=resp;
  });
}


Includechk(value:boolean)
{
let url="api/Printer/SaveSetting";
let postData={
  Include:value
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
  {
 window.location.reload();
this.ngxLoader.stop();
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });

}
Includeprinters()
{
  let url="api/Printer/LoadIncludePrinter";
  let postData={
   Include:""
 }
 this.service.post(url,postData).subscribe(resp=>
   {
     this.Include=resp.Include;
 this.ngxLoader.stop();
   },(error)=>
   {
     this.notification.openSnackBar(error.message,"","red-snackbar");
     this.ngxLoader.stop();
   });
}
JobMap()
  {
    for (var i = 0; i < this.Printers.length; i++) {
      if (this.Printers[i].Selected) {
          var printername = this.Printers[i].PrinterName;
          this.folderTargets.push({ "JobName": printername, "TargetFolderName": printername, "TargetSiteName": null, "DocLibrarytarget": null });
      }
  }
    if (this.Jobs != undefined && this.Jobs.length > 0) {
        for (var i = 0; i < this.Jobs.length; i++) {
            this.JobName = this.Jobs[i].JobName;
            this.Joblist.push(this.JobName);
        }
    }
    if (this.folderTargets.length <= 0) {
      this.notification.openSnackBar(this.errorCodes["WAR034"].msg,"","warning-snackbar");
      return;
  }
    let url="api/Jobs/CreateJob";

        let postData = {
            JobName: "",
            ConnectorName: "Send To Printer",
            ConnectorPrefix: "Print",
            ConnectorSuffix: "",
            Description: "Send documents to specified printer",
            XmlFileName: "connector_info_sendtoprinter.xml",
            JobFileFormat: "pdf",
            JobDestination: "",
            JobTemplate: this.templateJobName,
            JobTargets: this.folderTargets,
            AvailableJobs: this.Joblist
        }
        this.ngxLoader.start();
        this.service.post(url,postData).subscribe(resp => {
         if(resp==true)
         {
            this.notification.openSnackBar(this.errorCodes["SUC008"].msg,"","success-snackbar");
            this.folderTargets=[];
            this.LoadJobs();
            this.LoadJobGroups();
            this.ngxLoader.stop();   
         } 
         else{
            this.notification.openSnackBar(this.errorCodes["WAR010"].msg,"","warning-snackbar");
            this.Joblist = [];
                        this.folderTargets = [];
         }
        },(error)=>
        {
          this.notification.openSnackBar(error.message,"","red-snackbar");
          this.ngxLoader.stop();   
        })

    
  }

  JobEdit()
  {

    for (var i = 0; i < this.Jobs.length; i++) {
        if (this.Jobs[i].Selected) {
            this.editJobName = this.Jobs[i].JobName;
            this.editJobNameId = this.Jobs[i].Id;
        }
    }
    if (this.editJobName == undefined || this.editJobName == "") {
        this.notification.openSnackBar(this.errorCodes["WAR034"].msg,"","warning-snackbar");
        return;
    }

    let urldtl="api/Jobs/GetJobInfo?jobName="+this.editJobName;
    this.service.getAPI(urldtl).then(resp=>{
        this.editJobName = "";
        var val = resp.ButtonColor;
        var buttoncolor = val.toLowerCase();
        this.Jobname = resp.JobName;
        this.Description = resp.Description;
        this.buttoncolor = buttoncolor;
        this.ProfileName = resp.ProfileName;
        if (resp.metaDataEntry == false) {
            this.MetaDataName = "";
            this.MetaDataType = "";
        }
        else {
            this.MetaDataName = resp.MetaDataName;
            this.MetaDataType = resp.MetaDataType;
        }
});

    let url="api/Jobs/GetJobprofiles";
    this.service.getAPI(url).then(resp=>{
this.IPprofiles=resp;
    });
}

Templatejobmap()
  {
    for (var i = 0; i < this.Jobs.length; i++) {
        if (this.Jobs[i].Selected) {
            this.templateJobName = this.Jobs[i].JobName;
            this.templateclear = false;
        }
    }
  }
  clearjob()
  {
    this.templateJobName = "";
    this.templateclear = true;
  }
JobGroupscreate()
{
    this.router.navigate(['/CreateJobGroups'])
}
TestConnector()
{
  this.displayStyle = "block";
  var idval = -1;
  switch (localStorage.getItem("AuthType")) {
      case "LocalDirectory":
          idval = 0;
          break;
      case "ActiveDirectory":
          idval = 1;
          break;
      case "Docuware":
          idval = 2;
          break;
      case "Egnyte":
          idval = 3;
          break;
      case "MFiles":
          idval = 4;
          break;
      case "CustomTarget":
          idval = 99;
          break;
  }
  let url=this.url+"api/User/GetLocalUsersByAuthId"
  let postData={
    AuthId:idval
  }
  this.http.get(url,{params:postData}).subscribe(resp=>{
  if(resp!=null)
  {
    this.ListOfUser=resp
    if(this.ListOfUser.length>0)
  {
    this.ListOfUser=resp;
    this.userlist = true;
    this.txtuser=false;
  }
  else{ 
    this.userlist = false;
    this.txtuser=true;
  }
  }
  else {
    this.userlist = false;
    this.txtuser = true;
  }
    });
  

  }
  closePopup() {
    this.displayStyle = "none";
  }
  getUser(val:any)
  {
    let userId = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;
    this.testusername=userId;
  }
  Test(username:any,target:any)
{
  if (target == undefined || target == "") {
    
    this.notification.openSnackBar(this.errorCodes["WAR066"].msg,"","warning-snackbar");

    return;
}
let url="api/User/TestConnector";
let postData={
  ProviderCode: "PRINTERCONNECTOR",
                    UserName: username,
                    Arguments: "-t=" + '"' + target + '"'
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
  {
if(resp.Status=='SUCCESS')
{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
else{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });
}

}
