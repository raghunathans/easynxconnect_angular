import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditJobGroupsComponent } from './edit-job-groups.component';

describe('EditJobGroupsComponent', () => {
  let component: EditJobGroupsComponent;
  let fixture: ComponentFixture<EditJobGroupsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditJobGroupsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditJobGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
