import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { StoredataService } from 'src/app/storedata.service';
import { ApiService } from 'src/app/_api/api.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FormsModule } from '@angular/forms';

declare var $: any;
import { ComponentCanDeactivate } from '../../component-can-deactivate';


@Component({
  selector: 'app-edit-job-groups',
  templateUrl: './edit-job-groups.component.html',
  styleUrls: ['./edit-job-groups.component.scss']
})
export class EditJobGroupsComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
   {
     return !this.isDirty
   }
   isDirty=false;


  errorCodes:any = {};

  Repository:any=[];
  Users:any=[];
  jobgroupdtl:any;
  JobGroup:any;
  JobgroupId:any;
  url:any
  user:any;
  JobGroupInfo:any=[];
  public static AllJobGroups:any=[];
  my_data = [];
  mytreedata:any;
   ENXJobGroup:any = [];
   JGID:any = 1;
   Jobgroupname:any;
   description:any;
   Idval:any;
   jobgroupeditdialog="none";
   tree:any;
   my_tree:any;
  constructor(private http: HttpClient,public translate: TranslateService,public dataStore:StoredataService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) {
  }
  ngOnInit(): void {
    this.my_tree = this.tree = {};
    let returnUrl:any = localStorage.getItem('PreferredLanguage');

    this.url = environment.hostUrl;

    this.translate.use(returnUrl);
    this.jobgroupdtl=this.dataStore.Jobgroup;

    if(this.jobgroupdtl!=null)
    {
this.JobGroup=this.jobgroupdtl.JobGroup;
this.JobgroupId=this.jobgroupdtl.JobgroupId;
localStorage.setItem('jobgroupval', this.JobGroup);

    }
    this.errorCodes=this.ErrorCodes.errormsgs();
     this.GetJobs();

       this.GetUsers();
      //  setTimeout(()=> this.GetUsers(), 100);

    setTimeout(()=> this.LoadTree(), 4000);

  }
  GetJobs()
  {
    let url="api/Jobs/GetJobsList";
    this.service.getAPI(url).then((resp:any[])=>{
this.Repository=resp;
    });
  }
  GetUsers()
  {
    let url=this.url+"api/Jobs/GetUsers";
//     this.service.getAPI(url).then((resp:any[])=>{
// this.Users=resp;
  
// });
var id:any=localStorage.getItem('jobgroupval');
let postData={
  editJobGroupName:id
}
this.http.get(url,{params:postData}).subscribe(resp=>{
  this.Users=resp;
});
  }
LoadTree()
{
  let url=this.url+"api/Jobs/GetJobGroupInfo"
  var id:any=localStorage.getItem('jobgroupval');
  let postData={
    jobGroupName:id
  }
  this.http.get(url,{params:postData}).subscribe(resp=>{
console.log(resp);
var response:any=resp;
    // this.user = resp.UsersList;
     this.JobGroupInfo = response.JobGroupInfo;
     EditJobGroupsComponent.AllJobGroups = JSON.parse(response.AllJobGroups);
    //  this.ENXJobGroup=EditJobGroupsComponent.AllJobGroups;
    //  var treedata_avm = JSON.parse("[" + response.JobGroupInfo + "]");
    var treedata_avm = JSON.parse(response.JobGroupInfo);

    this.JobGroupInfo = JSON.parse(response.JobGroupInfo);
    var jobListArray:any = [];

    $.each(EditJobGroupsComponent.AllJobGroups, function (index:any, item:any) {
      $.each(item.jobs, function (idx:any, jb:any) {
          jobListArray.push(jb);
      });
  });

  for (var i = 0; i < EditJobGroupsComponent.AllJobGroups.length; i++) {
    var newJobGroup:any = {};
    newJobGroup['Id'] = parseInt(EditJobGroupsComponent.AllJobGroups[i].JGID);
    newJobGroup['ParentId'] = EditJobGroupsComponent.AllJobGroups[i].ParentId;
    newJobGroup['Parent'] = null;
    newJobGroup['Name'] = EditJobGroupsComponent.AllJobGroups[i].name;
    newJobGroup['Description'] = EditJobGroupsComponent.AllJobGroups[i].description;
    newJobGroup['LevelDepth'] = EditJobGroupsComponent.AllJobGroups[i].level;
    newJobGroup['JobGroupId'] = EditJobGroupsComponent.AllJobGroups[i].JobGroupId;
    newJobGroup['Type'] = null;
    newJobGroup['JobGroups'] = null;
    newJobGroup['Jobs'] = null;
    newJobGroup['Users'] = null;
    newJobGroup['AllJobs'] = null;
    newJobGroup['JobGroupRefernce'] = null
    this.ENXJobGroup.push(newJobGroup);
}
     //jobs bind

     for (var i = 0; i < this.Repository.length; i++) {
      var jobDetails = this.Repository[i];
      if (this.isJobSelected(jobDetails, jobListArray)) {
          jobDetails.Selected1 = true;
      }
      else {
          jobDetails.Selected1 = false;
      }
      this.Repository[i] = jobDetails;
  }

  //User Bind

  for (var i = 0; i < this.Users.length; i++) {
    var userDetails = this.Users[i];
    if (this.isUserSelected(userDetails, response.UsersList)) {
        userDetails.Selectedass1 = true;
    }
    else {
        userDetails.Selectedass1 = false;
    }
    this.Users[i] = userDetails;
}

this.my_data=treedata_avm;
this.mytreedata=treedata_avm;
    $('#jobGroupsTreeDiv').jstree({         
      'core' : {
        "check_callback": true,
        'data' : treedata_avm
               }
   }).on('select_node.jstree', this.onNodeSelected)
    ; //add an event to the node selection (to get the data)
     //expand all the nodes when a new node is added

  });
   //add an event to the node selection (to get the data)


}
output:any;
// Jobgroupname:any
onNodeSelected(e:any,data:any)
  {
    // alert(data.node.id + " - " + JSON.stringify(data.node.original.additional_data));
    // this.dataStore.Jobgroup={Selected:data.node.original};
    localStorage.setItem("nodeid",data.node.id);

this.output=data.node.original.text;
this.Jobgroupname=data.node.original.text;

    for (var i = 0; i < EditJobGroupsComponent.AllJobGroups.length; i++) {
      if (EditJobGroupsComponent.AllJobGroups[i].name == data.node.original.text) {
          this.Jobgroupname = EditJobGroupsComponent.AllJobGroups[i].name;
          this.description = EditJobGroupsComponent.AllJobGroups[i].description;
          this.Idval = EditJobGroupsComponent.AllJobGroups[i].JGID;
          console.log(this.Jobgroupname);
      }
  }
  }
  onclick(va:any)
  {
alert(va);
  }
  jobgroupname:any;
  desc:any;
  double(e:any)
  {
var data= $('#jobGroupsTreeDiv').jstree().get_selected(true);
console.log(data);
this.jobgroupname = data[0].original.text;
                    this.desc = this.description;
                    for (var i in EditJobGroupsComponent.AllJobGroups) {
                        if (EditJobGroupsComponent.AllJobGroups[i].name == data[0].original.text) {
                            // this.jobgroupedit;
                            alert();
                            this.jobgroupeditdialog="block"

                        }
                    }
  }
  jobgroupedit()
  {
    this.jobgroupeditdialog="block"
  }
  closePopupRetrybatch()
  {
    this.jobgroupeditdialog="none"

  }
  Ok()
  {

  }
isJobSelected(jobDetail:any, jobNameList:any) {
  var isSelected = false;
  if (jobDetail && jobNameList && jobNameList.length > 0) {
      for (var i = 0; i < jobNameList.length; i++) {
          if (jobNameList[i] == jobDetail.JobName) {
              isSelected = true;
              break;
          }
      }
  }
  return isSelected;
}

isUserSelected = function (userDetails:any, userNameList:any) {
  var isSelected = false;
  if (userDetails && userNameList && userNameList.length > 0) {
      for (var i = 0; i < userNameList.length; i++) {
          if (userNameList[i] == userDetails.UserName) {
              isSelected = true;
              break;
          }
      }
  }
  return isSelected;
}



name:string="";
// jobgroupTree(node:any, editNodeId:any) {
//   if (node.JGID == editNodeId) {
//       //root node 
//       this.mytreedata[0].label = this.name;
//       this.updatetree(this.name, this.des, editNodeId);
//   }
//   else {
//       if (node.children && node.children.length > 0) {
//           var isfound = false;
//           $.each(node.children, function (index, item) {
//               if (item.type === 'jobgroup') {
//                   if (item.JGID == editNodeId) {
//                       $scope.updatetree($scope.name, $scope.des, editNodeId);
//                       item.label = $scope.name;
//                       isfound = true;
//                       $scope.children.push(item);
//                       return;
//                   }
//                   if (item.children)
//                       $scope.jobgroupTree(item, editNodeId);
//               }
//           });
//       }
//   }
// }


updatetree(nameval:any, description:any, editNodeId:any) {
  for (var i in this.ENXJobGroup) {
      if (this.output == this.ENXJobGroup[i].Name) {
          if (this.ENXJobGroup[i].Id == editNodeId)
          this.ENXJobGroup[i].Name = nameval;
          this.ENXJobGroup[i].Description = description;
      }
  }
}
groupname:any;
nodeLevel:any;

try_adding_a_branch()
{
  try {
//get the tree object reference
var obj_tree_ref = $("#jobGroupsTreeDiv").jstree(true);

//get the selected node
var obj_parent_node = $("#jobGroupsTreeDiv").jstree('get_selected');

//if no node is selected, set the value to null, which means we are adding a root node
if (obj_parent_node == null || obj_parent_node.length == 0)
    obj_parent_node = null; //to create a root node

    if (this.ENXJobGroup.length > 3) {
      this.notification.openSnackBar(this.errorCodes["WAR049"].msg,"","warning-snackbar");
return;
    }
    this.ENXJobGroup.sort(function (a:any, b:any) {
      return a.Id == b.Id ? 0 : +(a.Id > b.Id) || -1;
  });
  var GroupId = 0;
                
                GroupId=parseInt(this.ENXJobGroup.length);
                if ((this.Jobgroupname != undefined) && (this.Jobgroupname != "")) {
                  if (this.ENXJobGroup.length > 0) {
                    for (var k = 0; k < this.ENXJobGroup.length; k++) {
                        var groupName = this.ENXJobGroup[k].Name;
                        if (groupName == this.Jobgroupname) {
                            
                            this.notification.openSnackBar(this.errorCodes["WAR061"].msg,"","warning-snackbar");

                            return;
                        }
                    }
                }
                this.groupname = this.Jobgroupname;
                this.Jobgroupname = '';

                var newJobGroup:any = {};
                var node = $('#jobGroupsTreeDiv').jstree(true).get_node(localStorage.getItem('nodeid'));
                if(obj_parent_node==null)
                {
                  
                   this.nodeLevel=0;
                  newJobGroup['ParentId'] = '0';
  
                }
                else{
  
                    this.nodeLevel=parseInt(node.original.level)+1;
                   var id=localStorage.getItem('select');
                  newJobGroup['ParentId'] = node.original.JGID;
  
                }
                this.JGID = GroupId + 1;
                newJobGroup['Parent'] = null;
                newJobGroup['Name'] = this.groupname;
                newJobGroup['Description'] = this.description;
                newJobGroup['LevelDepth'] = this.nodeLevel;
                newJobGroup['Type'] = null;
                newJobGroup['JobGroups'] = null;
                newJobGroup['Jobs'] = null;
                newJobGroup['Users'] = null;
                newJobGroup['AllJobs'] = null;
                newJobGroup['JobGroupRefernce'] = null;
                if (node != null) {
                  if (node.original.NodeType == '2') {
                      
                      this.notification.openSnackBar(this.errorCodes["WAR050"].msg,"","warning-snackbar");

                      return
                  }
              }
//node json with additional information
var node_data = {
  "id": this.groupname,
  "text": this.groupname,
  "label": this.groupname,
  "NodeType": '1',
  "JGID": this.JGID,
  "Level": this.nodeLevel,
  "additional_data": { /*this additional_data will be added to the 'original' property of the node object */
      "job_level": 1,
      "jobs": [
          { "job_id": 1, "job_name": "New Job" },
          { "job_id": 2, "job_name": "New Job" }
      ],
      "users": [
          { "username": "Rajesh" },
          { "username": "Ramesh" }
      ]
  }
}
newJobGroup['Id'] = this.JGID;

              this.ENXJobGroup.push(newJobGroup);

              var new_node_id = obj_tree_ref.create_node(obj_parent_node, node_data, 'last', this.createNodeCallback, true);
              if (!new_node_id) {
                alert('Node creation failed'); //aler the user with the error message when creation fails
            }
            else{
                $("#jobGroupsTreeDiv").jstree('open_all'); //expand all the nodes when a new node is added
        
                }
            
                };
                this.description = '';


  }
  catch(e)
  {

  }
}
createNodeCallback()
{

}
Joblist = [];
 ENXJob:any = [];
 selectedaddjoblist:any=[];
 
try_adding_a_job()
{
  var obj_tree_ref = $("#jobGroupsTreeDiv").jstree(true);
  var obj_parent_node = $("#jobGroupsTreeDiv").jstree('get_selected');
  var node = $('#jobGroupsTreeDiv').jstree(true).get_node(localStorage.getItem('nodeid'));

  this.traverseTree(this.my_data);
  for (var i = 0; i < this.Repository.length; i++) {
    for (var j = 0; j < this.Repository.length; j++) {
      if (this.Repository[j].JobName == node.original.id) {
        return;
    }
    }
    if (node == null) {
      return;
  }
  var a=0;
  if (this.Repository[i].Selected) {
    this.selectedaddjoblist.push(this.Repository[i]);
    var enxJob:any = {};
    enxJob['ParentId'] = parseInt(node.original.JGID);
    enxJob['Name'] = this.Repository[i].JobName;
    enxJob['Description'] = "";
    enxJob['Id'] = "";
    enxJob['ButtonColor'] = "";
    enxJob['Profile'] = "";
    enxJob['MetadataName'] = "";
    enxJob['MetadataType'] = "";
    enxJob['MetadataEntry'] = "";
    enxJob['JobGroupReference'] = "";
    for (var k = 0; k < this.chkExistjOb.length; k++) {
        if (this.chkExistjOb[k] == this.Repository[i].JobName) {
            a = 1;
            break;
        }
    }
    if (a != 1) {
        this.ENXJob.push(enxJob);
        var JobName = this.Repository[i].JobName;
        var node_data = {
          "id": JobName,
          "text": JobName,
          "label": JobName,
          "NodeType": '2'
      }
      var new_node_id = obj_tree_ref.create_node(obj_parent_node, node_data, 'last', this.createNodeCallback, true);
  
      if (!new_node_id) {
        alert('Node creation failed'); //aler the user with the error message when creation fails
    }
    else
    {
        $("#jobGroupsTreeDiv").jstree('open_all'); //expand all the nodes when a new node is added

        }
    
    }
}


  }
}
chkExistjOb:any = [];

traverseTree(node:any)
{
  if (node.children && node.children.length > 0) {
    for (var i = 0; i < node.children.length; i++) {
        if (node.children[i].NodeType == "2")
            this.chkExistjOb.push(node.children[i].text);
        else {
            if (node.children[i].children)
                this.traverseTree(node.children[i]);
        }
    }
}
}
isAllSelected(val:any)
{
  this.Repository.every(function(item:any) {
    return item.Selected == true;
  });
}

isAllSelectedass(val:any)
 {
   this.Users.every(function(item:any) {
     return item.Selectedass == true;
   })
  }
 Userlist:any = [];

JobGroupUpdate()
{
  this.isDirty=false;
  this.getChildren(this.my_data);
  if (this.mytreedata.children.length == 0) {
    
    this.notification.openSnackBar(this.errorCodes["WAR051"].msg,"","warning-snackbar");

    return
}
if(this.Check!=1)
{
for (var i = 0; i < this.children.length; i++) {
  var enxJob:any = {};
  enxJob['ParentId'] = parseInt(this.children[i].parentId);
  enxJob['Name'] = this.children[i].text;
  enxJob['Description'] = "";
  enxJob['Id'] = "";
  enxJob['ButtonColor'] = "";
  enxJob['Profile'] = "";
  enxJob['MetadataName'] = "";
  enxJob['MetadataType'] = "";
  enxJob['MetadataEntry'] = "";
  enxJob['JobGroupReference'] = "";
  this.ENXJob.push(enxJob);
}
}

let userlist:any=[];
for (var i = 0; i < this.Users.length; i++) {
  if (this.Users[i].Selectedass) {
      var username = this.Users[i].UserName;
      this.Userlist.push(username);
       userlist = this.Userlist;
  }
}
let url="api/Jobs/UpdateJobGroup";
let postData = {
  JobGroups: this.ENXJobGroup,
  Jobs: this.ENXJob,
  Users: userlist
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
{
  if(resp==true)
{
  this.notification.openSnackBar(this.errorCodes["SUC012"].msg,"","success-snackbar");
  this.Joblist = [];
  this.Userlist = [];
  this.ngxLoader.stop();

  if (localStorage.getItem('printer') == "Printer") {
      localStorage.removeItem('printer');
      //$state.go('app.PrinterConnector');
      this.router.navigate(['/printerConnector']);
      this.ngxLoader.stop();
  }
  else {
    this.router.navigate(['/JobManagement']);
    this.ngxLoader.stop();
  }
  // unlockUI();
  this.ngxLoader.stop();
}
else{
  
this.notification.openSnackBar(this.errorCodes["WAR013"].msg,"","success-snackbar");

this.Joblist = [];
this.Userlist = [];
this.ngxLoader.stop();
}
this.ngxLoader.stop();
});
}
children:any = [];
ENXJobChk:any=[];
Check:number=0;
getChildren(jobData:any)
{
  $.each(jobData["children"],(index:any, item:any)=> {
    if (item.type === 'job')
        this.children.push(item);
    if (item.children)
        this.getChildren(item);
});
}
try_delete_a_job()
{
  this.getChildren(this.my_data);
  for(var i = 0; i<this.children.length; i++) {
    var enxJob:any = {};
    enxJob['ParentId'] = parseInt(this.children[i].parentId);
    enxJob['Name'] = this.children[i].text;
    enxJob['Description'] = "";
    enxJob['Id'] = "";
    enxJob['ButtonColor'] = "";
    enxJob['Profile'] = "";
    enxJob['MetadataName'] = "";
    enxJob['MetadataType'] = "";
    enxJob['MetadataEntry'] = "";
    enxJob['JobGroupReference'] = "";
    this.ENXJob.push(enxJob);
    var node = $('#jobGroupsTreeDiv').jstree(true).get_node(localStorage.getItem('nodeid'));
   

}
this.DeleteJob(node);
this.DeleteJobGroup(node);

this.children = [];
}
DeleteJob(node:any)
{
 for (var j = 0; j < this.ENXJob.length; j++) {
      if (this.ENXJob[j].Name == node.original.text) {
        this.ENXJob.splice(j, 1);
        $("#jobGroupsTreeDiv").jstree(true).delete_node(node);
    this.ENXJobChk=this.ENXJob;
        this.Check=1;
    

      }
  }
}

DeleteJobGroup(node:any)
{
  for (var k = 0; k < this.ENXJobGroup.length; k++) {
    if (this.ENXJobGroup[k].Name == node.original.text) {
      this.ENXJobGroup.splice(k, 1)
    }
}
}
Close()
{
  if (localStorage.getItem('printer') == "Printer") {
    localStorage.removeItem('printer');
    this.router.navigate(['/printerConnector']);
  }
else {
  this.router.navigate(['/JobManagement']);
}
}
}

