import { StoredataService } from 'src/app/storedata.service';
import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from 'src/app/_api/api.service';
import { NotificationService } from 'src/app/notification.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { FormControl, FormGroup } from '@angular/forms';
import { MatRadioChange } from '@angular/material/radio';
import { Router } from '@angular/router';
 import {MatDatepickerModule} from '@angular/material/datepicker';
 import {User} from 'src/app/model/user'
import { post } from 'jquery';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { SetPasswordComponent } from '../set-password/set-password.component';
import * as XLSX from 'xlsx';
import { NgxSpinnerService } from "ngx-spinner";  
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

 export interface Language{
  Id:number;
  LanguageCode:string;
  LanguageName:string;
  RoleName:string;
}
export interface Role{
  Id:number;
  RoleName:string;
  RoleCode:string;
}

@Component({
  selector: 'app-user-configure-summary',
  templateUrl: './user-configure-summary.component.html',
  styleUrls: ['./user-configure-summary.component.scss']
})
export class UserConfigureSummaryComponent implements OnInit {
  languages:Language[]=[];
  roles:Role[]=[];
  Language:any;
  Role:any;

getUserconfigdtl:any
User: User[] = [];
AuthName:string="";
adminEmailBody:string="";
adminSubject:string="";
adminemail:string="";
checkadminemail:any;
adminmailform:boolean=false;
userconfig:boolean=true;
LaserFicheuserdetail:boolean=false;
alternateusername:string="";
Adserver:string="";
Adfilter:string="";
IncludeGroups:any;
AdGroupfilter:any;
showColumn:boolean = true
Active:any;

Languageshow:boolean=false;


egnytelogin:string="";
sharepointlogin:string="";
onedrivelogin:string="";
dropboxlogin:string="";
boxlogin:string="";
awssecret_key:any;
awsaccess_key:any;
lastname:string="";
firstname:string="";
homefolder:string="";
nfccardid:string="";
uniqueid:string="";
email:string="";
username:string="";
mandatory:any;
Showmandatory:any;
BtnADSearch:any;
Enabledelete:any;
cancelforuser:any
DocuwareCheck:any;
AWS:any;
load:any;
create:any;
ShowLFOnly:any;
Egnyte:any;
Spo:any;
Odb:any;
DropBox:any;
Box:any;
Awskey:any;
aAwskey:any;
AdShowitemscopykey:any;
AdShowchangepwd:any;
userchangepasswordshow:any;
userpasswordset:any;
VAwskey:any;
Otherpasswordset:any;
userdetail:any;
userform:any;
Nfcnorml:any;
Nfcmap:any;
import:any;
foruser:any;
LFuserconfig:any;
export:any;
authType:any;
secondaryauthType:any;
changepasswordshow:boolean = false;
emailhide:boolean = false;
homefolderhide:boolean = false;
fnamehide:boolean = false;
lnamehide:boolean = false;
nfcmapcardhide:boolean = false;
userId:any;
provider:any;
roleName:any;
MFilesCheck:any;
errorCodes:any = {};
 display1 = "none";
hide: boolean = true;
hideNew: boolean = true;
passwordset:string="";
confirmpassword:string="";
verifypassword:string="";
dataSource: MatTableDataSource<User>;
unmap:boolean=false;
displayStyle="none";
displayVerifyLogin="none";
displayEditMapNFC="none";
displayDeleteuser="none"
unique_id:any;
Mapcardno:any;
btnmapcard:any = false;
btnassign:any;
btngetcard:any;
notifyalert:any;
Userlist:any;
displaycurrentId="none";
displayUnMapNFC="none";
displayAssignAWSKey="none";
IsAllChecked:any;
Selected:any;
checkedList:any;
displayNFCMappingMode="none";
scannershow:any;
ScannerList:any;
scannername:any;
btnOk:boolean = true;
nfcmultipleuser:boolean=false;
url:any;
apiurl:any
ListOfUsers:any = [];
Delname:any;
sync:boolean=false;
@ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
@ViewChildren(MatSort) sort = new QueryList<MatSort>();
displayedColumns: string[] = ['select','Username','Email','FirstName', 'LastName','HomeFolder','UniqueId','NFCCardId','Action'];

  constructor(private http: HttpClient,public translate: TranslateService,private SpinnerService:NgxSpinnerService,private dataStore:StoredataService,private ngxLoader:NgxUiLoaderService,private service:ApiService,private router:Router,private notification:NotificationService,
    private ErrorCodes:ErrorCodesService,public dialog: MatDialog) { 
    this.dataSource = new MatTableDataSource<User>();

  }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');
    this.translate.use(returnUrl);
    this.errorCodes=this.ErrorCodes.errormsgs();
    this.apiurl = environment.hostUrl;
  
    
this.getUserconfigdtl=this.dataStore.userConfigure;

if(this.getUserconfigdtl!=undefined)
{
  this.AuthName=this.getUserconfigdtl.Authentication;
  this.Adserver=this.getUserconfigdtl.Adserver;
  this.Adfilter=this.getUserconfigdtl.Adfilter;
  this.AdGroupfilter=this.getUserconfigdtl.AdGroupfilter;
  
  if (this.AuthName == "ActiveDirectory") {
    localStorage.setItem('Active', this.AuthName);
  }
  if (this.AuthName == "LocalDirectory") {
    localStorage.setItem('Local', this.AuthName);
  }
  if (this.AuthName == "Docuware") {
    localStorage.setItem('Docuware', this.AuthName);
  }
  if (this.AuthName == "LaserFiche") {
    localStorage.setItem('LaserFiche', this.AuthName);
  }
  if (this.AuthName == "CustomTarget") {
    localStorage.setItem('CustomTarget', this.AuthName);
  }
  
  if (localStorage.getItem('AuthType') == "LocalDirectory") {
    this.import = false;
    this.export = true;
}
}
this.Active = localStorage.getItem('Active');

            this.userconfig = true;
            this.changepasswordshow = false;
            this.mandatory = true;
            this.adminmailform = false;
            this.Showmandatory = true;
            if (localStorage.getItem('AuthType') == "LocalDirectory") {
                this.Showmandatory = true;
            }
            else {
                this.Showmandatory = false;
            }

            if (localStorage.getItem('AuthType') == "ActiveDirectory") {
              this.BtnADSearch = true;
              this.Enabledelete = false;
              if ((localStorage.getItem('Role') == "SuperAdmin") || (localStorage.getItem('Role') == "Administrator")) {
                  this.adminmailform = true;
              }
          }
          if (localStorage.getItem('AuthType') == "LocalDirectory") {
              this.changepasswordshow = true;
              this.Enabledelete = true;
          }
          if ((localStorage.getItem('Role') == "Administrator") || (localStorage.getItem('Role') == "SuperAdmin")) {
            this.cancelforuser = true;
        }
        if (localStorage.getItem('AuthType') == "Docuware") {
            this.DocuwareCheck = true;
            this.AWS = false;
            this.mandatory = false;
            this.Enabledelete = false;
            if ((localStorage.getItem('Role') == "Administrator") || (localStorage.getItem('Role') == "SuperAdmin")) {
                this.cancelforuser = true;
            }
            else {
                this.cancelforuser = false;
            }
        }
        if (localStorage.getItem('AuthType') == "LaserFiche") {
          this.Enabledelete = true;
          this.load = false;
          this.create = false;
          this.ShowLFOnly = true;
      }

      //Admin mail for Active directory
      if (localStorage.getItem('AuthType') == "ActiveDirectory") {
            
        let url="api/User/LoadAdminEmailSetting";
        let postData={
          Adminemailcheck: this.checkadminemail,
        }
        this.service.post(url,postData).subscribe(resp=>{
if(resp!=null)
{
  this.checkadminemail = resp.Adminemailcheck;
                    this.adminemail = resp.AdminEmail;
                    this.adminSubject = resp.AdminSubject;
                    this.adminEmailBody = resp.AdminBody;
   
}
        },(error)=>
        {

        })
      }

      
      if (localStorage.getItem('AuthType') == "MFiles") {
        localStorage.removeItem('Active');
        localStorage.removeItem('Local');
        localStorage.removeItem('secondaryAuthType');
        this.MFilesCheck = true;
        this.AWS = false;
        this.Enabledelete = false;
        this.mandatory = false;
        if ((localStorage.getItem('Role') == "Administrator") || (localStorage.getItem('Role') == "SuperAdmin")) {
            this.cancelforuser = true;
        }
        else {
            this.cancelforuser = false;
        }
    }


    if ((localStorage.getItem('AuthType') == "CustomTarget") || (localStorage.getItem('CustomProductType') == "CUSTOM")) {
      localStorage.removeItem('Active');
      localStorage.removeItem('Local');
      localStorage.removeItem('secondaryAuthType');
      this.AWS = false;
      this.Enabledelete = false;
      this.mandatory = false;
      if ((localStorage.getItem('Role') == "Administrator") || (localStorage.getItem('Role') == "SuperAdmin")) {
          this.cancelforuser = true;
      }
      else {
          this.cancelforuser = false;
      }
  }


  if (localStorage.getItem('AuthType') == "ActiveDirectory") {
    this.showColumn = true;
    this.load = true;
    this.create = false;
    this.Nfcmap = true;
}
else if (localStorage.getItem('AuthType') == "LocalDirectory") {
    this.showColumn = true;
    this.load = false;
    this.create = true;
    this.Nfcmap = true;
}
else if (localStorage.getItem('AuthType') == "LaserFiche") {
    this.showColumn = false;
    this.load = false;
    this.create = false;
    this.Nfcmap = true;
}
else if (localStorage.getItem('AuthType') == "Docuware") {
    this.showColumn = true;
    this.load = true;
    this.create = false;
    this.Nfcmap = true;
    localStorage.removeItem('secondaryAuthType');
}
else if (localStorage.getItem("secondaryAuthType") == "Nintex") {
    this.load = false;
    this.create = true;
}
else if (localStorage.getItem("secondaryAuthType") == "UIPath") {
    this.load = false;
    this.create = true;
}
else if (localStorage.getItem("secondaryAuthType") == "Egnyte") {
    this.load = false;
    this.create = true;
}
if (localStorage.getItem('AuthType') == "MFiles") {
    this.showColumn = true;
    this.load = true;
    this.create = false;
    this.Nfcmap = true;
}

// Custom target settings for user configuration
// if (localStorage.getItem('CustomProductType') == "CUSTOM") {
//   this.showawskey = false;
//   this.showlang = false;
//   this.showemail = false;
//   this.showhomefolder = false;
// }

this.getLanguages();
this.getRoles();
this.LoadUser();
if (localStorage.getItem('Role') == "Users") {
this.LoadUserroledtl();
}
  }

  getRoles()
  {
    let url = "api/User/GetAllRoles?PreferredLanguage=" + localStorage.getItem('PreferredLanguage');
    this.service.getAPI(url).then(resp => {
      // this.Languageshow = ! this.Languageshow;
      this.roles = resp;
      // this.ngxLoader.stop();
    },(error)=>{
      // this.notification.openSnackBar(error.message,"","red-snackbar");
      // this.ngxLoader.stop();
    });
  }
  getLanguages()
  {
    let url = "api/User/GetLanguages?id=" + "";
    this.service.getAPI(url).then(resp => {
       this.Languageshow = ! this.Languageshow;
      this.languages = resp;
      // this.ngxLoader.stop();
    },(error)=>{
      // this.notification.openSnackBar(error.message,"","red-snackbar");
      // this.ngxLoader.stop();
    });

  }
LoadUser()
{
  let url="api/User/ConfigureUser";
  let postData={
    Authentication: this.AuthName,
    Adserver: this.Adserver,
    Adfilter: this.Adfilter,
    IncludeGroups: this.IncludeGroups,
    AdGroupfilter: this.AdGroupfilter
  }
  this.service.post(url,postData).subscribe(resp=>{
console.log(resp);
if(resp!=null)
{
  this.dataSource = new MatTableDataSource(resp);
  this.dataSource.paginator = this.paginator.toArray()[0];
  this.dataSource.sort = this.sort.toArray()[0];
}
  },(error)=>
  {

  });
}
CreateUser()
{
  this.router.navigate(['/createuser']);
}

 findItem(currentValue:any, index:any, arrObj:any) {
  var providerCode = this;
  return currentValue.ProviderCode == providerCode
}


LoadUserroledtl()
{
  this.userdetail = true;
                // this.createuserform = false;
                this.userconfig = false;
                this.LFuserconfig = false;
                this.adminmailform = false;
                this.userform = true;
                this.foruser = true;
                this.cancelforuser = false;
                this.AWS = false;
                this.changepasswordshow = false;
                this.Box = false;
                this.DropBox = false;
                this.Odb = false;
                this.Spo = false;
                this.Egnyte = false;

                let url="api/User/EditUserDetails";
                let postData={
                  UserId: localStorage.getItem('Id')
                }
                this.service.post(url,postData).subscribe(resp=>{
                  console.log(resp);
if(resp!=null)
{
  this.authType = resp.AuthType;
                    this.secondaryauthType = resp.Secondary;
                    this.changepasswordshow = false;
                    if ((this.authType == 'ActiveDirectory') || (this.authType == 'Docuware')) {
                      this.emailhide = true;
                      this.homefolderhide = true;
                      this.fnamehide = true;
                      this.lnamehide = true;
                  }
                  else if (this.authType == 'MFiles') {
                      this.emailhide = true;
                      this.homefolderhide = false;
                      this.fnamehide = true;
                      this.lnamehide = true;
                  }
                  else if (this.authType == 'LocalDirectory') {
                      this.changepasswordshow = true;
                      this.emailhide = false;
                      this.homefolderhide = false;
                      this.fnamehide = false;
                      this.lnamehide = false;
                  }
                  else if (this.authType == 'LaserFiche') {
                      this.LaserFicheuserdetail = true;
                      this.userdetail = false;
                  }
                  this.awsaccess_key = "";
                  this.awssecret_key = "";
                  if (resp && resp.UserList) {
                      this.AWS = true;
                      if (localStorage.getItem('AuthType') == "Docuware") {
                          this.AWS = false;
                      }
                      if (localStorage.getItem('AuthType') == "MFiles") {
                          this.AWS = false;
                      }
                      if (localStorage.getItem('secondaryAuthType') == "Egnyte") {
                          this.AWS = true;
                      }
                      if (localStorage.getItem('secondaryAuthType') == "Nintex") {
                          this.AWS = false;
                      }
                      if (localStorage.getItem('secondaryAuthType') == "UIPath") {
                          this.AWS = false;
                      }
                      this.awsaccess_key = null;
                      this.awssecret_key = null;

                      if (resp.UserList.ProviderDetails && resp.UserList.ProviderDetails.length > 0) {
                        var providerDetails = resp.UserList.ProviderDetails;

                        var awsDetails = providerDetails.filter(this.findItem, "AWSS3");
                        var boxDetails = providerDetails.filter(this.findItem, "BOX");
                        var odbDetails = providerDetails.filter(this.findItem, "ODB");
                        var spoDetails = providerDetails.filter(this.findItem, "SPO");
                        var egnyteDetails = providerDetails.filter(this.findItem, "EGNYTE");
                        var dropboxDetails = providerDetails.filter(this.findItem, "DROPBOX");
                        switch (localStorage.getItem('secondaryAuthType')) {
                          case 'Egnyte':
                              if (egnyteDetails != null && egnyteDetails.length > 0) {
                                  this.AWS = true;
                                  if (egnyteDetails[0].AccessToken != null && egnyteDetails[0].RefreshToken != null) {
                                      this.Egnyte = true;
                                      this.egnytelogin = egnyteDetails[0].LoginName;
                                  }
                              }
                              if (awsDetails != null && awsDetails.length > 0) {
                                  this.AWS = true;
                                  this.awsaccess_key = awsDetails[0].AccessToken;
                                  this.awssecret_key = awsDetails[0].RefreshToken;
                              }

                              if (boxDetails != null && boxDetails.length > 0) {
                                  if (boxDetails[0].AccessToken != null && boxDetails[0].RefreshToken != null) {
                                      this.Box = true;
                                      this.boxlogin = boxDetails[0].LoginName;
                                  }
                              }
                              if (dropboxDetails != null && dropboxDetails.length > 0) {
                                  if (dropboxDetails[0].AccessToken != null && dropboxDetails[0].RefreshToken != null) {
                                      this.DropBox = true;
                                      this.dropboxlogin = dropboxDetails[0].LoginName;
                                  }
                              }
                              if (odbDetails != null && odbDetails.length > 0) {
                                  if (odbDetails[0].AccessToken != null && odbDetails[0].RefreshToken != null) {
                                      this.Odb = true;
                                      this.onedrivelogin = odbDetails[0].LoginName;
                                  }
                              }

                              if (spoDetails != null && spoDetails.length > 0) {
                                  if (spoDetails[0].AccessToken != null && spoDetails[0].RefreshToken != null) {
                                      this.Spo = true;
                                      this.sharepointlogin = spoDetails[0].LoginName;
                                  }
                              }
                              break;
                          case 'UIPath':
                              if (spoDetails != null && spoDetails.length > 0) {
                                  if (spoDetails[0].AccessToken != null && spoDetails[0].RefreshToken != null) {
                                      this.Spo = true;
                                      this.sharepointlogin = spoDetails[0].LoginName;
                                  }
                              }
                              break;
                          case 'Nintex':
                              this.AWS = false;
                              this.Box = false;
                              this.Odb = false;
                              this.Spo = false;
                              this.Egnyte = false;
                              this.DropBox = false;

                              break;
                          default:
                              if (this.authType == 'MFiles') {
                                  this.AWS = false;
                                  this.Box = false;
                                  this.Odb = false;
                                  this.Spo = false;
                                  this.Egnyte = false;
                                  this.DropBox = false;

                              }
                              else if (this.authType == 'Docuware') {
                                  this.AWS = false;
                                  this.Box = false;
                                  this.Odb = false;
                                  this.Spo = false;
                                  this.Egnyte = false;
                                  this.DropBox = false;

                              }
                              else {
                                  if (awsDetails != null && awsDetails.length > 0) {
                                      this.AWS = true;
                                      this.awsaccess_key = awsDetails[0].AccessToken;
                                      this.awssecret_key = awsDetails[0].RefreshToken;
                                  }

                                  if (boxDetails != null && boxDetails.length > 0) {
                                      if (boxDetails[0].AccessToken != null && boxDetails[0].RefreshToken != null) {
                                          this.Box = true;
                                          this.boxlogin = boxDetails[0].LoginName;
                                      }
                                  }
                                  if (dropboxDetails != null && dropboxDetails.length > 0) {
                                      if (dropboxDetails[0].AccessToken != null && dropboxDetails[0].RefreshToken != null) {
                                          this.DropBox = true;
                                          this.dropboxlogin = dropboxDetails[0].LoginName;
                                      }
                                  }
                                  if (odbDetails != null && odbDetails.length > 0) {
                                      if (odbDetails[0].AccessToken != null && odbDetails[0].RefreshToken != null) {
                                          this.Odb = true;
                                          this.onedrivelogin = odbDetails[0].LoginName;
                                      }
                                  }

                                  if (spoDetails != null && spoDetails.length > 0) {
                                      if (spoDetails[0].AccessToken != null && spoDetails[0].RefreshToken != null) {
                                          this.Spo = true;
                                          this.sharepointlogin = spoDetails[0].LoginName;
                                      }
                                  }
                                  if (egnyteDetails != null && egnyteDetails.length > 0) {
                                      this.AWS = true;
                                      if (egnyteDetails[0].AccessToken != null && egnyteDetails[0].RefreshToken != null) {
                                          this.Egnyte = true;
                                          this.egnytelogin = egnyteDetails[0].LoginName;
                                      }
                                  }
                              }

                              break;
                      }
//switch case end

this.username = resp.UserList.Username;
this.email = resp.UserList.Email;
this.homefolder = resp.UserList.HomeFolder;
this.nfccardid = resp.UserList.NFCCardId;
this.uniqueid = resp.UserList.UniqueId;
this.firstname = resp.UserList.FirstName;
this.lastname = resp.UserList.LastName;
this.Role = resp.UserList.RoleCode;
this.Language = resp.UserList.PreferredLanguage;
this.alternateusername = resp.UserList.AlternateUsername;
if (resp.UserList.NFCCardId == "" || resp.UserList.NFCCardId == null) {
    this.nfcmapcardhide = false;
}
else {
    this.nfcmapcardhide = true;
}
if (this.nfccardid == null || this.nfccardid == "") {
    this.unmap = true;
}
else {
    this.unmap = false;
}
this.Awskey = false;
this.changepasswordshow = false;
if ((this.Role == "SuperAdmin" || this.Role == "Administrator") && (localStorage.getItem('AuthType') == "LocalDirectory" || localStorage.getItem('AuthType') == "ActiveDirectory")) {// && (localStorage.getItem('secondaryAuthType') != "Egnyte")) {
  this.Awskey = true;
  this.changepasswordshow = true;
  if (localStorage.getItem('AuthType') == "ActiveDirectory") {
      this.AdShowchangepwd = false;
      this.Awskey = false;
      this.changepasswordshow = false;
      this.AdShowitemscopykey = true;
      this.aAwskey = true;
      this.changepasswordshow = false;
  }
}
if ((this.Role == "Users") && (localStorage.getItem('AuthType') == "LocalDirectory")) {
  this.Otherpasswordset = false;
  this.VAwskey = false;
  this.userpasswordset = true;
  this.userchangepasswordshow = true;
  this.AdShowitemscopykey = true;
  //this.aAwskey = true;

}
if ((this.Role == "Users") && ((localStorage.getItem('AuthType') == "Docuware") || (localStorage.getItem('AuthType') == "MFiles"))) {
  this.AdShowchangepwd = true;
  this.changepasswordshow = false;
  this.AdShowitemscopykey = false;
  this.Otherpasswordset = true;
  this.VAwskey = false;
}
if ((this.Role == "Users") && (localStorage.getItem('AuthType') == "ActiveDirectory")) {
  this.Otherpasswordset = true;
  this.VAwskey = false;
  this.userpasswordset = false;
  this.userchangepasswordshow = false;
  this.AdShowchangepwd = true;
  this.changepasswordshow = false;
  this.AdShowitemscopykey = false;
  this.aAwskey = false;
  this.Awskey = false;
}
  }
}
}
},(error)=>
  {

  });
}
SyncAduser()
{
    this.Active = localStorage.getItem('Active');
    this.sync = true;
    let url="api/User/ConfigureUser";

    let postData={
        Sync: this.sync,
        Authentication: this.AuthName,
        Adserver: this.Adserver,
        Adfilter: this.Adfilter,
        IncludeGroups: this.IncludeGroups,
        AdGroupfilter: this.AdGroupfilter

      }
      this.service.post(url,postData).subscribe(resp=>{
        if(resp!=null)
        {
          this.dataSource = new MatTableDataSource(resp);
          this.dataSource.paginator = this.paginator.toArray()[0];
          this.dataSource.sort = this.sort.toArray()[0];
        }
          },(error)=>
          {
        
          });
}
Edituser(Id:any)
{
  this.userId = Id;
  this.userdetail = true;
  this.userform = true;
  // this.createuserform = false;
  this.provider = false;
  this.userconfig = false;
  this.LFuserconfig = false;
  this.adminmailform = false;
  this.AWS = false;
  this.Box = false;
  this.DropBox = false;
  this.Odb = false;
  this.Spo = false;
  
  let url="api/User/EditUserDetails";
  let postData={
    UserId: Id
  }
  this.service.post(url,postData).subscribe(resp=>
    {
      console.log(resp);
      if(resp!=null)
      {
        this.authType = resp.AuthType;
        this.secondaryauthType = resp.Secondary;
        this.roleName = localStorage.getItem('Role');
        this.GetLang(resp.UserList.Username);

        if ((this.authType == 'ActiveDirectory') || (this.authType == 'Docuware')) {
          this.emailhide = true;
          this.homefolderhide = true;
          this.fnamehide = true;
          this.lnamehide = true;
      }
      else if (this.authType == 'MFiles') {
          this.fnamehide = true;
          this.lnamehide = true;
          this.emailhide = true;
      }
      else if (this.authType == 'LocalDirectory') {
          //this.changepasswordshow = true;
          if (this.roleName == 'Users') {
              this.emailhide = false;
              this.homefolderhide = false;
              this.fnamehide = false;
              this.lnamehide = false;

          }
      }
      else if (this.authType == 'LaserFiche') {
          this.LaserFicheuserdetail = true;
          this.userdetail = false;
      }
      this.awsaccess_key = "";
      this.awssecret_key = "";
      if (resp && resp.UserList) {

        this.AWS = true;
        if (localStorage.getItem('AuthType') == "Docuware") {
            this.AWS = false;
        }
        if (localStorage.getItem('AuthType') == "MFiles") {
            this.AWS = false;
        }
        if (localStorage.getItem('secondaryAuthType') == "Egnyte") {
            this.AWS = true;
        }
        if (localStorage.getItem('secondaryAuthType') == "Nintex") {
            this.AWS = false;
        }
        if (localStorage.getItem('secondaryAuthType') == "UIPath") {
            this.AWS = false;
        }
        if (localStorage.getItem('AuthType') == "LaserFiche") {
            this.AWS = false;
            this.changepasswordshow = true;
            this.unmap = true;
            //this.fnamehide = false;
            //this.lnamehide = false;
        }
        this.awsaccess_key = null;
        this.awssecret_key = null;


        if (resp.UserList.ProviderDetails && resp.UserList.ProviderDetails.length > 0) {
            var providerDetails = resp.UserList.ProviderDetails;

            var awsDetails = providerDetails.filter(this.findItem, "AWSS3");
            var boxDetails = providerDetails.filter(this.findItem, "BOX");
            var odbDetails = providerDetails.filter(this.findItem, "ODB");
            var spoDetails = providerDetails.filter(this.findItem, "SPO");
            var egnyteDetails = providerDetails.filter(this.findItem, "EGNYTE");
            var dropboxDetails = providerDetails.filter(this.findItem, "DROPBOX");


            switch (localStorage.getItem('secondaryAuthType')) {
                case 'Egnyte':
                    if (egnyteDetails != null && egnyteDetails.length > 0) {
                        this.AWS = true;
                        if (egnyteDetails[0].AccessToken != null && egnyteDetails[0].RefreshToken != null) {
                            this.Egnyte = true;
                            this.egnytelogin = egnyteDetails[0].LoginName;
                        }
                    }
                    if (awsDetails != null && awsDetails.length > 0) {
                        this.AWS = true;
                        this.awsaccess_key = awsDetails[0].AccessToken;
                        this.awssecret_key = awsDetails[0].RefreshToken;
                    }

                    if (boxDetails != null && boxDetails.length > 0) {
                        if (boxDetails[0].AccessToken != null && boxDetails[0].RefreshToken != null) {
                            this.Box = true;
                            this.boxlogin = boxDetails[0].LoginName;
                        }
                    }
                    if (dropboxDetails != null && dropboxDetails.length > 0) {
                        if (dropboxDetails[0].AccessToken != null && dropboxDetails[0].RefreshToken != null) {
                            this.DropBox = true;
                            this.dropboxlogin = dropboxDetails[0].LoginName;
                        }
                    }
                    if (odbDetails != null && odbDetails.length > 0) {
                        if (odbDetails[0].AccessToken != null && odbDetails[0].RefreshToken != null) {
                            this.Odb = true;
                            this.onedrivelogin = odbDetails[0].LoginName;
                        }
                    }

                    if (spoDetails != null && spoDetails.length > 0) {
                        if (spoDetails[0].AccessToken != null && spoDetails[0].RefreshToken != null) {
                            this.Spo = true;
                            this.sharepointlogin = spoDetails[0].LoginName;
                        }
                    }
                    break;
                case 'UIPath':
                    if (spoDetails != null && spoDetails.length > 0) {
                        if (spoDetails[0].AccessToken != null && spoDetails[0].RefreshToken != null) {
                            this.Spo = true;
                            this.sharepointlogin = spoDetails[0].LoginName;
                        }
                    }
                    break;
                case 'Nintex':
                    this.AWS = false;
                    this.Box = false;
                    this.Odb = false;
                    this.Spo = false;
                    this.Egnyte = false;
                    this.DropBox = false;

                    break;
                default:
                    if (this.authType == 'MFiles') {
                        this.AWS = false;
                        this.Box = false;
                        this.Odb = false;
                        this.Spo = false;
                        this.Egnyte = false;
                        this.DropBox = false;
                    }
                    else if (this.authType == 'Docuware') {
                        this.AWS = false;
                        this.Box = false;
                        this.Odb = false;
                        this.Spo = false;
                        this.Egnyte = false;
                        this.DropBox = false;
                    }
                    else {
                        if (awsDetails != null && awsDetails.length > 0) {
                            this.AWS = true;
                            this.awsaccess_key = awsDetails[0].AccessToken;
                            this.awssecret_key = awsDetails[0].RefreshToken;
                        }

                        if (boxDetails != null && boxDetails.length > 0) {
                            if (boxDetails[0].AccessToken != null && boxDetails[0].RefreshToken != null) {
                                this.Box = true;
                                this.boxlogin = boxDetails[0].LoginName;
                            }
                        }
                        if (dropboxDetails != null && dropboxDetails.length > 0) {
                            if (dropboxDetails[0].AccessToken != null && dropboxDetails[0].RefreshToken != null) {
                                this.DropBox = true;
                                this.dropboxlogin = dropboxDetails[0].LoginName;
                            }
                        }
                        if (odbDetails != null && odbDetails.length > 0) {
                            if (odbDetails[0].AccessToken != null && odbDetails[0].RefreshToken != null) {
                                this.Odb = true;
                                this.onedrivelogin = odbDetails[0].LoginName;
                            }
                        }

                        if (spoDetails != null && spoDetails.length > 0) {
                            if (spoDetails[0].AccessToken != null && spoDetails[0].RefreshToken != null) {
                                this.Spo = true;
                                this.sharepointlogin = spoDetails[0].LoginName;
                            }
                        }
                        if (egnyteDetails != null && egnyteDetails.length > 0) {
                            this.AWS = true;
                            if (egnyteDetails[0].AccessToken != null && egnyteDetails[0].RefreshToken != null) {
                                this.Egnyte = true;
                                this.egnytelogin = egnyteDetails[0].LoginName;
                            }
                        }
                    }

                    break;
            }
        }

        this.username = resp.UserList.Username;
        this.email = resp.UserList.Email;
        this.homefolder = resp.UserList.HomeFolder;
        this.nfccardid = resp.UserList.NFCCardId;
        this.uniqueid = resp.UserList.UniqueId;
        this.firstname = resp.UserList.FirstName;
        this.lastname = resp.UserList.LastName;
        this.Role = resp.UserList.RoleCode;
        this.Language = resp.UserList.PreferredLanguage;
        this.alternateusername = resp.UserList.AlternateUsername;

        if (resp.UserList.NFCCardId == "" || resp.UserList.NFCCardId == null) {
            this.nfcmapcardhide = false;
        }
        else {
            this.nfcmapcardhide = true;
        }
        if (this.nfccardid == null || this.nfccardid == "") {
            this.unmap = true;
        }
        else {
            this.unmap = false;
        }
        this.Awskey = false;
        this.changepasswordshow = false;
        if ((this.Role == "SuperAdmin" || this.Role == "Administrator") && (localStorage.getItem('AuthType') == "LocalDirectory" || localStorage.getItem('AuthType') == "ActiveDirectory" || localStorage.getItem('AuthType') == "Docuware" || this.authType == 'MFiles')) {// && (localStorage.getItem('secondaryAuthType') != "Egnyte")) {
            if (localStorage.getItem('AuthType') == "Docuware" || this.authType == 'MFiles' || localStorage.getItem('AuthType') == "ActiveDirectory") {
                this.Otherpasswordset = true;
                this.VAwskey = true;
                this.userpasswordset = false;
                this.userchangepasswordshow = false;
                this.changepasswordshow = false;
                this.AdShowchangepwd = true;
                this.AdShowitemscopykey = false;
                if (localStorage.getItem('AuthType') == "ActiveDirectory") {
                    this.AdShowchangepwd = false;
                    this.Awskey = false;
                    this.changepasswordshow = false;
                    this.AdShowitemscopykey = true;
                    this.aAwskey = true;
                }
            }
            else {
                this.userpasswordset = false;
                this.userchangepasswordshow = false;
                this.Otherpasswordset = true;
                this.AdShowchangepwd = true;
                this.AdShowitemscopykey = false;
                this.Awskey = true;
                this.changepasswordshow = true;
                this.VAwskey = true;
            }
        }
        if (this.Role == "Users" || this.Role == "RestrictedUser") {

            if (localStorage.getItem('AuthType') == "LocalDirectory") {
                this.Otherpasswordset = true;
                this.VAwskey = true;
                this.userpasswordset = false;
                this.changepasswordshow = true;
                this.AdShowchangepwd = true;
                this.AdShowitemscopykey = false;
                this.Awskey = false;
            }
            else if ((localStorage.getItem('AuthType') == "Docuware" || localStorage.getItem('AuthType') == 'MFiles') || localStorage.getItem('AuthType') == "ActiveDirectory") {
                //else if ((localStorage.getItem('AuthType') == "Docuware" || localStorage.getItem('AuthType') == 'MFiles')) {
                this.Otherpasswordset = true;
                this.VAwskey = true;
                this.userpasswordset = false;
                this.userchangepasswordshow = false;
                this.changepasswordshow = false;
                this.AdShowchangepwd = true;
                this.AdShowitemscopykey = false;
                if (localStorage.getItem('AuthType') == "ActiveDirectory") {
                    this.AdShowchangepwd = false;
                    this.changepasswordshow = false;
                    this.AdShowitemscopykey = true;
                    this.aAwskey = false;
                    this.Awskey = false;
                }
            }
        }
    }



      }
    },(error)=>
    {

    })
}
GetLang(username:any)
{
let url="api/User/GetLanguageCode";
let postData={
  Username: username
}
this.service.post(url,postData).subscribe(resp=>
  {
    this.Language = resp[0].LanguageCode;

  },(error)=>
  {

  })
}
Back() {
  this.userconfig = true;
  this.adminmailform = false;
  this.userdetail = false;
  // this.createuserform = false;
  this.LaserFicheuserdetail = false;
  window.location.reload();
}
Update()
{
  if ((localStorage.getItem('AuthType') != "Docuware") && (localStorage.getItem('AuthType') != "MFiles") && (localStorage.getItem('AuthType') != "LaserFiche") && (localStorage.getItem('AuthType') != "CustomTarget") && (localStorage.getItem('AuthType') != "ActiveDirectory")) {
    if ((this.firstname == null || this.username == null || this.uniqueid == null || this.email == null) ||
        (this.firstname == "" || this.username == "" || this.uniqueid == "" || this.email == "")) {
        this.notification.openSnackBar(this.errorCodes["WAR035"].msg,"","warning-snackbar");
        return;
    }
}
  let url="api/User/UpdateUser";
  let postData={
    Username: this.username,
    Email: this.email,
    HomeFolder: this.homefolder,
    NFCCardId: this.nfccardid,
    FirstName: this.firstname,
    LastName: this.lastname,
    UniqueId: this.uniqueid,
    Password: "",
    AWSS3AccessID: this.awsaccess_key,
    AWSS3SecretKey: this.awssecret_key,
    Role: this.Role,
    PreferredLanguage: this.Language
  }
  this.service.post(url,postData).subscribe(resp=>
    {
      if (resp == true) {
        // this.userconfiguresummaryForm.$dirty = false;
        if ((this.nfccardid != "") && (this.nfccardid != null)) {
            this.nfcmapcardhide = true;
            this.unmap = false;
        }
        this.notification.openSnackBar(this.errorCodes["SUC004"].msg,"","success-snackbar");

        if (localStorage.getItem('Role') == "Users") {
            // $state.reload();
            this.LoadUserroledtl();
        }
        // this.recoverForm.$setPristine();
    }
    else if (resp == "Id is Already Assigned.") {
        this.nfccardid = '';
        
        this.notification.openSnackBar(this.errorCodes["WAR018"].msg,"","warning-snackbar");
    }
    else {
        this.notification.openSnackBar(this.errorCodes["ERR016"].msg,"","red-snackbar");
    }
    },(error)=>
    {

    });
}

ChangePassword1(): void {
  let dialogRef = this.dialog.open(SetPasswordComponent, {
     panelClass: 'custom-dialog-container' ,
    width: '680px',
    height:'350px',
    data: {  }
  });

  dialogRef.afterClosed().subscribe(result => {
    // this.animal = result;
  });
}
openModal()
{
    this.display1 = "block";
}
onCloseHandled()
{

}
myFunction() {
    this.hide = !this.hide;
  }
  myFunctionNew(){
    this.hideNew = !this.hideNew;
  }
  password()
  {
    this.displayStyle="block";
  }
  Passwordset()
  {
    if (this.passwordset != this.confirmpassword) {
        this.notification.openSnackBar(this.errorCodes['WAR030'].msg,"","warning-snackbar");
        return;
    }
    let url="api/User/UpdateUser";
    let postData={
        Username: this.username,
        Email: this.email,
        HomeFolder: this.homefolder,
        NFCCardId: this.nfccardid,
        FirstName: this.firstname,
        LastName: this.lastname,
        UniqueId: this.uniqueid,
        Password: this.passwordset,
        AWSS3AccessID: this.awsaccess_key,
        AWSS3SecretKey: this.awssecret_key,
        Role: this.Role,
        PreferredLanguage: this.Language
    }
this.service.post(url,postData).subscribe(resp=>
    {
        if (resp == true) {
            this.notification.openSnackBar(this.errorCodes['SUC007'].msg,"","success-snackbar");
            this.passwordset='';
            this.confirmpassword='';
            this.displayStyle="none";
        }
    },(error)=>
    {
        this.notification.openSnackBar(error.message,"","red-snackbar");
    })
  }
  Cancel()
  {
    this.displayStyle="none";
    this.displayVerifyLogin="none";
    this.displaycurrentId="none";
    this.displayAssignAWSKey="none";
    this.displayNFCMappingMode="none";
    this.displayDeleteuser="none"
  }
  LoginVerify()
  {
    this.displayVerifyLogin="block";
  }
  passwordverify()
  {
    if (this.verifypassword == undefined || this.verifypassword == "") {       
        this.notification.openSnackBar(this.errorCodes['WAR063'].msg,"","warning-snackbar");
        return;
    }
    if (this.alternateusername == null) {
        this.alternateusername = "";
    }
    let url="api/User/verifyLogin";
    let postData={
        Username: this.username,
        Password: this.verifypassword,
        AlternateUsername: this.alternateusername
    }
    this.service.post(url,postData).subscribe(resp=>
        {
            if (resp == true) {
                this.username = this.username;     
                this.notification.openSnackBar(this.errorCodes['SUC006'].msg,"","success-snackbar");
                this.verifypassword='';
                this.displayVerifyLogin="none";
            }
            else {
                this.notification.openSnackBar(this.errorCodes['ERR003'].msg,"","red-snackbar");
                this.displayVerifyLogin="none";

            }
        },(error)=>
        {
            this.notification.openSnackBar(error.message,"","red-snackbar");

        });

  }
adminmailsave()
{
    let url="api/User/AdminEmailSaveSetting";
    let postData={
        Adminemailcheck: this.checkadminemail,
                    AdminEmail: this.adminemail,
                    AdminSubject: this.adminSubject,
                    AdminBody: this.adminEmailBody
                   }
    this.service.post(url,postData).subscribe(resp=>
    {
if(resp==true)
{
    this.notification.openSnackBar(this.errorCodes['SUC002'].msg,"","success-snackbar");
}
    });
}
switchLang(event:any)
{

}

exportsave()
{
    this.import = true;
    this.export = false;

}

uploadUserExcel(event:any)
{
    const input = window.document.getElementById("fileUpload")!;
        const element = event.currentTarget as HTMLInputElement;
         var emailList:any = [];
            if (element.files && element.files[0]) {
                var myFile = element.files[0];
                var name = myFile.name.split('.')[1]
                if (name == "csv" || name == "xlsx" || name == "xls") {
                    if (myFile) {
                        var reader = new FileReader();
                        reader.addEventListener('load', (e:any)=> {
                            if (name == 'csv') {
                                var rows = e.target.result.split("\r\n");
                                var result = [];
                                var headers = rows[0].split(",");
                                for (var i = 1; i < rows.length; i++) {
                                    var obj:any = {};
                                    var currentline = rows[i].split(",");
                                    if (currentline[0] == "") {
                                        break;
                                    }
                                    for (var j = 0; j < headers.length; j++) {
                                        obj[headers[j]] = currentline[j];
                                    }
                                    result.push(obj);
                                }
                            this. ListOfUsers=result;

                               
                            }
                            else{
                                var bufferData = e.target.result;
                                if (bufferData) {
                                    var fileBytes = new Uint8Array(bufferData);
                                    var length = fileBytes.length;
                                    var binary = "";
                                    for (var i = 0; i < length; i++) {
                                        binary += String.fromCharCode(fileBytes[i]);
                                    }

                                    var xlWorkbook = XLSX.read(binary, { type: 'binary', cellDates: true, cellStyles: true });
                                    if (xlWorkbook) {
                                        var firstSheetName = xlWorkbook.SheetNames[0]; //get the first sheet from the XL file which has the user data
                                        var xlWorkSheet = xlWorkbook.Sheets[firstSheetName]; // get the first worksheet object using the name
                                        var jsonData = XLSX.utils.sheet_to_json(xlWorkSheet, { raw: true });
                                        if (jsonData) {
                                            // this.save(jsonData);
                                            this. ListOfUsers=jsonData;


                                        } else {
                                            // this.msg = "Error : Something Wrong !";
                                        }
                                    }
                                }
                            }
                            // var textContent = e.target.result;
                            // var obj = JSON.parse(textContent);
                            // for (var i = 0; i <= obj.length; i++) {
                            //     // Azure.push(obj[i]);
                            // }
                            // // this.folderTargets = [];
                            // // this.loadAzureTargetTree();
        
                        });
                        // reader.readAsText(myFile);
                        reader.readAsArrayBuffer(myFile);

                    }
                }
                else
                {
                    this.notification.openSnackBar(this.errorCodes['WAR069'].msg,"","warning-snackbar");
                }
            }
}

Importsave()
{

    if (this.ListOfUsers.length > 0) {
        // for (var k = 0; k < this.ListOfUsers.length; k++) {
        //     var newuser:any = {};
        //     newuser['Username'] = this.ListOfUsers[k].UserName;
        //     newuser['FirstName'] = this.ListOfUsers[k].FirstName;
        //     newuser['LastName'] = this.ListOfUsers[k].LastName;
        //     newuser['Email'] = this.ListOfUsers[k].Email;
        //     newuser['HomeFolder'] = this.ListOfUsers[k].HomeFolder;
        //     newuser['NFCCardId'] = this.ListOfUsers[k].NFCCardId;
        //     this.ListOfUsers.push(newuser);

        // }

        let url="api/User/CreateUserList";
        let postData={
            Userlist: this.ListOfUsers
        }
        this.service.post(url,postData).subscribe(resp=>
            {
                if (resp == true) {
                    this.notification.openSnackBar(this.errorCodes['WAR080'].msg,"","success-snackbar");
                    setTimeout(()=> window.location.reload(), 200);

                }
       
                
                else{
                    this.notification.openSnackBar(this.errorCodes['WAR081'].msg,"","warning-snackbar");
                    window.location.reload();

                }
            },(error)=>
            {
                this.notification.openSnackBar(error.message,"","red-snackbar");

            })
    }
    
}
MapNFCCard()
{
    this.notifyalert = false;

    this.btnassign = true;
    this.btngetcard = true;
this.displayEditMapNFC="block";
}
closePopup()
{
    this.displayEditMapNFC="none"; 
}
// DeleteUser
Deleteuser(useridval:any, Uname:any)
{
    this.Delname = Uname;
this.displayDeleteuser="block"
}
DeleteuserConfirm()
{
let url="api/User/DeleteUserById";
let postData={
    Username: this.Delname
}
this.service.post(url,postData).subscribe(resp=>
    {
        if (resp > 0) {
            this.notification.openSnackBar(this.errorCodes['SUC001'].msg,"","success-snackbar");
            this.displayDeleteuser="none"
            setTimeout(()=> window.location.reload(), 100);
        }
        else{
            this.notification.openSnackBar(this.errorCodes['ERR015'].msg,"","red-snackbar");
            this.displayDeleteuser="none"

        }
    })
}
BackUserAccounts()
{
    this.router.navigate(['/useraccounts']);
}
mapNFCId(val:any)
{
    this.btnmapcard = true;
    if (this.Mapcardno != "") {
        this.Mapcardno = '';
        this.btnassign = true;
    }
    this.notifyalert = true;

let url="api/User/MapNfcCardId";
let postData;
    if (localStorage.getItem('Role') == "Users") {
         postData = {
            Mapid: val,
            Userid: localStorage.getItem('Id'),
            Username: this.username
        }
    }
    else {
         postData = {
            Mapid: val,
            Userid: this.userId,
            Username: this.username
        }
    }
    this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
    {
        this.unique_id = resp.UniqueID;
        if ((this.unique_id != "") || (this.unique_id != null)) {
            this.btngetcard = false;
        }
        this.ngxLoader.stop();
    },(error)=>
    {
        this.ngxLoader.stop();
    });

}
Getcardid()
{
let url="api/User/GetCardId";
let postData={
    uniqueid: this.unique_id
}
this.service.post(url,postData).subscribe(resp=>
    {
        this.Mapcardno = resp.NfcCardId;
        if (this.nfccardid == "" || this.nfccardid == null) {
            this.Mapcardno = resp.NfcCardId;
            this.notifyalert = false;
            if (resp.NfcCardId != "") {
                this.btnassign = false;
            }

        }
else{
    this.displaycurrentId="block";
}
if (this.Mapcardno != "") {
    this.notifyalert = false;
    this.btnassign = false;
}
    },(error)=>
    {

    })
}
ConfirmCurrentId()
{
    this.Mapcardno = this.Mapcardno;
                                this.notifyalert = false;
                                this.btnassign = false;
                                this.displaycurrentId="none";
                 
}
back()
{
    if (this.unique_id != "") {
let url="api/User/DeleteUniqueId";
let postData={
    uniqueid: this.unique_id
}
this.service.post(url,postData).subscribe(resp=>
    {

    })
    this.Mapcardno = '';
                        this.unique_id = '';

    }
    this.displayEditMapNFC="none";
}
Assign(val:any)
{
    var card = val;
    this.nfccardid = val;
    if (this.unique_id != "") {
        let url="api/User/DeleteUniqueId";
        let postData={
            uniqueid: this.unique_id
        }
        this.service.post(url,postData).subscribe(resp=>
            {
        
            })
            this.Mapcardno = '';
                                this.unique_id = '';
                                this.displayEditMapNFC="none";

            }
}

UnMapNFCCard()
{
this.displayUnMapNFC="block";
}
UnmapConfirm()
{
    var id;
    if ((localStorage.getItem('Role') == "Administrator") || (localStorage.getItem('Role') == "SuperAdmin")) {
        id = this.userId
    }
    else {
        id = localStorage.getItem('Id')
    }

    let url="api/User/UnMapNFCCard";
    let postData={
        UserId: id
    }
    this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
    {
if(resp==true)
{
    this.nfccardid = '';
    this.notification.openSnackBar(this.errorCodes['SUC005'].msg,"","success-snackbar");
    this.nfcmapcardhide = false;
    this.unmap = true;
    this.displayUnMapNFC="none";
    this.ngxLoader.stop();
}
else{
    this.notification.openSnackBar(this.errorCodes['ERR014'].msg,"","red-snackbar");
    this.ngxLoader.stop();
}
    },(error)=>
    {
this.ngxLoader.stop();
    })
}
//AWSKey Assign

Awsassign()
{
this.displayAssignAWSKey="block";

let url="api/User/GetUsers";
this.service.getAPI(url).then(resp=>
    {
        this.Userlist=resp;
        for (var i = 0; i < this.Userlist.length; i++) {
            if (this.Userlist[i].Username == this.username) {
                this.Userlist.splice(i, 1)
            }
        }
    })
}
 Users:any = [];
CheckUncheckAll()
{
    for (var i = 0; i < this.Userlist.length; i++) {
        this.Userlist[i].Selected = this.IsAllChecked;
      }
      this.getCheckedItemList();
  
}




isAllSelected() {
    this.IsAllChecked = this.Userlist.every(function(item:any) {
        return item.Selected == true;
      })
    this.getCheckedItemList();
  }


  getCheckedItemList(){
    this.checkedList = [];
    for (var i = 0; i < this.Userlist.length; i++) {
      if(this.Userlist[i].Selected)
      this.checkedList.push(this.Userlist[i].Username);
    }
    // this.checkedList = JSON.stringify(this.checkedList);
  }

UsersAssign()
{
    if ((this.awsaccess_key == null || this.awsaccess_key == "") && (this.awssecret_key == null || this.awssecret_key == "")) {
        
        this.notification.openSnackBar(this.errorCodes['WAR054'].msg,"","red-snackbar");

        return;
    }


    let url="api/User/CopyingAWSCredentials";
    let postData={
        Users: this.checkedList,
                    AWSS3AccessID: this.awsaccess_key,
                    AWSS3SecretKey: this.awssecret_key
                
    }
    this.service.post(url,postData).subscribe(resp=>
        {
this.Users="";
if (resp == true) {
    this.notification.openSnackBar(this.errorCodes['SUC027'].msg,"","success-snackbar");
    this.displayAssignAWSKey="none";

}
else{
    this.notification.openSnackBar(this.errorCodes['WAR055'].msg,"","warning-snackbar");
    this.displayAssignAWSKey="none";
}
        },
        (error)=>
        {

        })
}

//NFC MappingMode

NFCMappingMode()
{
this.displayNFCMappingMode="block";
this.scannershow = true;
let url="api/User/GetAllScanners";

this.service.getAPI(url).then(resp=>
    {
        this.scannershow = false;

        if (resp!="No Scanners found" && resp.length > 0) {
            this.ScannerList = resp;
            // this.ScannerList = ['MainScanner-AE235623'];
        }
        // this.ScannerList = ['MainScanner-AE235623'];

    },(error)=>
    {
        this.displayNFCMappingMode="none";

    })
}
ScannerChange(sname:any)
{
    if (sname != undefined) {
        this.scannername = sname;
        this.btnOk = false;
    }
    else {
        this.btnOk = true;
    }
}
NFCMultipleOk(val:any)
{
    this.nfcmultipleuser = true;
    this.Nfcmap = false;
    this.Nfcnorml = true;
    this.displayNFCMappingMode="none";

}
 
MappingNFCCardToUser(id:any,index:any)
{
    this.SpinnerService.show();
    let url=this.apiurl+"api/User/MapNFCCardForUser";

    let postData = 
    {userId:id,
    scannerName:this.scannername
};


}
NFCMappingNormal()
{
    this.nfcmultipleuser = false;
                this.Nfcmap = true;
                this.Nfcnorml = false;

}
cancelnfc()
{
    this.SpinnerService.hide();
}
}





