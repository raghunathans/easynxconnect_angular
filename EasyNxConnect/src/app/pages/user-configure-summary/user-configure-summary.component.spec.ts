import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserConfigureSummaryComponent } from './user-configure-summary.component';

describe('UserConfigureSummaryComponent', () => {
  let component: UserConfigureSummaryComponent;
  let fixture: ComponentFixture<UserConfigureSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserConfigureSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserConfigureSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
