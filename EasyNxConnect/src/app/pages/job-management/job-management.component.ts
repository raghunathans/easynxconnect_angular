import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import {MatIconModule} from '@angular/material/icon';
import { AnyRecord } from 'dns';
import { StoredataService } from 'src/app/storedata.service';
import { event } from 'jquery';
import { checkServerIdentity } from 'tls';
declare var $: any;


@Component({
  selector: 'app-job-management',
  templateUrl: './job-management.component.html',
  styleUrls: ['./job-management.component.scss']
})
export class JobManagementComponent implements OnInit {

  jobmanagementClass:string="col-sm-4";

  
  HomeFolder:any;
   rootfolder:any;
  BoxTargets:boolean= false;

  DropBoxTargets:boolean = false;
  cabinates:boolean = false;
  Buckets:boolean = false;
  EmailTargets:boolean = false;
  FaxTargets:boolean = false;
  Nintex:boolean = false;
  SPOTargets:boolean = false;
  ODBTargets:boolean = false;
  EgnyteTargets:boolean = false;
  HomeFolderTargets:boolean = false;
  AzureBlobsTargets:boolean = false;
  FTPTargets:boolean = false;
  CustomTarget:boolean = false;
  UIpathTargets:boolean = false;
  MapMFiles:any;
  EmailMapTargets:any;
  FaxMapTargets:any;
  MapWorkflow:any;
  errorCodes:any = {};
  Jobs:any=[];
  emailList:any=[];
  Jobgroups:any;
  BoxMapjobs:any;
  ODBMapjobs:any;
  DropBoxMapjobs:any;
  EgnyteMapjobs:any;
  AzureBlobMapjobs:any;
  Mapbucket:any;
  SPOMapjobs:any;
  FolderMapjobs:any;
  MapEmail:any;
  FTPMapjobs:any;
  Mapcabinate:any;
  MapFax:any;
  MapCustom:any;
  MapUIPath:any;
  Joblist:any = [];
  JobName:string="";
  templateJobName:string="";
  folderTargets:any=[];
  multipleedit:any;
  mfiles:any;
  IPprofiles:any;
  ProfileName:any;
  buttoncolor:any;
  MetaDataName:string="";
  MetaDataType:any;
  Jobname:string="";
  Description:string="";
  editJobName:any;
  editJobNameId:any;
  templateok:boolean=true;
  templateclear:boolean=true;
  emailData:any={};
    jsTreeObject: any=true;
    FtpFolder:any;
    AmazonS3dtl:any;
    Username:string="";
    Region:any;
    multipleeditjobgroup:any;
    JobGroup:any;
JobgroupId:any;
editjobpopup="none";
  constructor(public translate: TranslateService,private dataStore:StoredataService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) { }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');
    this.AmazonS3dtl=this.dataStore.AmazonS3;

    this.translate.use(returnUrl);    
    this.errorCodes=this.ErrorCodes.errormsgs();
    this.LoadJobs();
    // this.LoadJobGroups();
    setTimeout(()=> this.LoadJobGroups(), 500);


    //Box
    this.BoxMapjobs = localStorage.getItem('BoxMap');
   if (this.BoxMapjobs == 'BoxJobs') {
     setTimeout(()=> this.LoadBox(), 500);
    // setInterval(() => this.LoadBox(), 500);
      this.LoadBox();  
}
//ODB
   this.ODBMapjobs = localStorage.getItem('ODBMap');
   if (this.ODBMapjobs == 'ODBJobs') {
    setTimeout(()=> this.LoadODB(), 500);
     this.LoadODB();
   }
   //DropBox
   this.DropBoxMapjobs = localStorage.getItem('DropBoxMap');
   if (this.DropBoxMapjobs == 'DropBoxJobs') {
    setTimeout(()=> this.LoadDropBox(), 500);
     this.LoadDropBox();
   }

   //Egnyte
   this.EgnyteMapjobs = localStorage.getItem('EgnyteMap');
   if (this.EgnyteMapjobs == 'EgnyteJobs') {
    setTimeout(()=> this.LoadEgnyteTargets(), 500);
     this.LoadEgnyteTargets();
   }
//AmazonS3


this.Mapbucket = localStorage.getItem('Map');
   if (this.Mapbucket == 'Buckets') {
    if(this.AmazonS3dtl!=undefined)
{
    this.Username=this.AmazonS3dtl.Username;
    this.Region=this.AmazonS3dtl.Region;
    
}
    setTimeout(()=> this.AmazonS3Load(), 500);
     this.AmazonS3Load();
   }
   //AzureBlob
   this.AzureBlobMapjobs = localStorage.getItem('AzureBlobMap');
   if (this.AzureBlobMapjobs == 'AzureBlobJobs') {
    setTimeout(()=> this.AzureLoad(), 500);
     this.AzureLoad();
   }
//SPO
   this.SPOMapjobs = localStorage.getItem('SPOMap');
   if (this.SPOMapjobs == 'SPOJobs') {
    setTimeout(()=> this.LoadSPO(), 500);
     this.LoadSPO();
   }
//Folder
   localStorage.setItem("hitlevel", '1');
   this.FolderMapjobs = localStorage.getItem('FolderMap');
   if (this.FolderMapjobs == 'FolderJobs') {
    this.LoadHomeFolder();
       }
//Email
    this.MapEmail = localStorage.getItem('mapEmail');
    if (this.MapEmail == 'EmailMap') {
    this.LoadEmail();
    }
    
//FTP
this.FTPMapjobs = localStorage.getItem('FTPMap');
if (this.FTPMapjobs == 'FTPJobs') {
    setTimeout(()=> this.LoadFTP(), 500);
    this.LoadFTP();
    }

//Docuware cabinates


this.Mapcabinate = localStorage.getItem('MapDoc');
if (this.Mapcabinate == 'Cabinates') {
    setTimeout(()=> this.LoadCabinates(), 500);
    this.LoadCabinates();
    }

    //Fax
   this.MapFax = localStorage.getItem('mapFax');
   if (this.MapFax == 'FaxMap') {
    this.LoadFaxConnectors();
       }
  }

 

  
  LoadJobs()
  {
    let url="api/Jobs/GetJobsList";
    this.service.getAPI(url).then((resp:any[])=>{
this.Jobs=resp;
    });


  }
  LoadJobGroups()
  {
    let url="api/Jobs/GetJobGroups";
    this.service.getAPI(url).then(resp=>{
this.Jobgroups=resp;
    });
  }
  
  JobMap()
  {
    if (this.Jobs != undefined && this.Jobs.length > 0) {
        for (var i = 0; i < this.Jobs.length; i++) {
            this.JobName = this.Jobs[i].JobName;
            this.Joblist.push(this.JobName);
        }
    }
    let url="api/Jobs/CreateJob";
    let postData:any;
     if (this.BoxMapjobs == 'BoxJobs') {
         postData = {
            JobName: "",
            ConnectorName: "Send to Box",
            ConnectorPrefix: "Box",
            ConnectorSuffix: "",
            Description: "Upload document to a Box Folder",
            XmlFileName: "connector_info_sendtobox.xml",
            JobFileFormat: "pdf",
            JobDestination: "",
            JobTemplate: this.templateJobName,
            JobTargets: this.folderTargets,
            AvailableJobs: this.Joblist
        }
    }
    else if (this.AzureBlobMapjobs == 'AzureBlobJobs') {
        postData = {
            JobName: "",
            ConnectorName: "Send to Azure Storage",
            ConnectorPrefix: "Azure",
            ConnectorSuffix: "",
            Description: "Upload documents to Azure Storage container",
            XmlFileName: "connector_info_sendtoazurestorage.xml",
            JobFileFormat: "pdf",
            JobDestination: "",
            JobTemplate: this.templateJobName,
            JobTargets: this.folderTargets,
            AvailableJobs: this.Joblist
        }
    }
    else if (this.Mapcabinate == 'Cabinates') {
        for (var i = 0; i < this.Filecabinates.length; i++) {
            if (this.Filecabinates[i].Selected) {
                var cabinatename = this.Filecabinates[i].cabinatename;
                this.folderTargets.push({ "JobName": cabinatename, "TargetFolderName": cabinatename, "TargetSiteName": null, "DocLibrarytarget": null });
            }
        }
        postData = {
            JobName: "",
            ConnectorName: "Docuware",
            ConnectorPrefix: "DW_",
            ConnectorSuffix: "",
            Description: "Upload documents to Docuware Cabinet",
            XmlFileName: "connector_info_sendtodocuware.xml",
            JobFileFormat: "pdf",
            JobDestination: "",
            JobTemplate: this.templateJobName,
            JobTargets: this.folderTargets,
            AvailableJobs: this.Joblist
        }
    }
    else if (this.Mapbucket == 'Buckets') {
        for (var i = 0; i < this.GetBuckets.length; i++) {
            if (this.GetBuckets[i].Selected) {
                var bucketname = this.GetBuckets[i].BucketName;
                this.folderTargets.push({ "JobName": bucketname, "TargetFolderName": bucketname, "TargetSiteName": null, "DocLibrarytarget": null });
            }
        }
         postData= {
            JobName: "",
            ConnectorName: "Amazon S3",
            ConnectorPrefix: "AWSS3_",
            ConnectorSuffix: "",
            Description: "Upload documents to Amazon S3 Buckets",
            XmlFileName: "connector_info_sendtoamazons3.xml",
            JobFileFormat: "pdf",
            JobDestination: "",
            JobTemplate: this.templateJobName,
            JobTargets: this.folderTargets,
            AvailableJobs: this.Joblist
        }
    }
    else if (this.MapEmail == 'EmailMap') {
        for (var i = 0; i < this.EmailMapTargets.length; i++) {
            if (this.EmailMapTargets[i].Selected) {
                var emailName = this.EmailMapTargets[i].Name;
                var emailAddress = this.EmailMapTargets[i].Email;
                this.folderTargets.push({ "JobName": emailName, "TargetFolderName": emailAddress, "TargetSiteName": null, "DocLibrarytarget": null });
            }
        }
        postData = {
            JobName: "",
            ConnectorName: "Send Email",
            ConnectorPrefix: "FaxTo",
            ConnectorSuffix: "",
            Description: "Send email to preconfigured target",
            XmlFileName: "connector_info_sendemail.xml",
            JobFileFormat: "pdf",
            JobDestination: "",
            JobTemplate: this.templateJobName,
            JobTargets: this.folderTargets,
            AvailableJobs: this.Joblist
        }
    }
    else if (this.MapFax == 'FaxMap') {
        for (var i = 0; i < this.FaxMapTargets.length; i++) {
            if (this.FaxMapTargets[i].Selected) {
                var faxNumber = this.FaxMapTargets[i].FaxNumber;
                var faxName = this.FaxMapTargets[i].Name;
                this.folderTargets.push({ "JobName": faxName, "TargetFolderName": faxNumber, "TargetSiteName": null, "DocLibrarytarget": null });
            }
        }
         postData = {
            JobName: "",
            ConnectorName: "Send Fax",
            ConnectorPrefix: "FaxTo_", // Should be translated?
            ConnectorSuffix: "",
            Description: "Send fax to preconfigured target",
            XmlFileName: "connector_info_sendfax.xml",
            JobFileFormat: "pdf",
            JobDestination: "",
            JobTemplate: this.templateJobName,
            JobTargets: this.folderTargets,
            AvailableJobs: this.Joblist
        }
    }
    else if (this.DropBoxMapjobs == 'DropBoxJobs') {
         postData = {
            JobName: "",
            ConnectorName: "Send to Dropbox",
            ConnectorPrefix: "DropBox",
            ConnectorSuffix: "",
            Description: "Upload document to a DropBox Folder",
            XmlFileName: "connector_info_sendtodropbox.xml",
            JobFileFormat: "pdf",
            JobDestination: "",
            JobTemplate: this.templateJobName,
            JobTargets: this.folderTargets,
            AvailableJobs: this.Joblist
        }
    }
    else if (this.ODBMapjobs == 'ODBJobs') {
         postData = {
            JobName: "",
            ConnectorName: "Send to OneDrive",
            ConnectorPrefix: "Odb",
            ConnectorSuffix: "",
            Description: "Upload documents to a OneDrive folder",
            XmlFileName: "connector_info_sendtoonedrive.xml",
            JobFileFormat: "pdf",
            JobDestination: "",
            JobTemplate: this.templateJobName,
            JobTargets: this.folderTargets,
            AvailableJobs: this.Joblist
        }
    }
    else if (this.SPOMapjobs == 'SPOJobs') {
         postData = {
            JobName: "",
            ConnectorName: "Send to SPOnline",
            ConnectorPrefix: "Spo",
            ConnectorSuffix: "",
            Description: "Upload documents to a SharePoint document library",
            XmlFileName: "connector_info_sendtosponline.xml",
            JobFileFormat: "pdf",
            JobDestination: "",
            JobTemplate: this.templateJobName,
            JobTargets: this.folderTargets,
            AvailableJobs: this.Joblist
        }
    }
    else if (this.EgnyteMapjobs == 'EgnyteJobs') {
         postData = {
            JobName: "",
            ConnectorName: "Send to Egnyte",
            ConnectorPrefix: "Egnyte",
            ConnectorSuffix: "",
            Description: "Upload documents to a Egnyte Folder",
            XmlFileName: "connector_info_sendtoegnyte.xml",
            JobFileFormat: "pdf",
            JobDestination: "",
            JobTemplate: this.templateJobName,
            JobTargets: this.folderTargets,
            AvailableJobs: this.Joblist
        }
    }
    else if (this.FolderMapjobs == 'FolderJobs') {
         postData = {
            JobName: "",
            ConnectorName: "Copy To Folder",
            ConnectorPrefix: "Folder",
            ConnectorSuffix: "",
            Description: "Upload document to a Folder",
            XmlFileName: "connector_info_copytofolder.xml",
            JobFileFormat: "pdf",
            JobDestination: "",
            JobTemplate: this.templateJobName,
            JobTargets: this.folderTargets,
            AvailableJobs: this.Joblist
        }
    }
    else if (this.FTPMapjobs == 'FTPJobs') {
         postData = {
            JobName: "",
            ConnectorName: "Upload to FTP",
            ConnectorPrefix: "FTP",
            ConnectorSuffix: "",
            Description: "Upload documents to a FTP folder",
            XmlFileName: "connector_info_uploadtoftp.xml",
            JobFileFormat: "pdf",
            JobDestination: "",
            JobTemplate: this.templateJobName,
            JobTargets: this.folderTargets,
            AvailableJobs: this.Joblist
        }
    }
    if (this.folderTargets.length <= 0) {
        this.notification.openSnackBar(this.errorCodes['WAR034'].msg,"","warning-snackbar");
        return;
    }
        this.ngxLoader.start();
        this.service.post(url,postData).subscribe(resp => {
         if(resp==true)
         {
            this.notification.openSnackBar(this.errorCodes["SUC008"].msg,"","success-snackbar");
            this.folderTargets="";

            this.LoadJobs();
            this.LoadJobGroups();
            this.ngxLoader.stop();   
            window.location.reload();
         } 
         else{
            this.notification.openSnackBar(this.errorCodes["WAR010"].msg,"","warning-snackbar");
            this.Joblist = [];
                        this.folderTargets = [];
                        this.ngxLoader.stop();   

         }
        },(error)=>
        {
          this.notification.openSnackBar(error.message,"","red-snackbar");
          this.ngxLoader.stop();   
        })

    
  }
  
  LoadBox()
  {
       const jsTree=$("#jstree_demo_div").jstree({    
      'core': {
          'data': function (node:any, cb:any) {
              if (!node) return;
              var isChildNode = false;
              var url = "";
              if (node.id === '#') {
                  url = 'http://localhost/EasyNxConnectWebApi/api/jobs/GetBoxFolders';
                  
              }
              else {
                  //isChildNode = true;
                  url = 'http://localhost/EasyNxConnectWebApi/api/jobs/GetBoxFolders?pid=' + node.id;
              }
              //console.log(url);
              //return url;


              $.ajax({
                  type: 'GET',
                  url: url,
                  async: true,
                  success: function (resp:any, textStatus:any, xhr:any) {
                      if (resp) {
                          if (resp.status == "SUCCESS") {
                              var treeNodeData = null;
                              //if (!isChildNode) {
                              treeNodeData = resp.data;
                              //}
                              //else {
                              //    treeNodeData = resp.data;
                              //}

                              cb.call(this, treeNodeData);//this call will load the nodes onto the tree 
                          }
                          else {
                              //alert(resp.status + ": " + resp.error.err_source + ": " + resp.error.err_message);
                              if (resp.exit_code != null) {
                                  
                                  this.notification.openSnackBar(this.errorCodes[resp.exit_code].msg,"","warning-snackbar");

                              }

                          }

                      }
                  }

              })
          }
      },
      "plugins": ["checkbox", "search"],
      "checkbox": {
          "keep_selected_style": false,
          "three_state": false,
          "tie_selection": false, // for checking without selecting and selecting without checking
      },
  }).on('hover_node.jstree', function (e:any, data:any) {
      $("#" + data.node.id).prop('title', data.node.text);
  });
  $('#jstree_demo_div').on("check_node.jstree uncheck_node.jstree",  (e:any, data:any) => {
      var path = data.instance.get_path(data.node, '/');
      //this.folderTargets.push({ "FolderName": data.node.text, "FolderTarget": path, "SiteTarget": null, "DocLibrarytarget": null });
      if (data.selected[0] == undefined) {
          var removeIndex = this.folderTargets.map(function (item:any) { return item.JobName; }).indexOf(data.node.text);
          for (var i = 0; i < this.folderTargets.length; i++) {
              if (this.folderTargets[i].JobName == data.node.text) {
                  if (removeIndex == 0) {
                      this.folderTargets.splice(removeIndex, 1);
                  }
                  else {
                      this.folderTargets.splice(removeIndex, i);
                  }
                  break;
              }
          }
      }
      else {
          this.folderTargets.push({ "JobName": data.node.text, "TargetFolderName": path, "TargetSiteName": null, "DocLibrarytarget": null });
          data.selected.length = 0;
      }
  });

  this.BoxTargets = true;


  localStorage.removeItem('Map');
  localStorage.removeItem('mapEmail');
  localStorage.removeItem('mapFax');
  localStorage.removeItem('MapDoc');
  localStorage.removeItem('mapMFiles');
  localStorage.removeItem('MapWorkflow');
  localStorage.removeItem('ODBMap');
  localStorage.removeItem('SPOMap');
  localStorage.removeItem('EgnyteMap');
  localStorage.removeItem('DropBoxMap');
  }

  //ODB connectors targets load

  LoadODB()
  {
    const jsTree = $("#jstree_demo_div_odb").jstree({
        "plugins": ["checkbox", "search"],
        "checkbox": {
            "keep_selected_style": false,
            "three_state": false,
            "tie_selection": false, // for checking without selecting and selecting without checking
        },
        'core': {
            'data': function (node:any, cb:any) {
                if (!node) return;

                var isChildNode = false;
                var url = "";
                if (node.id === '#') {
                    url = 'http://localhost/EasyNXConnectWebApi/api/Jobs/GetOneDriveFolders';

                }
                else {
                    isChildNode = true;
                    url = 'http://localhost/EasyNXConnectWebApi/api/Jobs/GetOneDriveFolders?username=' + node.id;

                }
                $.ajax({
                    type: 'GET',
                    url: url,
                    async: false,
                    success: function (response:any, textStatus:any, xhr:any) {
                        if (response) {
                            if (response.status == "SUCCESS") {
                                var treeNodeData = null;
                                if (!isChildNode) { 
                                    treeNodeData = response.data;
                                    // this.Notification.openSnackBar("tets","","warning-snackbar");

                                }
                                else {
                                    treeNodeData = response.data;
                                }

                                cb.call(this, treeNodeData);//this call will load the nodes onto the tree 
                            }
                            else {
                                if (response.exit_code != null) {
                                    this.notification.openSnackBar(this.errorCodes[response.exit_code].msg,"","warning-snackbar");
                                }
                            }
                        }
                    }
                })

            }
        }
    }).on('hover_node.jstree', function (e:any, data:any) {
        $("#" + data.node.id).prop('title', data.node.text);
    });
    $('#jstree_demo_div_odb').on("check_node.jstree uncheck_node.jstree",  (e:any, data:any) => {
        var path = data.instance.get_path(data.node, '/');
        //this.folderTargets.push({ "FolderName": data.node.text, "FolderTarget": path, "SiteTarget": null, "DocLibrarytarget": null });

        if (data.selected[0] == undefined) {
            var removeIndex = this.folderTargets.map(function (item:any) { return item.JobName; }).indexOf(data.node.text);
            for (var i = 0; i < this.folderTargets.length; i++) {
                if (this.folderTargets[i].JobName == data.node.text) {
                    if (removeIndex == 0) {
                        this.folderTargets.splice(removeIndex, 1);
                    }
                    else {
                        this.folderTargets.splice(removeIndex, i);
                    }
                    break;
                }
            }
        }
        else {
            this.folderTargets.push({ "JobName": data.node.text, "TargetFolderName": path, "TargetSiteName": null, "DocLibrarytarget": null });
            data.selected.length = 0;
        }
    });

    this.ODBTargets = true;

    localStorage.removeItem('Map');
    localStorage.removeItem('mapEmail');
    localStorage.removeItem('mapFax');
    localStorage.removeItem('MapDoc');
    localStorage.removeItem('mapMFiles');
    localStorage.removeItem('MapWorkflow');
    localStorage.removeItem('BoxMap');
    localStorage.removeItem('SPOMap');
    localStorage.removeItem('EgnyteMap');
    localStorage.removeItem('DropBoxMap');
  }

  //Load DropBox Connector Targets.

  LoadDropBox()
  {
    const jsTree = $("#jstree_demo_Dropdiv").jstree({
        "plugins": ["checkbox", "search"],
        "checkbox": {
            "keep_selected_style": false,
            "three_state": false,
            "tie_selection": false, // for checking without selecting and selecting without checking
        },
        'core': {
            'data': function (node:any, cb:any) {
                if (!node) return;
                var isChildNode = false;
                var url = "";
                if (node.id === '#') {
                    url = 'http://localhost/EasyNXConnectWebApi/api/Jobs/GetDropBoxFolders';
                }
                else {
                    if (node.parent == '#') {
                        url = 'http://localhost/EasyNXConnectWebApi/api/jobs/GetDropBoxFolders?baseFolderPath=' + node.text + '&fid=' + node.id;
                    }
                    else {
                        var path = node.original.sitepath.substring(1);
                        url = 'http://localhost/EasyNXConnectWebApi/api/jobs/GetDropBoxFolders?baseFolderPath=' + path + '&fid=' + node.id;

                    }
                }
                $.ajax({
                    type: 'GET',
                    url: url,
                    async: true,
                    success: function (response:any, textStatus:any, xhr:any) {
                        if (response) {
                            if (response.status == "SUCCESS") {
                                var treeNodeData = null;
                                treeNodeData = response.data;
                                cb.call(this, treeNodeData);//this call will load the nodes onto the tree 
                            }
                            else {
                                if (response.exit_code != null) {
                                    
                                    this.notification.openSnackBar(this.errorCodes[response.exit_code].msg,"","warning-snackbar");

                                }

                            }

                        }
                    }

                })
            }
        }
    }).on('hover_node.jstree', function (e:any, data:any) {
        $("#" + data.node.id).prop('title', data.node.text);
    });
    $('#jstree_demo_Dropdiv').on("check_node.jstree uncheck_node.jstree", (e:any, data:any)=> {
        var path = data.instance.get_path(data.node, '/');
        if (data.selected[0] == undefined) {
            var removeIndex = this.folderTargets.map(function (item:any) { return item.JobName; }).indexOf(data.node.text);
            for (var i = 0; i < this.folderTargets.length; i++) {
                if (this.folderTargets[i].JobName == data.node.text) {
                    if (removeIndex == 0) {
                        this.folderTargets.splice(removeIndex, 1);
                    }
                    else {
                        this.folderTargets.splice(removeIndex, i);
                    }
                    break;
                }
            }
        }
        else {
            this.folderTargets.push({ "JobName": data.node.text, "TargetFolderName": path, "TargetSiteName": null, "DocLibrarytarget": null });
            data.selected.length = 0;
        }
    });
    this.DropBoxTargets = true;

    localStorage.removeItem('Map');
    localStorage.removeItem('mapEmail');
    localStorage.removeItem('mapFax');
    localStorage.removeItem('MapDoc');
    localStorage.removeItem('mapMFiles');
    localStorage.removeItem('MapWorkflow');
    localStorage.removeItem('ODBMap');
    localStorage.removeItem('SPOMap');
    localStorage.removeItem('EgnyteMap');
  }

  //Load egnyte targets

  LoadEgnyteTargets()
  {

    const jsTree = $("#jstree_demo_div_egnyte").jstree({
        "plugins": ["checkbox", "search"],
        "checkbox": {
            "keep_selected_style": false,
            "three_state": false,
            "tie_selection": false, // for checking without selecting and selecting without checking
        },
        'core': {
            'data': function (node:any, cb:any) {
                if (!node) return;

                var isChildNode = false;
                var url = "";
                if (node.id === '#') {
                    url = 'http://localhost/EasyNXConnectWebApi/api/Jobs/GetEgnyteFolders';

                }
                else {
                    isChildNode = true;
                    url = 'http://localhost/EasyNXConnectWebApi/api/Jobs//GetEgnyteFolders?baseFolderPath=' + node.original.sitepath;
                }
                $.ajax({
                    type: 'GET',
                    url: url,
                    async: false,
                    success: function (response:any, textStatus:any, xhr:any) {
                        if (response) {
                            if (response.status == "SUCCESS") {
                                var treeNodeData = null;
                                if (!isChildNode) {
                                    treeNodeData = response.data;
                                }
                                else {
                                    treeNodeData = response.data;
                                }

                                cb.call(this, treeNodeData);//this call will load the nodes onto the tree 
                            }
                            else {
                                if (response.exit_code != null) {
                                    
                                    this.notification.openSnackBar(this.errorCodes[response.exit_code].msg,"","warning-snackbar");

                                }
                            }
                        }
                    }
                })

            }
        }
    });
    $('#jstree_demo_div_egnyte').on("check_node.jstree uncheck_node.jstree",(e:any, data:any)=> {
        var path = data.instance.get_path(data.node, '/');
        //this.folderTargets.push({ "FolderName": data.node.text, "FolderTarget": path, "SiteTarget": null, "DocLibrarytarget": null });

        if (data.selected[0] == undefined) {
            var removeIndex = this.folderTargets.map(function (item:any) { return item.JobName; }).indexOf(data.node.text);
            for (var i = 0; i < this.folderTargets.length; i++) {
                if (this.folderTargets[i].JobName == data.node.text) {
                    if (removeIndex == 0) {
                        this.folderTargets.splice(removeIndex, 1);
                    }
                    else {
                        this.folderTargets.splice(removeIndex, i);
                    }
                    break;
                }
            }
        }
        else {
            this.folderTargets.push({ "JobName": data.node.text, "TargetFolderName": path, "TargetSiteName": null, "DocLibrarytarget": null });
            data.selected.length = 0;
        }
    });

    this.EgnyteTargets = true;

                localStorage.removeItem('Map');
                localStorage.removeItem('mapEmail');
                localStorage.removeItem('mapFax');
                localStorage.removeItem('MapDoc');
                localStorage.removeItem('mapMFiles');
                localStorage.removeItem('MapWorkflow');
                localStorage.removeItem('BoxMap');
                localStorage.removeItem('ODBMap');
                localStorage.removeItem('DropBoxMap');

  }
//SPO Connector Targets Load

LoadSPO()
{
     this.jsTreeObject = $('#jstree_demo_div_spo').jstree(true);


     const jsTree = $("#jstree_demo_div_spo").jstree({
        "plugins": ["checkbox", "search"],
        "checkbox": {
            "keep_selected_style": false,
            "three_state": false,
            "tie_selection": false, // for checking without selecting and selecting without checking
        },
        'core': {
            'data': function (node:any, cb:any) {
                if (!node) return;

                var isChildNode = false;

                var apiUrl = 'http://localhost/EasyNXConnectWebApi/api/Jobs/GetRootSite'; // for getting root folders or sites

                if (node.id != '#') // this means user is expanding a root node, which means we have to populate the child nodes.. change the url
                {
                    isChildNode = true;

                    if (node.original.type == 'folder') {
                        if (node.original.siteid == null)
                            apiUrl = 'http://localhost/EasyNXConnectWebApi/api/jobs/GetChildren?t=folder&s=' + node.original.parent + '&d=' + node.id + '&p=' + "";
                        else
                            apiUrl = 'http://localhost/EasyNXConnectWebApi/api/jobs//GetChildren?t=folder&s=' + node.original.siteid + '&d=' + node.original.driveid + '&p=' + node.id;
                    }
                    else
                        apiUrl = 'http://localhost/EasyNXConnectWebApi/api/jobs//GetChildren?t=site&s=' + node.id + '&d=' + "" + '&p=' + "";
                }
                $.ajax({
                    type: 'GET',
                    url: apiUrl,
                    async: true,
                    success: function (response:any, textStatus:any, xhr:any) {
                        if (response) {
                            if (response.status == "SUCCESS") {
                                var treeNodeData = null;
                                if (!isChildNode) {
                                    treeNodeData = response.root;
                                }
                                else {
                                    treeNodeData = response.data;
                                }

                                cb.call(this, treeNodeData);//this call will load the nodes onto the tree 
                            }
                            else {
                                if (response.exit_code != null) {
                                    this.notification.openSnackBar(this.errorCodes[response.exit_code].msg,"","red-snackbar");
                                }
                            }
                        }
                    }
                })
            }
        }
    });

      this.jsTreeObject = $('#jstree_demo_div_spo').jstree(true);
    $('#jstree_demo_div_spo').on("check_node.jstree uncheck_node.jstree",  (e:any, data:any) => {
        var path = data.instance.get_path(data.node, '/');
        var fname = "";
        var fpath = "";
        var spath = "";
        var dlibpath = "";

        for (var i = 0; i < data.node.parents.length; i++) {
            var item = data.node.parents[i];
            if (item != "#") {
                if (this.jsTreeObject._model.data[item].original.type == undefined) {
                    break;
                }
                var sitepath = "";
                var folderpath = "";
                var doclibpath = "";
                console.log(this.jsTreeObject._model.data);
                if (this.jsTreeObject._model.data[item].original.type == "site") {
                    sitepath = (this.jsTreeObject._model.data[item].original.sitename);
                    if ((sitepath != undefined) && (sitepath != null) && (sitepath != "")) {
                        if (spath == "") {
                            spath = sitepath
                        }
                        else {
                            spath = sitepath + '/' + spath;
                        }
                    }

                }
                if (this.jsTreeObject._model.data[item].original.type == "folder") {
                    folderpath = (this.jsTreeObject._model.data[item].original.text);
                    if (fpath == "") {
                        fpath = folderpath + '/' + data.node.text;
                    }
                    else {
                        fpath = folderpath + '/' + fpath;
                    }
                }
                if (this.jsTreeObject._model.data[item].original.type == "doclib") {
                    doclibpath = (this.jsTreeObject._model.data[item].text);
                    if (dlibpath == "") {
                        dlibpath = doclibpath;
                    }
                    else {
                        dlibpath = doclibpath + '/' + dlibpath;
                    }
                }
            }
            else {
                break;
            }
        }
        //this.folderTargets.push({ "FolderName": data.node.text, "FolderTarget": fpath, "SiteTarget": spath, "DocLibrarytarget": dlibpath});

        if (data.selected[0] == undefined) {
            var removeIndex = this.folderTargets.map(function (item:any) { return item.JobName; }).indexOf(data.node.text);
            for (var i = 0; i < this.folderTargets.length; i++) {
                if (this.folderTargets[i].JobName == data.node.text) {
                    if (removeIndex == 0) {
                        this.folderTargets.splice(removeIndex, 1);
                    }
                    else {
                        this.folderTargets.splice(removeIndex, i);
                    }
                    break;
                }
            }
        }
        else {
            this.folderTargets.push({ "JobName": data.node.text, "TargetFolderName": fpath, "TargetSiteName": spath, "DocLibrarytarget": dlibpath });
            data.selected.length = 0;
        }

    });
    this.SPOTargets = true;

    localStorage.removeItem('Map');
    localStorage.removeItem('mapEmail');
    localStorage.removeItem('mapFax');
    localStorage.removeItem('MapDoc');
    localStorage.removeItem('mapMFiles');
    localStorage.removeItem('MapWorkflow');
    localStorage.removeItem('BoxMap');
    localStorage.removeItem('ODBMap');
    localStorage.removeItem('EgnyteMap');
    localStorage.removeItem('DropBoxMap');

   


}
//AzureBlob load targets
AzureFolder:any;
AzureLoad()
{
    let url="api/Jobs/GetAzureBlobStorageContainers";
    this.service.getAPI(url).then((resp:any)=>{
 this.AzureFolder=resp.data;
if (resp.status == "SUCCESS") {
    $('#jstree_demo_divAzure').jstree({
        core: {
            data: this.AzureFolder,
            check_callback: false
        },
        checkbox: {
            three_state: false, // to avoid that fact that checking a node also check others
            whole_node: false, // to avoid checking the box just clicking the node 
            tie_selection: false, // for checking without selecting and selecting without checking
            keep_selected_style: false,
        },
        plugins: ['checkbox']

    }).on('hover_node.jstree', function (e:any, data:any) {
        $("#" + data.node.id).prop('title', data.node.text);
    });
     this.SelectAzureTreeCheckBox();
}
else if (resp.status == "FAILED") {
    this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
}
    });
    this.AzureBlobsTargets = true;

    localStorage.removeItem('Map');
    localStorage.removeItem('mapEmail');
    localStorage.removeItem('mapFax');
    localStorage.removeItem('MapDoc');
    localStorage.removeItem('mapMFiles');
    localStorage.removeItem('MapWorkflow');
    localStorage.removeItem('BoxMap');
    localStorage.removeItem('ODBMap');
    localStorage.removeItem('DropBoxMap');


}

//AmazonS3 targets load
GetBuckets:any;
AmazonS3Load()
{


    this.Buckets = true;

    localStorage.removeItem('mapEmail');
    localStorage.removeItem('mapFax');
    localStorage.removeItem('MapDoc');
    localStorage.removeItem('mapMFiles');
    localStorage.removeItem('MapWorkflow');
    localStorage.removeItem('ODBMap');
    localStorage.removeItem('BoxMap');
    localStorage.removeItem('SPOMap');
    localStorage.removeItem('EgnyteMap');
    localStorage.removeItem('mapCustom');
    localStorage.removeItem('DropBoxMap');
    let url="api/Amazon/GetBuckets"
    let postData={
        Username: this.Username,
        SelectedRegion:this.Region,
    }
    this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp=>{
        this.GetBuckets = resp;

        this.ngxLoader.stop();
    });
}

Filecabinates:any;
//Load Docuware Cabinates
LoadCabinates()
{
    this.cabinates = true;

    localStorage.removeItem('mapEmail');
    localStorage.removeItem('mapFax');
    localStorage.removeItem('Map');
    localStorage.removeItem('mapMFiles');
    localStorage.removeItem('MapWorkflow');
    localStorage.removeItem('ODBMap');
    localStorage.removeItem('BoxMap');
    localStorage.removeItem('SPOMap');
    localStorage.removeItem('EgnyteMap');
    localStorage.removeItem('mapCustom');
    localStorage.removeItem('DropBoxMap');
    let url="api/Docuware/GetFileCabinets"
    
    this.ngxLoader.start();
    this.service.getAPI(url).then(resp=>{
        this.Filecabinates = resp;

        this.ngxLoader.stop();
    });

}
uploadFileAzure(event:Event)
{
    
    try {
        const input = window.document.getElementById("myAzureFile")!;
        const element = event.currentTarget as HTMLInputElement;
let Azure=this.AzureFolder;
         var emailList:any = [];
            if (element.files && element.files[0]) {
                var myFile = element.files[0];
                var reader = new FileReader();
                reader.addEventListener('load', function (e:any) {
                    var textContent = e.target.result;
                    var obj = JSON.parse(textContent);
                    for (var i = 0; i <= obj.length; i++) {
                        Azure.push(obj[i]);
                    }
                    $("#jstree_demo_divAzure").jstree('destroy');
                    // this.folderTargets = [];
                    // this.loadAzureTargetTree();

                });
                reader.readAsText(myFile);
            }
    }
    catch (e) {
        alert("Exception Occurred : " + e);
    }
}


// loadAzureTargetTree() {
//     $('#jstree_demo_divAzure').jstree({
//         core: {
//             data: AzureFolder,
//             check_callback: false
//         },
//         checkbox: {
//             three_state: false, // to avoid that fact that checking a node also check others
//             whole_node: false, // to avoid checking the box just clicking the node 
//             tie_selection: false, // for checking without selecting and selecting without checking
//             keep_selected_style: false,
//         },
//         plugins: ['checkbox']

//     }).on('hover_node.jstree', function (e:any, data:any) {
//         $("#" + data.node.id).prop('title', data.node.text);
//     });
//     this.SelectAzureTreeCheckBox();
// }
 SelectAzureTreeCheckBox() {
    $('#jstree_demo_divAzure').on("check_node.jstree uncheck_node.jstree", (e:any, data:any) =>{
        var path = data.instance.get_path(data.node, '/');
        if (data.selected[0] == undefined) {
            var removeIndex = this.folderTargets.map(function (item:any) { return item.JobName; }).indexOf(data.node.text);
            for (var i = 0; i < this.folderTargets.length; i++) {
                if (this.folderTargets[i].JobName == data.node.text) {
                    if (removeIndex == 0) {
                        this.folderTargets.splice(removeIndex, 1);
                    }
                    else {
                        this.folderTargets.splice(removeIndex, i);
                    }
                    break;
                }
            }
        }
        else {
            this.folderTargets.push({ "JobName": data.node.text, "TargetFolderName": path, "TargetSiteName": null, "DocLibrarytarget": null });
            data.selected.length = 0;
        }

    });

}

//Folder Connector MapJobs
  LoadHomeFolder()
  {
    this.HomeFolderTargets = true;
    if (localStorage.getItem('homefolderlist') != null) {
        localStorage.setItem("hitlevel", '2');
        this.HomeFolder = JSON.parse(localStorage.getItem('homefolderlist') ||'{}')
        //this.rootfolder = localStorage.getItem('GetHome');

        const jsTree = $("#jstree_demo_divHome").jstree({
            "plugins": ["checkbox", "search"],
            "checkbox": {
                "keep_selected_style": false,
                "three_state": false,
                "tie_selection": false, // for checking without selecting and selecting without checking
            },
            'core': {
                'data': function (node:any, cb:any,) {
                    if (!node) return;
                    var isChildNode = false;
                    var url = "";

                    if (node.id === '#')
                        url = 'http://localhost/EasyNXConnectWebApi/api/Jobs/GetFolderList?p=' + localStorage.getItem('HomePath') + '&root=1';
                    else {
                        isChildNode = true;
                        if (node.parent == '#') {
                            url = 'http://localhost/EasyNXConnectWebApi/api/Jobs/GetFolderList?p=' + localStorage.getItem('HomePath') + '\\' + node.text + '&root=2';
                        }
                        else {
                            url = 'http://localhost/EasyNXConnectWebApi/api/Jobs/GetFolderList?p=' + node.parent + '\\' + node.text + '&root=3';
                        }
                    }

                    $.ajax({
                        type: 'GET',
                        url: url,
                        async: false,
                        success: function (response:any, textStatus:any, xhr:any) {
                            if (response) {
                                if (response.status == "SUCCESS") {
                                    var treeNodeData = null;
                                    if (!isChildNode) {
                                        treeNodeData = response.data;
                                    }
                                    else {
                                        treeNodeData = response.data;
                                    }

                                    cb.call(this, treeNodeData);//this call will load the nodes onto the tree 
                                }
                                else {
                                    if (response.exit_code != null) {
                                        this.notification.openSnackBar(this.errorCodes[response.exit_code].msg,"","red-snackbar");
                                    }

                                }
                            }
                        }
                    })

                }
            }
        });

    $('#jstree_demo_divHome').on("check_node.jstree uncheck_node.jstree", (e:any, data:any)=> {
        var path = data.instance.get_path(data.node, '/');
        var parameter = this.rootfolder + "/" + path;
        //this.folderTargets.push({ "FolderName": data.node.text, "FolderTarget": path, "SiteTarget": null, "DocLibrarytarget": null });
    
        if (data.selected[0] == undefined) {
            var removeIndex = this.folderTargets.map(function (item:any) { return item.JobName; }).indexOf(data.node.text);
            for (var i = 0; i < this.folderTargets.length; i++) {
                if (this.folderTargets[i].JobName == data.node.text) {
                    if (removeIndex == 0) {
                        this.folderTargets.splice(removeIndex, 1);
                    }
                    else {
                        this.folderTargets.splice(removeIndex, i);
                    }
                    break;
                }
            }
        }
        else {
            this.folderTargets.push({ "JobName": data.node.text, "TargetFolderName": parameter, "TargetSiteName": null, "DocLibrarytarget": null });
            data.selected.length = 0;
        }
    });
    }
  }
  GetTargets()
  {
    if (this.FolderMapjobs == 'FolderJobs') {

        if (this.rootfolder == "" || this.rootfolder == null) {
            
            this.notification.openSnackBar("Please enter rootfolder","","warning-snackbar");
            return;
        }
        if (localStorage.getItem('HomePath') != null) {
            if (localStorage.getItem('HomePath') != this.rootfolder) {
                $("#jstree_demo_divHome").jstree("destroy");
            }
        }

        localStorage.setItem('HomePath', this.rootfolder);

        //lockUI();
        const jsTree = $("#jstree_demo_divHome").jstree({
            "plugins": ["checkbox", "search"],
            "checkbox": {
                "keep_selected_style": false,
                "three_state": false,
                "tie_selection": false, // for checking without selecting and selecting without checking
            },
            'core': {
                'data': function (node:any, cb:any) {
                    if (!node) return;

                    var isChildNode = false;
                    var url = "";

                    if (node.id === '#')
                        url = 'http://localhost/EasyNXConnectWebApi/api/Jobs/GetFolderList?p=' + localStorage.getItem('HomePath') + '&root=1';
                    else {
                        isChildNode = true;
                        if (node.parent == '#') {
                            url = 'http://localhost/EasyNXConnectWebApi/api/Jobs/GetFolderList?p=' + localStorage.getItem('HomePath') + '\\' + node.text + '&root=2';
                        }
                        else {
                            url = 'http://localhost/EasyNXConnectWebApi/api/Jobs/GetFolderList?p=' + node.parent + '\\' + node.text + '&root=3';
                        }
                    }

                    $.ajax({
                        type: 'GET',
                        url: url,
                        async: false,
                        success: function (response:any, textStatus:any, xhr:any) {
                            if (response) {
                                if (response.status == "SUCCESS") {
                                    var treeNodeData = null;
                                    if (!isChildNode) {
                                        treeNodeData = response.data;
                                    }
                                    else {
                                        treeNodeData = response.data;
                                    }

                                    this.HomeFolder = response.data;
                                    localStorage.setItem('GetHome', this.rootfolder)
                                    localStorage.setItem('homefolderlist', JSON.stringify(response.data))
                                    localStorage.setItem("hitlevel", '2');

                                    cb.call(this, treeNodeData);//this call will load the nodes onto the tree 
                                }
                                else {
                                    if (response.exit_code != null) {
                                        
                                        this.notification.openSnackBar(this.errorCodes[response.exit_code].msg,"","warning-snackbar");
                                    }

                                }
                            }
                        }
                    })

                }
            }
        });
    }

  
    $('#jstree_demo_divHome').on("check_node.jstree uncheck_node.jstree", (e:any, data:any)=> {
        var path = data.instance.get_path(data.node, '/');
        var parameter = this.rootfolder + "/" + path;
        //this.folderTargets.push({ "FolderName": data.node.text, "FolderTarget": path, "SiteTarget": null, "DocLibrarytarget": null });
    
        if (data.selected[0] == undefined) {
            var removeIndex = this.folderTargets.map(function (item:any) { return item.JobName; }).indexOf(data.node.text);
            for (var i = 0; i < this.folderTargets.length; i++) {
                if (this.folderTargets[i].JobName == data.node.text) {
                    if (removeIndex == 0) {
                        this.folderTargets.splice(removeIndex, 1);
                    }
                    else {
                        this.folderTargets.splice(removeIndex, i);
                    }
                    break;
                }
            }
        }
        else {
            this.folderTargets.push({ "JobName": data.node.text, "TargetFolderName": parameter, "TargetSiteName": null, "DocLibrarytarget": null });
            data.selected.length = 0;
        }
    });



}
//Email Targets

LoadEmail()
{
    this.EmailTargets = true;

                localStorage.removeItem('Map');
                localStorage.removeItem('MapDoc');
                localStorage.removeItem('mapMFiles');
                localStorage.removeItem('MapWorkflow');
                localStorage.removeItem('ODBMap');
                localStorage.removeItem('BoxMap');
                localStorage.removeItem('SPOMap');
                localStorage.removeItem('EgnyteMap');
                localStorage.removeItem('mapFax');
                localStorage.removeItem('mapCustom');
                localStorage.removeItem('DropBoxMap');

}

LoadFaxConnectors()
{
    this.FaxTargets = true;

    localStorage.removeItem('Map');
                localStorage.removeItem('MapDoc');
                localStorage.removeItem('mapMFiles');
                localStorage.removeItem('MapWorkflow');
                localStorage.removeItem('ODBMap');
                localStorage.removeItem('BoxMap');
                localStorage.removeItem('SPOMap');
                localStorage.removeItem('EgnyteMap');
                localStorage.removeItem('mapEmail');
                localStorage.removeItem('mapCustom');
                localStorage.removeItem('DropBoxMap');
                

}
uploadFaxFile(event: Event) {

    try {
        const input = window.document.getElementById("FaxFile")!;
        const element = event.currentTarget as HTMLInputElement;

         var faxList:any = [];
            if (element.files && element.files[0]) {
                var faxFile = element.files[0];
                var reader = new FileReader();
                reader.addEventListener('load', function (e:any) {
                    var textContent = e.target.result;
                    var arrContents = textContent.split('\r\n');
                    if (arrContents && arrContents.length > 0) {
                        for (var i = 0; i < arrContents.length; i++) {
                            var splitValues = arrContents[i].split(/,|\t/ig);
                            if (splitValues && splitValues.length == 2) {
                                  var faxData:any = {};
                                 var FaxMapTargets:any=[];
                                 faxData.Name = splitValues[0];
                                 faxData.FaxNumber = splitValues[1];
                                 
                                 if (splitValues.length >= 3)
                                 faxData.FaxDomain = splitValues[2];
                             if (splitValues.length >= 4)
                                 faxData.FaxBody = splitValues[3];
                                 faxList.push(faxData);
                            }
                        }
                    }
                    if (faxList && faxList.length > 0) {
                        FaxMapTargets = faxList;
                        localStorage.setItem('faxlist', JSON.stringify(FaxMapTargets)||'{}')
                        faxList = []
                        
                        // this.notification.openSnackBar(this.errorCodes["SUC026"].msg,"","success-snackbar");

                    }
                });
                reader.readAsText(faxFile);
            }
        this.FaxTargets = JSON.parse(localStorage.getItem('faxlist')||'{}')
    }
    catch (e) {
        alert("Exception Occurred : " + e);
    }
}
//FTP Targets

LoadFTP()
{
    const jsTree = $("#jstree_demo_divftp").jstree({
        "plugins": ["checkbox", "search"],
        "checkbox": {
            "keep_selected_style": false,
            "three_state": false,
            "tie_selection": false, // for checking without selecting and selecting without checking
        },
        'core': {
                'data': function (node:any, cb:any) {
                    var url = "";
                    var isChildNode = false;

                    if (node.id == '#')
                        url = 'http://localhost/EasyNXConnectWebApi/api/Jobs/GetFtpFolders?p=';
                    else
                    {
                        isChildNode = true;
                        url = 'http://localhost/EasyNXConnectWebApi/api/Jobs/GetFtpFolders?p=' + node.id;

                    }

                    console.log(url);
                
                // 'data': function (node:any) {
                //     return node;
                // },
                // "success": function (response:any,textStatus:any, xhr:any) {
                //     if (response) {
                //         if (!response.status) {
                //             let FtpFolder = response.data;
                //             return FtpFolder;
                //         }
                //         else if (response.status == "FAILED") {
                            
                //              this.notification.openSnackBar(this.errorCodes[response.Exit_code].msg,"","red-snackbar");

                //         }
                //     }


                // },

                $.ajax({
                    type: 'GET',
                    url: url,
                    async: true,
                    success: function (response:any, textStatus:any, xhr:any) {
                        if (response) {
                            if (response!=null) {
                                var treeNodeData = null;
                                if (!isChildNode) {
                                    treeNodeData = response;
                                }
                                else {
                                    treeNodeData = response;
                                }

                                cb.call(this, treeNodeData);//this call will load the nodes onto the tree 
                            }
                            else {
                                if (response.exit_code != null) {
                                    this.notification.openSnackBar(this.errorCodes[response.exit_code].msg,"","red-snackbar");
                                }
                            }
                        }
                    }
                })
            }
        }

    });
    //    .on('hover_node.jstree', function (e, data) {
    //    $("#" + data.node.id).prop('title', data.node.text);
    //});
    $('#jstree_demo_divftp').on("check_node.jstree uncheck_node.jstree", (e:any, data:any)=> {
        var path = data.instance.get_path(data.node, '/');
        //this.folderTargets.push({ "FolderName": data.node.text, "FolderTarget": path, "SiteTarget": null, "DocLibrarytarget": null });

        if (data.selected[0] == undefined) {
            var removeIndex = this.folderTargets.map(function (item:any) { return item.JobName; }).indexOf(data.node.text);
            for (var i = 0; i < this.folderTargets.length; i++) {
                if (this.folderTargets[i].JobName == data.node.text) {
                    if (removeIndex == 0) {
                        this.folderTargets.splice(removeIndex, 1);
                    }
                    else {
                        this.folderTargets.splice(removeIndex, i);
                    }
                    break;
                }
            }
        }
        else {
            this.folderTargets.push({ "JobName": data.node.text, "TargetFolderName": path, "TargetSiteName": null, "DocLibrarytarget": null });
            data.selected.length = 0;
        }
    });

    this.FTPTargets = true;

    localStorage.removeItem('Map');
    localStorage.removeItem('mapEmail');
    localStorage.removeItem('mapFax');
    localStorage.removeItem('MapDoc');
    localStorage.removeItem('mapMFiles');
    localStorage.removeItem('MapWorkflow');
    localStorage.removeItem('BoxMap');
    localStorage.removeItem('ODBMap');
    localStorage.removeItem('DropBoxMap');
}

uploadFile(event: Event) {

    try {
        const input = window.document.getElementById("myFile")!;
        const element = event.currentTarget as HTMLInputElement;

         var emailList:any = [];
            if (element.files && element.files[0]) {
                var myFile = element.files[0];
                var reader = new FileReader();
                reader.addEventListener('load', function (e:any) {
                    var textContent = e.target.result;
                    var arrContents = textContent.split('\r\n');
                    if (arrContents && arrContents.length > 0) {
                        for (var i = 0; i < arrContents.length; i++) {
                            var splitValues = arrContents[i].split(/,|\t/ig);
                            if (splitValues && splitValues.length == 2) {
                                  var emailData:any = {};
                                 var EmailMapTargets:any=[];
                                emailData.Name = splitValues[0];
                                emailData.Email = splitValues[1];
                                emailList.push(emailData);
                            }
                        }
                    }
                    if (emailList && emailList.length > 0) {
                        EmailMapTargets = emailList;
                        localStorage.setItem('emaillist', JSON.stringify(EmailMapTargets)||'{}')
                        emailList = []
                        
                        // this.notification.openSnackBar(this.errorCodes["SUC026"].msg,"","success-snackbar");

                    }
                });
                reader.readAsText(myFile);
            }
        this.EmailMapTargets = JSON.parse(localStorage.getItem('emaillist')||'{}')
    }
    catch (e) {
        alert("Exception Occurred : " + e);
    }
}
  CheckUncheckAllDoc(val1:any)
  {
    var count = 0;
    var val = "";
    for (var i = 0; i < this.Jobs.length; i++) {
        if (this.Jobs[i].Selected) {
            val = this.Jobs[i].Selected;
            this.templateok = false;
            var leng = count++;
            if (leng <= 0) {
                this.multipleedit = false;
            }
            else {
                this.multipleedit = true;
            }
        }

    }
    if (val == "") {
        this.editJobName = "";
    }
  }
  Templatejobmap()
  {
    for (var i = 0; i < this.Jobs.length; i++) {
        if (this.Jobs[i].Selected) {
            this.templateJobName = this.Jobs[i].JobName;
            this.templateclear = false;
        }
    }
  }
  clearjob()
  {
    this.templateJobName = "";
    this.templateclear = true;
  }
  JobEdit()
  {
    for (var i = 0; i < this.Jobs.length; i++) {
        if (this.Jobs[i].Selected) {
            this.editJobName = this.Jobs[i].JobName;
            this.editJobNameId = this.Jobs[i].Id;
        }
    }
    if (this.editJobName == undefined || this.editJobName == "") {
        this.notification.openSnackBar(this.errorCodes["WAR034"].msg,"","warning-snackbar");
        return;
    }
    else{
        this.editjobpopup="block";
    }

    let urldtl="api/Jobs/GetJobInfo?jobName="+this.editJobName;
    this.service.getAPI(urldtl).then(resp=>{
        this.editJobName = "";
        var val = resp.ButtonColor;
        var buttoncolor = val.toLowerCase();
        this.Jobname = resp.JobName;
        this.Description = resp.Description;
        this.buttoncolor = buttoncolor;
        this.ProfileName = resp.ProfileName;
        if (resp.metaDataEntry == false) {
            this.MetaDataName = "";
            this.MetaDataType = "";
        }
        else {
            this.MetaDataName = resp.MetaDataName;
            this.MetaDataType = resp.MetaDataType;
        }
});

    let url="api/Jobs/GetJobprofiles";
    this.service.getAPI(url).then(resp=>{
this.IPprofiles=resp;
    });
}
EditJob()
{
    let url="api/Jobs/UpdateJob";
    let postData = {
        Id: this.editJobNameId,
        JobName: this.Jobname,
        Description: this.Description,
        MetaDataName: this.MetaDataName,
        MetaDataType: this.MetaDataType,
        ButtonColor: this.buttoncolor,
        ProfileName: this.ProfileName

    }
    this.ngxLoader.start();
     this.service.post(url,postData).subscribe(resp => {
      if (resp==true) 
        {
            this.editjobpopup="none";
            this.notification.openSnackBar(this.errorCodes["SUC010"].msg,"","success-snackbar");
          this.ngxLoader.stop();
      }
      else {
        this.notification.openSnackBar(this.errorCodes["WAR011"].msg,"","warning-snackbar");
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
}
JobGroupscreate()
{
    this.router.navigate(['/CreateJobGroups'])
}

CheckUncheckAllCabinate(val:any)
{
    this.Filecabinates.every(function(item:any) {
        return item.Selected == true;
    });      
}
CheckUncheckAllbuckets()
{
    this.GetBuckets.every(function(item:any) {
        return item.Selected == true;
    });  
}
CheckUncheckAllEmail()
{
    this.EmailMapTargets.every(function(item:any) {
        return item.Selected == true;
    });  
}
CheckUncheckAllFax()
{
    this.FaxMapTargets.every(function(item:any) {
        return item.Selected == true;
    });  
}
CheckUncheckAllJobGroup(val:any)
{
    this.Jobgroups.every(function(item:any) {
    return item.Selected == true;
        
});
   this.Check(); 
}
Check()
{
    var count = 0;
                var val = "";

                for (var i = 0; i < this.Jobgroups.length; i++) {
                    if (this.Jobgroups[i].Selected) {
                        val = this.Jobgroups[i].Selected;

                        var JobGroupName = this.Jobgroups[i].JobGroupName;
                        var JobGroupId = this.Jobgroups[i].Id;
                        this.JobGroup = JobGroupName;
                        this.JobgroupId = JobGroupId;
                        var leng = count++;
                        if (leng <= 0) {
                            this.multipleeditjobgroup = false;
                        }
                        else {
                            this.multipleeditjobgroup = true;
                        }
                    }
                }
   
}
JobGroupEdit()
{
    if (this.JobGroup == undefined || this.JobGroup == "" || this.JobGroup == null) {
        
        this.notification.openSnackBar(this.errorCodes["WAR039"].msg,"","warning-snackbar");

        return;
    }
    // ScopeValueService.store('CTRJobmanagementController', this);
    // $state.go('app.EditJobGroups');
    this.dataStore.Jobgroup={JobGroup:this.JobGroup,JobgroupId:this.JobgroupId}

    this.router.navigate(['/EditJobGroups']);
}

 DeleteJobGrouplist:any = [];

JobGroupDelete()
{
    this.ngxLoader.start();
    if (this.Jobgroups != null) {
        for (var i = 0; i < this.Jobgroups.length; i++) {
            if (this.Jobgroups[i].Selected) {
                var GroupName = this.Jobgroups[i].JobGroupName;
                this.DeleteJobGrouplist.push(GroupName);
            }
        }
    }
    if (this.DeleteJobGrouplist.length <= 0) {
        
        this.notification.openSnackBar(this.errorCodes["WAR038"].msg,"","warning-snackbar");

        this.ngxLoader.stop();
        return ;
    }

    let url="api/Jobs/DeleteJobGroup";
    let postData={
        DeleteJobGroups: this.DeleteJobGrouplist
    }
    this.service.post(url,postData).subscribe(resp=>
        {
if(resp==true)
{
    this.JobGroup = "";
    this.notification.openSnackBar(this.errorCodes["SUC014"].msg,"","success-snackbar");
    this.LoadJobGroups();
    this.ShowTargets();
    this.ngxLoader.stop();

}
else{
    this.notification.openSnackBar(this.errorCodes["ERR005"].msg,"","red-snackbar");
    this.ngxLoader.stop();

}
        },
        (error)=>
        {
            this.notification.openSnackBar(error.message,"","red-snackbar");
            this.ngxLoader.stop();
        });
        this.ngxLoader.stop();

}

 ShowTargets() {
    this.cabinates = false;
    this.Buckets = false;
    this.EmailTargets = false;
    this.FaxTargets = false;
    this.mfiles = false;
    this.Nintex = false;
    this.BoxTargets = false;
    this.DropBoxTargets = false;
    this.ODBTargets = false;
    this.SPOTargets = false;
    this.EgnyteTargets = false;
    this.HomeFolderTargets = false;
    this.AzureBlobsTargets = false;
    this.FTPTargets = false;
    this.CustomTarget = false;
    this.UIpathTargets = false;

    if (this.Mapbucket == 'Buckets') {
        this.Buckets = true;
    }
    else if (this.MapEmail == 'EmailMap') {
        this.EmailTargets = true;
    }
    else if (this.MapFax == 'FaxMap') {
        this.FaxTargets = true;
    }
    else if (this.MapMFiles == 'M_Files') {
        this.mfiles = true;
    }
    else if (this.MapWorkflow == 'Workflow') {
        this.Nintex = true;
    }
    else if (this.BoxMapjobs == 'BoxJobs') {
        this.BoxTargets = true;
    }
    else if (this.DropBoxMapjobs == 'DropBoxJobs') {
        this.DropBoxTargets = true;
    }
    else if (this.ODBMapjobs == 'ODBJobs') {
        this.ODBTargets = true;
    }
    else if (this.SPOMapjobs == 'SPOJobs') {
        this.SPOTargets = true;
    }
    else if (this.EgnyteMapjobs == 'EgnyteJobs') {
        this.EgnyteTargets = true;
    }
    else if (this.AzureBlobMapjobs == 'AzureBlobJobs') {
        this.AzureBlobsTargets = true;
    }
    else if (this.FolderMapjobs == 'FolderJobs') {
        this.HomeFolderTargets = true;
    }
    else if (this.FTPMapjobs == 'FTPJobs') {
        this.FTPTargets = true;
    }
    else if (this.MapCustom == 'CustomTarget') {
        this.CustomTarget = true;
    }
    else if (this.MapUIPath == 'UIPathJobs') {
        this.UIpathTargets = true;
    }
    else {
        this.cabinates = true;
    }

}
EditClose()
{
    this.editjobpopup="none";
}
JobRefresh()
{
    this.LoadJobs();
    this.ShowTargets();
}

 DeleteJoblist:any = [];

DeleteJob()
{
    this.ngxLoader.start();
    for (var i = 0; i < this.Jobs.length; i++) {
        if (this.Jobs[i].Selected) {
            var JobName = this.Jobs[i].JobName;
            this.DeleteJoblist.push(JobName);
        }
    }
    if (this.DeleteJoblist.length <= 0) {
        
        this.notification.openSnackBar(this.errorCodes["WAR034"].msg,"","warning-snackbar");

        return;
    }
    
    let url="api/Jobs/DeleteJob";
    let postData={
        DeleteJobs: this.DeleteJoblist
    }
    this.service.post(url,postData).subscribe(resp=>
        {
if(resp==true)
{
    this.DeleteJoblist = [];
    this.LoadJobs();
    this.notification.openSnackBar(this.errorCodes["SUC009"].msg,"","success-snackbar");
    this.ShowTargets();
this.ngxLoader.stop();
}
else{
    this.notification.openSnackBar(this.errorCodes["ERR004"].msg,"","red-snackbar");
this.ngxLoader.stop();
this.DeleteJoblist = [];

}
        },(error)=>
        {

        });
}

JobGroupRefresh()
{
    this.LoadJobGroups();
    this.ShowTargets();

}
}


