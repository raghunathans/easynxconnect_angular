import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { environment } from 'src/environments/environment';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

@Component({
  selector: 'app-drop-box',
  templateUrl: './drop-box.component.html',
  styleUrls: ['./drop-box.component.scss']
})
export class DropBoxComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
   {
     return !this.isDirty
   }
   isDirty=false;

  servicelogin:any;
  note:boolean=false;
  enableforservice:boolean=false;
  AppDetails:boolean=true;
  AppSettings:boolean = true; 
  Accesstoken:any;
  appid:any;
  appsecret:any;
  redirecturl:any;
  Homefolder:any=true;
  Specificfolder:any;
  RootFolderId:any;
  Remind:any;
  RemindDays:any;
  InformUser:any;
  InformAdmin:any;
  AdminMail:any;
  folder:boolean=false;
  logintypeval:any;
  appidvalue:any;
  appsecretvalue:any;
  redirecturlvalue:any;
  logintypevalue:any;
  Accesstokenvalue:any;
  AdminEmail:boolean=false;
  Reminder:boolean=false;
  selectedFile:any;
  arrayBuffer:any;
  result:any;
  EmailSubject:string="";
  EmailBody:string="";
  errorCodes:any = {};
  username:any;
  target:any;
  url:any
ListOfUser:any;
userlist:any;
txtuser:any;
  displayStyle = "none";

  constructor(private http: HttpClient,public translate: TranslateService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) { }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');

    this.translate.use(returnUrl);
    this.url = environment.hostUrl;

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.LoadDropBoxConnectorSettings();

  }
  LoadDropBoxConnectorSettings()
  {
    let url="api/DropBoxConnectorSettings/LoadDropBoxControllerSetting";
    let postData={
      UserFolder:""
    }
     this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if(resp!=null)
      {
        this.Homefolder = resp.UserFolder;
        this.Specificfolder = resp.SpecifcFolder;
        this.RootFolderId = resp.RootFolderId;
        this.Remind = resp.Remind;
        this.RemindDays = resp.RemindDays;
        this.InformUser = resp.InformUser;
        this.InformAdmin = resp.InformAdmin;
        this.AdminMail = resp.AdminMail;
        this.appid = resp.AppID;
        this.appsecret = resp.AppSecret;
        this.redirecturl = resp.RedirectURL;
        this.logintypeval = resp.Logintype;
        this.Accesstoken = resp.AccessToken;
        
        this.appidvalue = resp.AppID;
        this.appsecretvalue = resp.AppSecret;
        this.redirecturlvalue = resp.RedirectURL;
        this.logintypevalue = resp.Logintype;
        this.Accesstokenvalue = this.Accesstoken;

        if (this.logintypeval == "2") {
          this.servicelogin = true;
          this.enableforservice = true;
          this.AppDetails = false;
      }
      else {
          this.servicelogin = false;
          this.enableforservice = false;
          this.AppDetails = true;
      }
      if (this.Specificfolder == false) {
        this.folder = true;
    }
    if (this.InformAdmin == false) {
        this.AdminEmail = true;
    }
    if (this.Remind == true) {
        this.Reminder = false;
    }
    else {
        this.Reminder = true;
    }
      }
      this.ngxLoader.stop();
    },(error)=>{
      this.ngxLoader.stop();
    
    });
    }

    DropBoxConnector(val:any)
    {
   if (val == '1') {
     this.Homefolder = true;
     this.Specificfolder = false;
      this.folder = true;
   }
   if (val == '2') {
     this.Homefolder = false;
     this.Specificfolder = true;
      this.folder = false;
   }
    }
    LogReminderAdmin(val:any)
    {
     if (val == true) {
       this.AdminEmail = false;
   } else {
       this.AdminEmail = true;
   }
    }
    LogReminder(val:any)
    {
     if (val == true) {
       this.Reminder = false;
       this.RemindDays = "5";
   }
   else {
       this.Reminder = true;
       this.AdminEmail = true;
       this.RemindDays = false;
       this.InformUser = false;
       this.InformAdmin = false;
   }
    }

    servicechange()
    {
      this.note=false;
      if (this.servicelogin == true) {
         this.logintypeval = "2";
        this.enableforservice = true;
         this.AppDetails = false;
    }
    else {
         this.logintypeval = "1";
        this.enableforservice = false;
         this.AppDetails = true;
         this.AppSettings = true; 
  
    } 
    }
    EditSettings()
    {

      this.AppSettings = false;  
      this.note = true;
    }
    ResetSettings()
    {
      let url="api/DropBoxConnectorSettings/ResetSetting";
      let postData = {
        ResetAppID: this.appid,
        ResetAppSecret: this.appsecret,
        ResetRedirectURL: this.redirecturl
      }
      this.ngxLoader.start();
      this.service.post(url,postData).subscribe(resp => {
        this.appid = resp.ResetAppID;
        this.appsecret = resp.ResetAppSecret;
        this.redirecturl = resp.ResetRedirectURL;  
        this.ngxLoader.stop();   
      },(error)=>
      {
        this.notification.openSnackBar(error.message,"","red-snackbar");
        this.ngxLoader.stop();   
      })
    
    }
    SaveDropBoxConnector()
    {
      this.note = false;
      if (this.Specificfolder == true) {
          if (this.RootFolderId == null || this.RootFolderId == "") {
            this.notification.openSnackBar(this.errorCodes["WAR007"].msg,"","warning-snackbar");
              return;
          }
      }
      if (this.InformAdmin == true) {
          if (this.AdminMail == null || this.AdminMail == "") {
              this.notification.openSnackBar(this.errorCodes["WAR008"].msg,"","warning-snackbar");
              return;
          }
      }

      if ((this.appid == null || this.appid == "") || (this.appsecret == null || this.appsecret == "") || (this.redirecturl == null || this.redirecturl == "")) {
       
          this.notification.openSnackBar(this.errorCodes["WAR066"].msg,"","warning-snackbar");
          return;
      }

      let url="api/DropBoxConnectorSettings/SaveSetting";
let postData = {
  UserFolder: this.Homefolder,
  SpecifcFolder: this.Specificfolder,
  RootFolderId: this.RootFolderId,
  Remind: this.Remind,
  RemindDays: this.RemindDays,
  InformUser: this.InformUser,
  InformAdmin: this.InformAdmin,
  AdminMail: this.AdminMail,
  AppID: this.appid,
  AppSecret: this.appsecret,
  RedirectURL: this.redirecturl,
  ProviderCode: "DROPBOX",
  Logintype: this.logintypeval,
  AccessToken: this.Accesstoken
}

this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp => {
if(resp==true)
{

  this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
  this.LoadDropBoxConnectorSettings();
  this.ngxLoader.stop();
}
else
{
  this.notification.openSnackBar(this.errorCodes["WAR003"].msg,"","warning-snackbar");
    this.ngxLoader.stop();
}
},(error)=>
{
  this.notification.openSnackBar(error.message,"","red-snackbar");
  this.ngxLoader.stop();
});

    }
    SaveDropBoxEmail()
    {
      let url="api/DropBoxConnectorSettings/SaveEmailSetting";
      let postData = {
        EmailSubject: this.EmailSubject,
        EmailBody: this.EmailBody
      }
      this.ngxLoader.start();
       this.service.post(url,postData).subscribe(resp => {
        if (resp==true) 
          {
            this.EmailSubject = resp.EmailSubject;
            this.EmailBody = resp.EmailBody;
            this.ngxLoader.stop();
        }
        else {
          this.notification.openSnackBar(this.errorCodes["WAR003"].msg,"","warning-snackbar");
          this.ngxLoader.stop();
      }
      },(error)=>
      {
        this.notification.openSnackBar(error.message,"","red-snackbar");
        this.ngxLoader.stop();
      });
    
    }
      EmailOption()
      {
       let url="api/DropBoxConnectorSettings/LoadDropBoxControllerSetting";
       let postData={
         EmailSubject: this.EmailSubject,
         EmailBody: this.EmailBody  }
        this.ngxLoader.start();
        this.service.post(url,postData).subscribe(resp => {
         if ((resp.EmailSubject != "") || (resp.EmailBody != "")) 
           {
             this.EmailSubject = resp.EmailSubject;
             this.EmailBody = resp.EmailBody;
             this.ngxLoader.stop();
         }
         else {
           this.EmailSubject = this.errorCodes["RES003"].res;
           this.EmailBody = this.errorCodes["RES004"].res;
           this.ngxLoader.stop();
       }
       },(error)=>
       {
         this.notification.openSnackBar(error.message,"","red-snackbar");
         this.ngxLoader.stop();
       });
     
      }
    
      DropBoxMapJobs()
        {
          if ((this.appid != this.appidvalue) || (this.appsecret != this.appsecretvalue) || (this.redirecturl != this.redirecturlvalue)
          || (this.logintypeval != this.logintypevalue)) {
          this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");
          return;
      }
      localStorage.removeItem('MapDoc');
                localStorage.removeItem('mapEmail');
                localStorage.removeItem('ODBMap');
                localStorage.removeItem('SPOMap');
                localStorage.removeItem("AzureBlobMap");
                localStorage.removeItem('FTPMap');
                localStorage.removeItem('FolderMap');
                localStorage.removeItem('EgnyteMap');
                localStorage.removeItem('Map');
                localStorage.removeItem("mapMFiles");
                localStorage.removeItem("UIPathMap");
                localStorage.removeItem("BoxMap");
                localStorage.setItem("DropBoxMap", "DropBoxJobs");
                this.router.navigate(['/JobManagement']);
        }
  TestConnector()
  {
    this.displayStyle = "block";
    if ((this.appid != this.appidvalue) || (this.appsecret != this.appsecretvalue) || (this.redirecturl != this.redirecturlvalue)
    || (this.logintypeval != this.logintypevalue)) {
    this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");
    return;
}
var idval = -1;
                switch (localStorage.getItem("AuthType")) {
                    case "LocalDirectory":
                        idval = 0;
                        break;
                    case "ActiveDirectory":
                        idval = 1;
                        break;
                    case "Docuware":
                        idval = 2;
                        break;
                    case "Egnyte":
                        idval = 3;
                        break;
                    case "MFiles":
                        idval = 4;
                        break;
                    case "CustomTarget":
                        idval = 99;
                        break;
                }
                let url=this.url+"api/User/GetLocalUsersByAuthId"
let postData={
  AuthId:idval
}
this.http.get(url,{params:postData}).subscribe(resp=>{
if(resp!=null)
{
  this.ListOfUser=resp
  if(this.ListOfUser.length>0)
{
  this.ListOfUser=resp;
  this.userlist = true;
  this.txtuser=false;
}
else{ 
  this.userlist = false;
  this.txtuser=true;
}
}
else {
  this.userlist = false;
  this.txtuser = true;
}
  });
  }
  closePopup() {
    this.displayStyle = "none";
  }
  getUser(val:any)
  {
    let userId = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;
    this.username=userId;
  }
  Test(username:any,target:any)
{
  if (target == undefined || target == null) {
    target = "";
}
let url="api/User/TestConnector";
let postData={
  ProviderCode: "DROPBOX",
                    UserName: this.username,
                    Arguments: "-t=" + '"' + target + '"' 
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
  {
if(resp.Status=='SUCCESS')
{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
else{
  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
  this.ngxLoader.stop();
  this.displayStyle = "none";

}
  },(error)=>
  {
    this.notification.openSnackBar(error.message,"","red-snackbar");
    this.ngxLoader.stop();
  });
}
}
