import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SharePointOnlineComponent } from './share-point-online.component';

describe('SharePointOnlineComponent', () => {
  let component: SharePointOnlineComponent;
  let fixture: ComponentFixture<SharePointOnlineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SharePointOnlineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SharePointOnlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
