import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { environment } from 'src/environments/environment';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

@Component({
  selector: 'app-share-point-online',
  templateUrl: './share-point-online.component.html',
  styleUrls: ['./share-point-online.component.scss']
})
export class SharePointOnlineComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
  {
    return !this.isDirty
  }
  isDirty=false;

  servicelogin:any;
  AppSettings:boolean=true;
  appid:string="";
  appsecret:string="";
  redirecturl:string="";
  spousername:string="";
  spopassword:string="";
  tokencreatedon:string="";
  Homefolder:boolean=true;
  note:any;
  enableforservice:any;
  Hideredirectforserviceenable:any;
  defaultsite:any;
  sitename:string="";
  defaultlibrary:any;
  librarylist:any;
  Specificfolder:any;
  RootFolderId:any;
  folder:any;
  AdminEmail:any;
  AdminMail:string="";
  InformAdmin:any;
  Reminder:any;
  InformUser:any;
  RemindDays:any;
  Remind:any;
  appidvalue:any;
  appsecretvalue:any;
  redirecturlvalue:any;
  EmailSubject:string="";
  EmailBody:string="";
  logintypeval:any;
  spousernamevalue:string="";
  spopasswordvalue:string="";
  logintypevalue:any;
  errorCodes:any = {};
  testusername:any;
  target:any;
  sitepath:any;
  doclibrary:any;
  url:any
ListOfUser:any;
userlist:any;
txtuser:any;
  displayStyle = "none";


  constructor(private http: HttpClient,public translate: TranslateService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) { }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');

    this.translate.use(returnUrl);
    this.url = environment.hostUrl;

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.LoadSPOConnectorSettings();

  }
  LoadSPOConnectorSettings()
  {
    let url="api/SharePointOnlineSettings/LoadSPOControllerSetting";
    let postData={
      SiteName: ""
    }
     this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if(resp!=null)
      {
        this.sitename = resp.SiteName;
                    this.Homefolder = resp.UserFolder;
                    this.Specificfolder = resp.SpecifcFolder;
                    this.RootFolderId = resp.RootFolderId;
                    this.Remind = resp.Remind;
                    this.RemindDays = resp.RemindDays;
                    this.InformUser = resp.InformUser;
                    this.InformAdmin = resp.InformAdmin;
                    this.AdminMail = resp.AdminMail;
                    this.EmailSubject = resp.EmailSubject;
                    this.EmailBody = resp.EmailBody;
                    this.defaultlibrary = resp.DefaultLibrary;
                    this.appid = resp.AppID;
                    this.appsecret = resp.AppSecret;
                    this.redirecturl = resp.RedirectURL;
                    this.spousername = resp.Username;
                    this.spopassword = resp.Password;
                    this.logintypeval = resp.Logintype;
                    this.tokencreatedon = resp.AccessTokenCreatedOn;

                    this.appidvalue = resp.AppID;
                    this.appsecretvalue = resp.AppSecret;
                    this.redirecturlvalue = resp.RedirectURL;
                    this.spousernamevalue = resp.Username;
                    this.spopasswordvalue = resp.Password;
                    this.logintypevalue = resp.Logintype;

                    if (this.logintypeval == "2") {
                        this.servicelogin = true;
                        this.enableforservice = true;
                        this.Hideredirectforserviceenable = false;
                    }
                    else {
                        this.servicelogin = false;
                        this.enableforservice = false;
                        this.Hideredirectforserviceenable = true;
                    }
                    if ((resp.EmailSubject != "") || (resp.EmailBody != "")) {
                        this.EmailSubject = resp.EmailSubject;
                        this.EmailBody = resp.EmailBody;
                    }
                    else {
                        this.EmailSubject = this.errorCodes['RES003'].res;
                        this.EmailBody = this.errorCodes['RES004'].res;
                    }
                    if (this.InformAdmin == false) {
                        this.AdminEmail = true;
                    }
                    if (this.Specificfolder == false) {
                        this.folder = true;
                    }
                    if (this.Remind == true) {
                        this.Reminder = false;
                    }
                    else {
                        this.Reminder = true;
                    }
                    let url="api/SharePointOnlineSettings/GetDocumentLibraries";
    let postData={
      SiteName: this.sitename
    }
     this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if(resp.Status=='SUCCESS')
      {
        this.librarylist = resp.Data;
        this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");
        this.ngxLoader.stop();

                }
                else{
                  this.ngxLoader.stop();

return;
                }
    
    });
                
      this.ngxLoader.stop();
              }
    },(error)=>{
      this.ngxLoader.stop();
    
    });
  }
  SPOConnector(val:any)
  {
    if (val == '1') {
      this.Homefolder = true;
      this.Specificfolder = false;
      this.folder = true;
  }
  if (val == '2') {
      this.Homefolder = false;
      this.Specificfolder = true;
      this.folder = false;
  }
  }
  Loadlibrary()
  {
    let url="api/SharePointOnlineSettings/GetDocumentLibraries";
    let postData={
      SitePath: this.sitename
    }
     this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if(resp.Status=='SUCCESS')
      {
        this.librarylist = resp.Data;
        this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");

                }
                else{
                  this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
return;
                }
      this.ngxLoader.stop();
    },(error)=>{
      this.ngxLoader.stop();
    
    });
    
  }
  Servicechange(val:any)
  {
    this.note = false;
    if (this.servicelogin == true) {
        this.logintypeval = "2";
        this.enableforservice = true;
        this.Hideredirectforserviceenable = false;
    }
    else {
        this.logintypeval = "1";
        this.enableforservice = false;
        this.Hideredirectforserviceenable = true;
        this.AppSettings = true; 
    }
  }
  EditSettings()
  {
    this.AppSettings = false;
    this.note = true;
  }
  ResetSettings()
  {
    let url="api/SharePointOnlineSettings/ResetSetting";
    let postData = {
      ResetAppID: this.appid,
                    ResetAppSecret: this.appsecret,
                    ResetRedirectURL: this.redirecturl
    }
    this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      this.appid = resp.ResetAppID;
                    this.appsecret = resp.ResetAppSecret;
                    this.redirecturl = resp.ResetRedirectURL;
                         this.ngxLoader.stop();   
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();   
    })
  
  }
  LogReminder(val:any)
  {
    if (val == true) {
      this.Reminder = false;
      this.RemindDays = "5";
  }
  else {
      this.Reminder = true;
      this.AdminEmail = true;
      this.RemindDays = false;
      this.InformUser = false;
      this.InformAdmin = false;
  }
  }
  LogReminderAdmin(val:any)
  {
    if (val == true) {
      this.AdminEmail = false;
  } else {
      this.AdminEmail = true;
  }
  }
  GetAccessToken()
  {
    let url="api/WebLogin/GetAccessToken";
    let postData = {
      ProviderCode: "SPO",
                    Logintype: this.logintypeval,
                    AppID: this.appid,
                    AppSecret: this.appsecret,
                    Username: this.spousername,
                    Password: this.spopassword
    }
    this.ngxLoader.start();
     this.service.post(url,postData).subscribe(resp => {
      if (resp!=null) 
        {
          this.ngxLoader.stop();
          this.tokencreatedon = resp;

      }
      else {
        this.notification.openSnackBar(this.errorCodes["WAR056"].msg,"","warning-snackbar");
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }

  SaveSharepointConnector()
  {
    this.isDirty=false;
    this.note = false;
                if (this.servicelogin == true) {
                    if ((this.appid == null || this.appid == "") || (this.appsecret == null || this.appsecret == "") || (this.spousername == null || this.spousername == "")
                        || (this.spopassword == null || this.spopassword == "")) {
                        
                        this.notification.openSnackBar(this.errorCodes["WAR066"].msg,"","warning-snackbar");
                        return;
                    }

                }
                else {
                    if ((this.appid == null || this.appid == "") || (this.appsecret == null || this.appsecret == "") || (this.redirecturl == null || this.redirecturl == "")) {
                        
                        this.notification.openSnackBar(this.errorCodes["WAR066"].msg,"","warning-snackbar");
                        return;
                    }
                }
                if (this.Specificfolder == true) {
                    if (this.RootFolderId == null || this.RootFolderId == "") {                        
                        this.notification.openSnackBar(this.errorCodes["WAR007"].msg,"","warning-snackbar");
                        return;
                    }
                }
                if (this.InformAdmin == true) {
                    if (this.AdminMail == null || this.AdminMail == "") {
                        
                        this.notification.openSnackBar(this.errorCodes["WAR008"].msg,"","warning-snackbar");
                        return;
                    }
                }
                let url="api/SharePointOnlineSettings/SaveSetting";
                let postData = {
                  SiteName: this.sitename,
                    UserFolder: this.Homefolder,
                    SpecifcFolder: this.Specificfolder,
                    RootFolderId: this.RootFolderId,
                    Remind: this.Remind,
                    RemindDays: this.RemindDays,
                    InformUser: this.InformUser,
                    InformAdmin: this.InformAdmin,
                    AdminMail: this.AdminMail,
                    DefaultLibrary: this.defaultlibrary,
                    AppID: this.appid,
                    AppSecret: this.appsecret,
                    RedirectURL: this.redirecturl,
                    Username: this.spousername,
                    Password: this.spopassword,
                    ProviderCode: "SPO",
                    Logintype: this.logintypeval
   
                }
                this.ngxLoader.start();
                 this.service.post(url,postData).subscribe(resp => {
                  if (resp==true) 
                    {
                      this.notification.openSnackBar(this.errorCodes["SUC002"].msg,"","success-snackbar");
                      this.ngxLoader.stop();
              this.LoadSPOConnectorSettings();
                  }
                  else {
                    this.notification.openSnackBar(this.errorCodes["WAR003"].msg,"","warning-snackbar");
                    this.ngxLoader.stop();
                }
                },(error)=>
                {
                  this.notification.openSnackBar(error.message,"","red-snackbar");
                  this.ngxLoader.stop();
                });
  }
  EmailOption()
  {
    let url="api/SharePointOnlineSettings/LoadSPOControllerSetting";
    let postData = {
      EmailSubject: this.EmailSubject,
      EmailBody: this.EmailBody
    }
    this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp => {
      if ((resp.EmailSubject != "") || (resp.EmailBody != "")) 
        {
          this.EmailSubject = resp.EmailSubject;
          this.EmailBody = resp.EmailBody;
          this.ngxLoader.stop();
      }
      else {
        this.EmailSubject = this.errorCodes["RES003"].res;
        this.EmailBody = this.errorCodes["RES004"].res;
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
  SaveEmailOption()
  {
    let url="api/SharePointOnlineSettings/SaveEmailSetting";
    let postData = {
      EmailSubject: this.EmailSubject,
      EmailBody: this.EmailBody
    }
    this.ngxLoader.start();
     this.service.post(url,postData).subscribe(resp => {
      if (resp==true) 
        {
          this.EmailSubject = resp.EmailSubject;
          this.EmailBody = resp.EmailBody;
          this.ngxLoader.stop();
      }
      else {
        this.ngxLoader.stop();
    }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  
  }

  SPOMapJobs()
  {
    if ((this.appid != this.appidvalue) || (this.appsecret != this.appsecretvalue) || (this.redirecturl != this.redirecturlvalue)
                    || (this.spousername != this.spousernamevalue) || (this.spopassword != this.spopasswordvalue) || (this.logintypeval != this.logintypevalue)) {                   
                    this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");
                    return;
                }
                //ScopeValueService.store('CTRSharepointConnectorController', this);
                localStorage.removeItem('Map');
                localStorage.removeItem('MapDoc');
                localStorage.removeItem('mapEmail');
                localStorage.removeItem('BoxMap');
                localStorage.removeItem('ODBMap');
                localStorage.removeItem('EgnyteMap');
                localStorage.removeItem("mapMFiles");
                localStorage.removeItem('FTPMap');
                localStorage.removeItem('FolderMap');
                localStorage.removeItem("AzureBlobMap");
                localStorage.removeItem("UIPathMap");
                localStorage.removeItem("DropBoxMap");

                localStorage.setItem("SPOMap", "SPOJobs");
                this.router.navigate(['/JobManagement']);

  }
  TestConnector()
  {
    if ((this.appid != this.appidvalue) || (this.appsecret != this.appsecretvalue) || (this.redirecturl != this.redirecturlvalue)
                    || (this.spousername != this.spousernamevalue) || (this.spopassword != this.spopasswordvalue) || (this.logintypeval != this.logintypevalue)) {
                    
                    this.notification.openSnackBar(this.errorCodes["WAR085"].msg,"","warning-snackbar");

                    return;
                }

                this.displayStyle = "block";
  var idval = -1;
  switch (localStorage.getItem("AuthType")) {
      case "LocalDirectory":
          idval = 0;
          break;
      case "ActiveDirectory":
          idval = 1;
          break;
      case "Docuware":
          idval = 2;
          break;
      case "Egnyte":
          idval = 3;
          break;
      case "MFiles":
          idval = 4;
          break;
      case "CustomTarget":
          idval = 99;
          break;
  }
  let url=this.url+"api/User/GetLocalUsersByAuthId"
  let postData={
    AuthId:idval
  }
  this.http.get(url,{params:postData}).subscribe(resp=>{
  if(resp!=null)
  {
    this.ListOfUser=resp
    if(this.ListOfUser.length>0)
  {
    this.ListOfUser=resp;
    this.userlist = true;
    this.txtuser=false;
  }
  else{ 
    this.userlist = false;
    this.txtuser=true;
  }
  }
  else {
    this.userlist = false;
    this.txtuser = true;
  }
    });
  
  }
  closePopup() {
    this.displayStyle = "none";
  }
  getUser(val:any)
  {
    let userId = (<HTMLInputElement>document.getElementById('userIdFirstWay')).value;
    this.testusername=userId;
  }
  Test(username:any, target:any, site:any, docs:any)
  {
    if (target == undefined || target == null) {
      target = "";
  }
  if (site == undefined || site == null) {
      site = "";
  }
  if (docs == undefined || docs == null) {
      docs = "";
  }
  let url="api/User/TestConnector";
  let postData={
    ProviderCode: "SPO",
                    UserName:username,
                    Arguments: "-t=" + '"' + target + '"'+" " + "site=" + '"' + site + '"'+ " " + "-lib=" + '"' + docs + '"'

  }
  this.ngxLoader.start();
  this.service.post(url,postData).subscribe(resp=>
    {
  if(resp.Status=='SUCCESS')
  {
    this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","success-snackbar");
    this.ngxLoader.stop();
    this.displayStyle = "none";
  
  }
  else{
    this.notification.openSnackBar(this.errorCodes[resp.ExitCode].msg,"","warning-snackbar");
    this.ngxLoader.stop();
    this.displayStyle = "none";
  
  }
    },(error)=>
    {
      this.notification.openSnackBar(error.message,"","red-snackbar");
      this.ngxLoader.stop();
    });
  }
}
