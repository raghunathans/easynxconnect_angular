import { Component, OnInit, ViewChild, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
// import { TreeNode } from 'angular-tree-component';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import { Observable, of } from 'rxjs';
import '@angular/localize/init';
import { event, post } from 'jquery';
import { StoredataService } from 'src/app/storedata.service';
import { Pipe, PipeTransform } from '@angular/core';


declare var $: any;


// interface FoodNode {
//   label:string,
//   NodeType:string,
//   JGID:string,
//   Level:string,
//   items?: FoodNode[],
//   text:string
// }

interface FoodNode {
  text:string,
  items?: FoodNode[]
}

@Component({
  selector: 'app-job-group-management',
  templateUrl: './job-group-management.component.html',
  styleUrls: ['./job-group-management.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class JobGroupManagementComponent implements OnInit {
  
  groupname:any;
  description:any;
  Jobgroupname:any;

  JGID:any=1;
  errorCodes:any = {};
  Repository:any=[];
  Users:any=[];
nodes:any=[];
  tree: any;
  ENXJobGroup:any = [];
 SelectedNode:any;
  treedata_avm = [];
  searchtxt:any;
my_data = this.treedata_avm;
  constructor(public translate: TranslateService,public dataStore:StoredataService,private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private ErrorCodes:ErrorCodesService,private notification:NotificationService) {
  }

  ngOnInit(): void {
    let returnUrl:any = localStorage.getItem('PreferredLanguage');

    this.translate.use(returnUrl);

    this.errorCodes=this.ErrorCodes.errormsgs();
    this.GetJobs();
    this.GetUsers();
        // this.getParentNodes();
        this.LoadTree();

  }
  LoadTree()
  {
    $("#jobGroupsTreeDiv").jstree({
      "core": {
          "check_callback": true,
          "themes": { "stripes": true }
      },
      "plugins": ["themes", "ui"]
  }).on('select_node.jstree', this.onNodeSelected); //add an event to the node selection (to get the data)

  $("#btnAdd").on('click', this.addNewNode);
  }
  
  onNodeSelected(e:any,data:any)
  {
    localStorage.setItem("select",data.node.original['JGID']);
    localStorage.setItem("nodeid",data.node.id);

  }

   //adding the root level jobgroup -- for the very first time.
   nodeLevel:any;
   addNewNode() {
    try {

        //get the tree object reference
        var obj_tree_ref = $("#jobGroupsTreeDiv").jstree(true);

        //get the selected node
        var obj_parent_node = $("#jobGroupsTreeDiv").jstree('get_selected');

        //if no node is selected, set the value to null, which means we are adding a root node
        if (obj_parent_node == null || obj_parent_node.length == 0)
            obj_parent_node = null; //to create a root node

            if ((this.Jobgroupname != undefined) && (this.Jobgroupname != "")) {
              if (this.ENXJobGroup.length > 0) {
                for (var k = 0; k < this.ENXJobGroup.length; k++) {
                  var groupName = this.ENXJobGroup[k].Name;
                  if (groupName == this.Jobgroupname) {
                      //Notify.alert('The groupname already exist.', {
                      // Notify.alert(errorMessageCodes.errorMessages['WAR061'].msg, {
                      //     status: 'warning',
                      //     pos: 'top-right',
                      //     timeout: 3000
                      // });
                      return;
                  }
              }
              }
              this.groupname = this.Jobgroupname;
              this.Jobgroupname = '';
              if (this.ENXJobGroup.length > 3) {
                  // Notify.alert(errorMessageCodes.errorMessages['WAR049'].msg, {
                  //     status: 'warning',
                  //     pos: 'top-right',
                  //     timeout: 3000
                  // });
                  return;
              }
              var newJobGroup:any = {};

              var node = $('#jobGroupsTreeDiv').jstree(true).get_node(localStorage.getItem('nodeid'));

              if(obj_parent_node==null)
              {
                
                 this.nodeLevel=0;
                newJobGroup['ParentId'] = '0';

              }
              else{

                  this.nodeLevel=node.original.Level+1;
                 var id=localStorage.getItem('select');
                newJobGroup['ParentId'] = node.original.JGID;

              }
              newJobGroup['Parent'] = null;
              newJobGroup['Name'] = this.groupname;
              newJobGroup['Description'] = this.description;
              //newJobGroup['ButtonColor'] = this.buttoncolor;
              newJobGroup['LevelDepth'] = this.nodeLevel;
              newJobGroup['Type'] = null;
              newJobGroup['JobGroups'] = null;
              newJobGroup['Jobs'] = null;
              newJobGroup['Users'] = null;
              newJobGroup['AllJobs'] = null;
              newJobGroup['JobGroupRefernce'] = null;
              newJobGroup['Id'] = this.JGID;

              this.ENXJobGroup.push(newJobGroup);

              //node text
        // var node_text = $("#txtJobGroupName").val();

        //node json with additional information
        var node_data = {
            "id": this.groupname,
            "text": this.groupname,
            "label": this.groupname,
            "NodeType": '1',
            "JGID": this.JGID,
            "Level": this.nodeLevel,
            "additional_data": { /*this additional_data will be added to the 'original' property of the node object */
                "job_level": 1,
                "jobs": [
                    { "job_id": 1, "job_name": "New Job" },
                    { "job_id": 2, "job_name": "New Job" }
                ],
                "users": [
                    { "username": "Rajesh" },
                    { "username": "Ramesh" }
                ]
            }
        }
        this.JGID = this.JGID + 1;
        //create the new node
        var new_node_id = obj_tree_ref.create_node(obj_parent_node, node_data, 'last', this.createNodeCallback, true);

        //check if the new node is created
        if (!new_node_id) {
            alert('Node creation failed'); //aler the user with the error message when creation fails
        }
        else
            $("#jobGroupsTreeDiv").jstree('open_all'); //expand all the nodes when a new node is added
    
            }
        

        }
    catch (e) {
        alert('Error occurred: ' + e);
    }
}
 Joblist = [];
 ENXJob:any = [];
 selectedaddjoblist:any=[];
 chkExistjOb:any=[];
adding_a_job()
{
  var obj_tree_ref = $("#jobGroupsTreeDiv").jstree(true);

  this.my_data = $("#jobGroupsTreeDiv").jstree(true);
  var obj_parent_node = $("#jobGroupsTreeDiv").jstree('get_selected');
  var node = $('#jobGroupsTreeDiv').jstree(true).get_node(localStorage.getItem('nodeid'));
if(node.original.NodeType=="2")
{
  this.notification.openSnackBar("Can not assign the job under the another job.","","warning-snackbar");
  return;
}
  for (var i = 0; i < this.Repository.length; i++) {
    for (var j = 0; j < this.Repository.length; j++) {
      if (this.Repository[j].JobName == node.original.id) {
        return;
    }
    }
    if (node == null) {
      return;
  }
  var a=0;
  if (this.Repository[i].Selected) {
    this.selectedaddjoblist.push(this.Repository[i]);
    var enxJob:any = {};
    enxJob['ParentId'] = node.original.JGID;
    enxJob['Name'] = this.Repository[i].JobName;
    enxJob['Description'] = this.Repository[i].description;
    enxJob['Id'] = this.Repository[i].Id;
    enxJob['ButtonColor'] = this.Repository[i].buttoncolor;
    enxJob['Profile'] = this.Repository[i].Profile;
    enxJob['MetadataName'] = this.Repository[i].MetadataName;
    enxJob['MetadataType'] = this.Repository[i].MetadataType;
    enxJob['MetadataEntry'] = this.Repository[i].MetadataEntry;
    enxJob['JobGroupReference'] = "";
    if(this.ENXJob.length>0)
    {
      for (var k = 0; k < this.ENXJob.length; k++) {
        if (this.ENXJob[k].Name == this.Repository[i].JobName) {
            a = 1;
            break;
        }
    }
    }
    
    if (a != 1) {
      this.ENXJob.push(enxJob);
      var JobName = this.Repository[i].JobName;
      var node_data = {
        "id": JobName,
        "text": JobName,
        "label": JobName,
        "NodeType": '2'
    }
    var new_node_id = obj_tree_ref.create_node(obj_parent_node, node_data, 'last', this.createNodeCallback, true);

    if (!new_node_id) {
      alert('Node creation failed'); //aler the user with the error message when creation fails
  }
  else{
      $("#jobGroupsTreeDiv").jstree('open_all'); //expand all the nodes when a new node is added

      }
  }
  }
  this.Repository[i].Selected = '';

  }

}
isAllSelected(val:any)
  {

     this.Repository.every(function(item:any) {
      return item.Selected == true;
    })
  // this.getCheckedItemList();
  }
createNodeCallback()
{

}
  logNode(node:any)
  {
console.log(node);
  }
  GetJobs()
  {
    let url="api/Jobs/GetJobsList";
    this.service.getAPI(url).then((resp:any[])=>{
this.Repository=resp;
    });

  }
  GetUsers()
  {
    let url="api/Jobs/GetUsers";
    this.service.getAPI(url).then((resp:any[])=>{
this.Users=resp;
    });

  }
 Userlist:any = [];
 isAllSelectedass(val:any)
 {

    this.Users.every(function(item:any) {
     return item.Selectedass == true;
   })
 // this.getCheckedItemList();
 }
  JobGroupSave()
  {
    let userlist:any;
    for (var i = 0; i < this.Users.length; i++) {
      if (this.Users[i].Selectedass) {
          var username = this.Users[i].UserName;
          this.Userlist.push(username);
           userlist = this.Userlist;
      }
  }
let url="api/Jobs/CreateJobGroup";
let postData = {
  JobGroups: this.ENXJobGroup,
  Jobs: this.ENXJob,
  Users: userlist
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>
  {
if(resp==true)
{
  this.notification.openSnackBar(this.errorCodes["SUC011"].msg,"","success-snackbar");
  userlist = '';
  this.Userlist = [];
  this.Joblist = [];
  if (localStorage.getItem('printer') == "Printer") {
    localStorage.removeItem('printer');
    this.router.navigate(['/printerConnector']);
    this.ngxLoader.stop();
}
else {
  this.ngxLoader.stop();
  this.router.navigate(['/JobManagement']);
}
this.ngxLoader.stop();
}
else{
this.notification.openSnackBar(this.errorCodes["WAR012"].msg,"","warning-snackbar");

this.ngxLoader.stop();

userlist = '';
this.Userlist = [];
this.Joblist = [];
}
  },(error)=>
  {

  })
console.log(postData);
  }

  try_delete_a_job()
  {
    var node = $('#jobGroupsTreeDiv').jstree(true).get_node(localStorage.getItem('nodeid'));
    $("#jobGroupsTreeDiv").jstree(true).delete_node(node);
    for (var i = 0; i < this.ENXJob.length; i++) {
      if (this.ENXJob[i].Name == node.original.text) {
          this.ENXJob.splice(i, 1);
      }
  }
  for (var i = 0; i < this.ENXJobGroup.length; i++) {
      if (this.ENXJobGroup[i].Name == node.original.text) {
          this.ENXJobGroup.splice(i, 1)
      }
  }

  }
  close()
  {
    if (localStorage.getItem('printer') == "Printer") {
      localStorage.removeItem('printer');
      this.router.navigate(['/printerConnector']);
    }
  else {
    this.router.navigate(['/JobManagement']);
  }
  }
}
