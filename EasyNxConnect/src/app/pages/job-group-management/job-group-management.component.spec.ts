import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobGroupManagementComponent } from './job-group-management.component';

describe('JobGroupManagementComponent', () => {
  let component: JobGroupManagementComponent;
  let fixture: ComponentFixture<JobGroupManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobGroupManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobGroupManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
