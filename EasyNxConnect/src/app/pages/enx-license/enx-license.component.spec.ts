import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnxLicenseComponent } from './enx-license.component';

describe('EnxLicenseComponent', () => {
  let component: EnxLicenseComponent;
  let fixture: ComponentFixture<EnxLicenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnxLicenseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnxLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
