import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ErrorCodesService } from 'src/app/ErrorMessageCodes/error-codes.service';
import { NotificationService } from 'src/app/notification.service';
import { ApiService } from 'src/app/_api/api.service';
import { ComponentCanDeactivate } from '../../component-can-deactivate';

@Component({
  selector: 'app-enx-license',
  templateUrl: './enx-license.component.html',
  styleUrls: ['./enx-license.component.scss']
})
export class EnxLicenseComponent implements OnInit,ComponentCanDeactivate {
  canDeactivate():boolean
   {
     return !this.isDirty
   }
   isDirty=false;

  LicenseType:string="";
  LicenseIssue:string="";
  LicenseCode:string="";
  Machineid:string="";
  RemainingImage:string="";
  MaintenanceImage:string="";
  deactivate:boolean=true;
  errorCodes:any = {};

  constructor(private service:ApiService,private router:Router,private ngxLoader:NgxUiLoaderService,private notification:NotificationService,private ErrorCodes:ErrorCodesService) { }

  ngOnInit(): void {
    this.errorCodes=this.ErrorCodes.errormsgs();
    this.loadLicenseSettings();
  }

  //Load General settings

  loadLicenseSettings()
  {
    let url="api/LicenseSetings/LoadLicenseSetting";
let postData={
  LicenseServer:""
}
 this.ngxLoader.start();
 this.service.post(url,postData).subscribe(resp => {
  if(resp!=null)
  {
    this.LicenseType = resp.LicenseType;
    this.LicenseIssue = resp.LicenseOwner;
    this.LicenseCode = resp.LicenseCode;
    this.Machineid = resp.MachineID;
    this.RemainingImage = resp.ImagesRemaining;
    this.MaintenanceImage = resp.MaintExpDate;
    this.ngxLoader.stop();
    if (resp.LicenseOwner == "<Not Licensed>") {
         this.deactivate = true;
         this.LicenseType = this.errorCodes["RES011"].res;
         this.LicenseIssue = this.errorCodes['RES011'].res;
         this.LicenseCode = this.errorCodes['RES011'].res;
        this.RemainingImage = resp.ImagesRemaining;
    }
    else if (resp.LicenseOwner == "<Corrupt License>") {
         this.deactivate = true;
         this.LicenseType = this.errorCodes['RES001'].res;
         this.LicenseIssue = this.errorCodes['RES001'].res;
         this.LicenseCode = this.errorCodes['RES001'].res;
        this.RemainingImage = resp.ImagesRemaining;
    }
    else if (resp.ImagesRemaining == null || resp.ImagesRemaining == "") {
         this.LicenseType = this.errorCodes['RES009'].res;
         this.RemainingImage = this.errorCodes['RES010'].res;
    }
  }
});
  }
  ActivateLicense()
  {
    this.isDirty=false;

let url="api/LicenseSetings/LicenseActivate"
let postData={
  LicenseCode: this.LicenseCode
}
this.ngxLoader.start();
this.service.post(url,postData).subscribe(resp=>{
  if (resp == "Invalid license code") {
    this.notification.openSnackBar(this.errorCodes["ERR006"].msg,"","red-snackbar");
    this.ngxLoader.stop();
  }
  else if (resp == "Offline Activation is required!") {
    this.notification.openSnackBar(this.errorCodes["ERR008"].msg,"","red-snackbar");
    this.ngxLoader.stop();
  }
  else if (resp == "The license (or image count) is corrupted. Please contact support.") {
    this.notification.openSnackBar(this.errorCodes["ERR009"].msg,"","red-snackbar");
    this.ngxLoader.stop();
  }
  else if (resp == "No more pages available in license.") {
    this.notification.openSnackBar(this.errorCodes["ERR010"].msg,"","red-snackbar");
    this.ngxLoader.stop();
  }
  else if (resp == "No more days available in license.") {
    this.notification.openSnackBar(this.errorCodes["ERR011"].msg,"","red-snackbar");
    this.ngxLoader.stop();
  }
  else if (resp == "Evaluation expired.") {
    this.notification.openSnackBar(this.errorCodes["ERR012"].msg,"","red-snackbar");
    this.ngxLoader.stop();
  }
  else if (resp == "Error during activation.") {
    this.notification.openSnackBar(this.errorCodes["ERR013"].msg,"","red-snackbar");
    this.ngxLoader.stop();
  }
  else{
    this.LicenseType = resp.LicenseType;
    this.LicenseIssue = resp.LicenseOwner;
    this.LicenseCode = resp.LicenseCode;
    this.Machineid = resp.MachineID;
    this.RemainingImage = resp.ImagesRemaining;
    this.MaintenanceImage = resp.MaintExpDate;
    this.notification.openSnackBar(this.errorCodes["SUC015"].msg,"","success-snackbar");
this.deactivate=false;
  }
});
  }
  DectivateLicense()
  {
    this.isDirty=false;
    let url="api/LicenseSetings/LicenseActivate"
    let postData={
      LicenseCode: this.LicenseCode
    }
    this.ngxLoader.start();
    this.service.post(url,postData).subscribe(resp=>{
      if (resp == false) {
        this.notification.openSnackBar(this.errorCodes["ERR007"].msg,"","red-snackbar");
        this.ngxLoader.stop();
      }
      else{
        this.LicenseType = resp.LicenseType;
        this.LicenseIssue = resp.LicenseOwner;
        this.LicenseCode = resp.LicenseCode;
        this.Machineid = resp.MachineID;
        this.RemainingImage = resp.ImagesRemaining;
        this.MaintenanceImage = resp.MaintExpDate;
        this.notification.openSnackBar(this.errorCodes["SUC016"].msg,"","success-snackbar");
    this.deactivate=true;
    this.ngxLoader.stop();

      }
    });
    
  }
}
