import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(public translate: TranslateService)
  {
    translate.addLangs(['en-US', 'nl']);
    translate.setDefaultLang('en-US');
  }
  title = 'EasyNxConnect';
  switchLang(event:any) {
    this.translate.use(event.value);
  }
}
